<?
session_start();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title id="page-title">DealerSocket | We Are Automotive</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="./stylesheets/all-min.css" rel="stylesheet">
	<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>
</head>
<body>

    <div class="container waa-heading">
        <div class="activateDar"></div>
        <div class="row waa-header">
            <div class="waa-logo">
                <a href="./index.php"><img class="shrink-mobile" src="./img/WeAreAutomotiveLogo-v2.png" alt="We Are Automotive"></a>
            </div>
            <div class="brand-container">
                <div class="brand-text">
                    Exclusive up-to-date market statistics<span class="larger-screen"> you can't get anywhere else.</span>
                </div>
            </div>
            <i class="fa fa-bars fa-2x mobile-nav" aria-hidden="true"></i>
            <div class="save-template">
                <i class="fa fa-save fa-2x" aria-hidden="true"></i>
                <div class="save-text animated">Save Template</div>
            </div>
            <ul class="social-links circle animated-effect-1 social-links-position">
                <li class="twitter"><a target="_blank" href="https://twitter.com/share" data-size="large" data-url="https://dev.twitter.com/web/tweet-button" data-via="twitterdev" data-related="twitterapi,twitter" data-hashtags="DealerSocket, We Are Automotive, WeAreAutomotive, Auto Dealers"
                        data-text="custom share text"><i class="fa fa-twitter"></i></a></li>
                <li class="facebook"><a target="_blank" class="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=http://www.we-are-automotive.com/"><i class="fa fa-facebook"></i></a></li>
                <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://www.we-are-automotive.com&title=We%20Are%20Automotive&summary=&source="><i class="fa fa-linkedin"></i></a></li>
                <li class="googleplus"><a target="_blank" href="https://plus.google.com/share?url=http://www.we-are-automotive.com"><i class="fa fa-google-plus"></i></a></li>
            </ul>
            <div class="autoscroll">
                Auto-Scroll: <span class="autoscroll-status">Off</span>
            </div>
            <div class="expand-toggle">
                <div class="toggle toggle-icons flow-right">
                    <i class="fa fa-toggle-off fa-2x" aria-hidden="true"></i>
                    <i class="fa fa-toggle-on fa-2x" aria-hidden="true"></i>
                </div>
                <i class="fa fa-expand expand-fullscreen" aria-hidden="true"></i>
                <i class="fa fa-expand close-fullscreen" aria-hidden="true"></i>
            </div>

        </div>
    </div>
    <!-- begin content -->
    <div class="p-10 main-container">
        <div class="left-navigation">
            <div class="general-manager icon-container">
                <a class="click-icons" href="./general-manager.php">
                    <i class="fa fa-group fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="gm-text">
                        <div class="v-center">
                            General Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="sales-manager icon-container">
                <a class="click-icons" href="./sales-manager.php">
                    <i class="fa fa-cubes fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="sales-text">
                        <div class="v-center">
                            Sales Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="service-manager icon-container">
                <a class="click-icons" href="./service-manager.php">
                    <i class="fa fa-cogs fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="service-text">
                        <div class="v-center">
                            Service Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="bdc-manager icon-container">
                <a class="click-icons" href="./bdc-manager.php">
                    <i class="fa fa-taxi fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="bdc-text">
                        <div class="v-center">
                            Business Development Center Template
                        </div>
                    </div>
                </a>
            </div>

            <div class="customize-manager icon-container" id="customize-waa">
                <i class="fa fa-code fa-2x nav-icon" aria-hidden="true"></i></br>
                <div class="customize-text">
                    <div class="v-center">
                        Customize Template
                    </div>
                </div>
            </div>
            <?

                if (isset($_SESSION['userid']))
                {
                    ?>
                    <div class="login-manager icon-container" id="logout-waa">
                        <i class="fa fa-sign-out fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="logout-text">
                            <div class="v-center">
                                Log Out
                            </div>
                        </div>
                    </div>
                    <?
                }
                else
                {
                    ?>
                    <div class="login-manager icon-container" id="login-waa">
                        <i class="fa fa-key fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="login-text">
                            <div class="v-center">
                                Login
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>
            <div class="refresh-manager icon-container" id="refresh-waa">
                <i class="fa fa-refresh fa-2x nav-icon" aria-hidden="true"></i></br>
                <div class="refresh-text">
                    <div class="v-center">
                        Refresh Board
                    </div>
                </div>
            </div>
        </div>
        <div class="charts-displayed">
            <div class="backdrop"></div>
            <div class="grid-stack grid-stack-12 m-l-140 m-r-20">
                <div class="grid-stack-item dealership-overview-grid" data-gs-x="0" data-gs-y="0" data-gs-width="12" data-gs-height="5" data-custom-id="1">
                    <div class="grid-stack-item-content">
                        <div class="box-prop" id="dealership-overview">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <h4 class="m-t-0 header-title">DEALERSHIP OVERVIEW</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="appt-sold knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Appt Sold %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="vehicle-serviced knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Sold Vehicles Also Serviced %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="return-customer knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Be-Back %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-cente dealership-overviewr">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="visits-with-turn knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Visits with Turn %</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="5" data-gs-width="6" data-gs-height="6" data-custom-id="16">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-line-2 box-prop scrollElement " data-sortable-id="target-revenue-line-2" id="chart-11">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> SALES AND SERVICE SOLD
                            <span class="text-muted m-b-30 font-13">
                                <div class="sales-legend inline"></div>
                                <span class="inline text-muted">Sales</span>
                                <div class="service-legend inline"></div>
                                <span class="inline text-muted">Service</span>
                                <div class="row">
                                    <div id="revenue-sparkline-2"></div>
                                </div>
                                <div class="row metrics">
                                    <div class="col-xs-6 revenue-sl-box">
                                        Sales Amount</br>
                                        <div>$<span class="sales-sold">0</span></div>
                                    </div>
                                    <div class="col-xs-6 revenue-sl-box">
                                        <div class="blue-text">Service Amount</br></div>
                                        <div>$<span class="service-sold">0</span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-stack-item" data-gs-x="6" data-gs-y="5" data-gs-width="6" data-gs-height="6" data-custom-id="18">
                        <div class="grid-stack-item-content">
                            <div class="target-revenue-pie-2 box-prop" data-sortable-id="target-revenue-pie-2" id="chart-13">
                                <i class="fa fa-times close-x" aria-hidden="true"></i> LABOR VS PARTS REVENUE
                                <span class="text-muted m-b-30 font-13">
                                    <div class="sales-legend inline"></div>
                                    <span class="inline text-muted">Labor</span>
                                    <div class="service-legend inline"></div>
                                    <span class="inline text-muted">Parts</span>
                                    <div class="row">
                                        <div id="revenue-sparkline"></div>
                                    </div>
                                    <div class="row metrics">
                                        <div class="col-xs-6 revenue-sl-box">
                                            Total Labor</br>
                                            <div>$<span class="labor-sold">0</span></div>
                                        </div>
                                        <div class="col-xs-6 revenue-sl-box">
                                            <span class="blue-text">Total Parts</br></span>
                                            <div>$<span class="parts-sold">0</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item" data-gs-x="0" data-gs-y="11" data-gs-width="12" data-gs-height="8" data-custom-id="24">
                            <div class="grid-stack-item-content">
                                <div class="vertical-bar box-prop" data-sortable-id="verticle-bar-2" id="chart-19">
                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                    <h4 class="m-t-0 header-title">ACTUAL LABOR HOURS VS BILLED</h4>
                                    <span class="text-muted m-b-30 font-13">
                                        <div class="red-legend inline"></div>
                                        <span class="inline text-muted">Labor Hours Spent</span>
                                        <div class="blue-legend m-l-10 inline"></div>
                                        <span class="inline text-muted">Labor Hours Billed</span>
                                    </span>
                                    <div id="labor-hours" class="ct-chart ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item automotive-metrics-grid" data-gs-x="0" data-gs-y="65" data-gs-width="12" data-gs-height="10" data-custom-id="26">
                            <div class="grid-stack-item-content">
                                <div class="box-prop" id="knobs">
                                    <div class="ipad-view">
                                        <i class="fa fa-times close-x" aria-hidden="true"></i>
                                        <h4 class="m-t-0 header-title">AUTOMOTIVE METRICS</h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Average displayed by percentage.
                                        </p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob outbound-calls-per-day" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                                    <h5 class="font-600 text-muted"><span class="completed">0</span> Outbound Calls Completed Today</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob closed-ro-appt" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#00b19d" value="0" />
                                                    <h5 class="font-600 f text-muted">Closed RO with Appointment Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob closed-ro-future" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#ffaa00" value="0" />
                                                    <h5 class="font-600 text-muted">Closed RO with Future Appointment Set Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-with-trade" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3ddcf7" value="0" />
                                                    <h5 class="font-600 text-muted">Sold With Trade Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-return-customer" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#f76397" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Return Customer Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-fresh-up" data-width="150" data-height="150" data-bgColor="#505A66" value="0" data-fgColor="#7266ba" />
                                                    <h5 class="font-600 text-muted">Sold Fresh Up Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-phone-up" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#98a6ad" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Phone Up Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-internet" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#ef5350" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Internet Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>





            </div>
        </div>
        <!-- hidden forms -->
        <div class="right-content" style="display: none;">
            <!-- begin login-header -->
            <div class="login-header">
                <div id="login-content">
                </div>
            </div>
            <!-- begin login-content -->
            <div class="login-content">
                <hr />
                <p class="text-center">
                    &copy; DealerSocket All Right Reserved 2016
                </p>
            </div>
            <!-- end login-content -->
        </div>
        <div class="blocker"></div>
        <div class="download-dar animated fadeInDownBig">
            <div class="close-dar-background"></div>
            <i class="fa fa-times-circle fa-2x close-dar-x" aria-hidden="true"></i>
            <div class="dar-container">
                <span class="dar-heading"><em><h4>Dealership Action Reports</h4></em></span><span class="dar">Shaped with data sourced from more than 6,500 dealerships around the world, the Dealership Action Report (DAR) is a resource developed by DealerSocket’s expert team to guide dealers to data-driven processes that drive sales, retention, and profitability.</span></br>
                </br>
                <div class="franchise-download">
                    <a href="http://info.dealersocket.com/DAR-Asset-Download.html" target="blank">Franchise</a>
                </div>
                <div class="independent-download">
                    <a href="http://info.dealersocket.com/idar.html" target="blank">Independent</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="./js/all-min.js"></script>

    <!-- end content -->
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->

    <!-- ================== BEGIN BASE JS FROM FOOTER ================== -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3743008-14', 'auto');
  ga('send', 'pageview');

</script>
<!-- RTP tag -->
<script type='text/javascript'>
(function(c,h,a,f,i){c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
 c[a].a=i;var g=h.createElement("script");g.async=true;g.type="text/javascript";
 g.src=f+'?aid='+i;var b=h.getElementsByTagName("script")[0];b.parentNode.insertBefore(g,b);
 })(window,document,"rtp","//sjrtp5-cdn.marketo.com/rtp-api/v1/rtp.js","dealersocket");

rtp('send','view');
rtp('get', 'campaign',true);
</script>
<!-- End of RTP tag -->

    <!--[if lt IE 9]>
                <script src="assets/crossbrowserjs/html5shiv.js"></script>
                <script src="assets/crossbrowserjs/respond.min.js"></script>
                <script src="assets/crossbrowserjs/excanvas.min.js"></script>
                <![endif]-->
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->



    <!-- ================== END PAGE LEVEL JS ================== -->

</body>

</html>
