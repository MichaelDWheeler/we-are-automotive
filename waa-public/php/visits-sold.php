<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function createNumber($min, $max, $multiple){
    $randomNumber = mt_rand($min, $max);
    $final = round($randomNumber * $multiple);
    return $final;
}

$internetLead = array();
$phoneLead = array();
$freshUp = array();
$base = array();

for($i = 0; $i <=31; $i++){
    array_push($internetLead, createNumber(0,0,0.48));
    array_push($phoneLead, createNumber(0,0,0.41));
    array_push($freshUp, createNumber(0,0,0.22));
    array_push($base, createNumber(0,0,0.22));
}

$visitsSold = array($internetLead, $phoneLead, $freshUp, $base);

echo json_encode($visitsSold);

// [[7,6,2,2,0,8,3,4,8,6,7,0,7,5,10,6,10,8,2,9,7,9,8,7,5,7,7,1,5,8,5,8],[3,7,8,7,2,1,5,4,2,5,2,6,2,5,0,1,5,7,0,1,6,1,8,3,6,6,5,7,7,4,6,8],[2,1,3,2,2,4,3,4,1,1,4,3,4,0,1,3,1,2,2,1,0,3,4,4,4,2,2,1,4,1,3,2]]
?>
