<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function createRandomNumber(){
    $sometimes = rand(1,100);
    if($sometimes > 95){
        $randomNumber = rand(99,5000);
    }else{
        $randomNumber = rand(99,1200);
    }
    return $randomNumber;
}

$salesDataOver30Days = array();
$serviceDataOver30Days = array();

for($i = 0; $i <=30; $i++){
    array_push($salesDataOver30Days, createRandomNumber());
    array_push($serviceDataOver30Days, createRandomNumber());
}

$salesAndServiceCombined = array($salesDataOver30Days, $serviceDataOver30Days);

echo json_encode($salesAndServiceCombined);
// [[955,242,523,107,191,643,841,2880,325,661,378,1135,1137,489,1198,321,648,530,473,1080,194,656,686,685,190,216,677,115,1095,1155,610],[449,197,3239,793,113,165,1113,208,315,625,846,191,727,287,1180,1101,917,1095,1096,1589,396,616,174,797,141,230,899,372,697,1105,1211]]
?>
