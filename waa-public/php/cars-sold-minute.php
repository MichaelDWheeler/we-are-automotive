<?php
//number of cars sold per minute based off operating hours and last year.

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

$year2015 = "17410000";
$carsSoldPerDay = $year2015/365;
$currentYear = date("Y");
$endOfYear = strtotime('31 December' . $currentYear) / 86400;
$now = strtotime('today')/86400;
$daysLeftUntilEndOfYear = $endOfYear - $now;
$daysGoneBy = 365 - $daysLeftUntilEndOfYear;
$carsSoldThisYear = ($daysGoneBy - 1) * $carsSoldPerDay;
$minutesStart = strtotime('today 00:00:00');
$minutesEnd = strtotime('today 24:00:00');
$minutesInWorkDay = ($minutesEnd - $minutesStart)/60;
$minutesNow = time();
$minutesPast = ($minutesNow - $minutesStart)/60;
$carsSoldPerMinute = $carsSoldPerDay/$minutesInWorkDay;

$arr = array($carsSoldPerMinute);

echo json_encode($arr);
// [33.12404870624]
 ?>
