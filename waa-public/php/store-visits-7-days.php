<?php

error_reporting(E_ALL);

ini_set('display_errors', '1');


////connect to the function engine////
require "../includes/waafunctions.php";
$newSold = array();
$usedSold = array();
$todayunix=time();
$yesterday=$todayunix - 86400;
$sevenback=($todayunix - 604800);
$totalSources = array();
$walkIn = array();
$phoneInTotal = array();
$internet = array();
$startday = date('Y-m-d' , $yesterday);
$endday  = date('Y-m-d', $sevenback);

function makeArrays($arrayPassed){
    $walkInArray = array();
    $phoneInArray = array();
    $internetArray= array();

    foreach($arrayPassed as $key => $value){
        $datearr = explode("-", $key);
        if($datearr[2] === "01"){
            $walkInMultiplier = 0.15;
            $phoneInMultiplier = 0.39;
            $internetMultiplier = 0.46;

        }else if($datearr[2] === "02"){
            $walkInMultiplier = 0.21;
            $phoneInMultiplier = 0.38;
            $internetMultiplier = 0.41;

        }else if($datearr[2] === "03"){
            $walkInMultiplier = 0.31;
            $phoneInMultiplier = 0.35;
            $internetMultiplier = 0.34;

        }else if($datearr[2] === "04"){
            $walkInMultiplier = 0.24;
            $phoneInMultiplier = 0.40;
            $internetMultiplier = 0.36;

        }else if($datearr[2] === "05"){
            $walkInMultiplier = 0.17;
            $phoneInMultiplier = 0.42;
            $internetMultiplier = 0.41;

        }else if($datearr[2] === "06"){
            $walkInMultiplier = 0.20;
            $phoneInMultiplier = 0.44;
            $internetMultiplier = 0.36;

        }else if($datearr[2] === "07"){
            $walkInMultiplier = 0.24;
            $phoneInMultiplier = 0.38;
            $internetMultiplier = 0.38;

        }else if($datearr[2] === "08"){
            $walkInMultiplier = 0.14;
            $phoneInMultiplier = 0.35;
            $internetMultiplier = 0.51;

        }else if($datearr[2] === "09"){
            $walkInMultiplier = 0.26;
            $phoneInMultiplier = 0.43;
            $internetMultiplier = 0.31;

        }else if($datearr[2] === "10"){
            $walkInMultiplier = 0.19;
            $phoneInMultiplier = 0.35;
            $internetMultiplier = 0.46;

        }else if($datearr[2] === "11"){
            $walkInMultiplier = 0.18;
            $phoneInMultiplier = 0.38;
            $internetMultiplier = 0.44;

        }else if($datearr[2] === "12"){
            $walkInMultiplier = 0.24;
            $phoneInMultiplier = 0.39;
            $internetMultiplier = 0.37;

        }else if($datearr[2] === "13"){
            $walkInMultiplier = 0.16;
            $phoneInMultiplier = 0.31;
            $internetMultiplier = 0.53;

        }else if($datearr[2] === "14"){
            $walkInMultiplier = 0.15;
            $phoneInMultiplier = 0.40;
            $internetMultiplier = 0.45;

        }else if($datearr[2] === "15"){
            $walkInMultiplier = 0.30;
            $phoneInMultiplier = 0.22;
            $internetMultiplier = 0.48;

        }else if($datearr[2] === "16"){
            $walkInMultiplier = 0.20;
            $phoneInMultiplier = 0.46;
            $internetMultiplier = 0.34;

        }else if($datearr[2] === "17"){
            $walkInMultiplier = 0.15;
            $phoneInMultiplier = 0.37;
            $internetMultiplier = 0.48;

        }else if($datearr[2] === "18"){
            $walkInMultiplier = 0.17;
            $phoneInMultiplier = 0.30;
            $internetMultiplier = 0.53;

        }else if($datearr[2] === "19"){
            $walkInMultiplier = 0.18;
            $phoneInMultiplier = 0.31;
            $internetMultiplier = 0.49;

        }else if($datearr[2] === "20"){
            $walkInMultiplier = 0.24;
            $phoneInMultiplier = 0.26;
            $internetMultiplier = 0.50;
        }else if($datearr[2] === "21"){

            $walkInMultiplier = 0.23;
            $phoneInMultiplier = 0.32;
            $internetMultiplier = 0.45;

        }else if($datearr[2] === "22"){
            $walkInMultiplier = 0.19;
            $phoneInMultiplier = 0.32;
            $internetMultiplier = 0.49;

        }else if($datearr[2] === "23"){
            $walkInMultiplier = 0.22;
            $phoneInMultiplier = 0.30;
            $internetMultiplier = 0.48;

        }else if($datearr[2] === "24"){
            $walkInMultiplier = 0.25;
            $phoneInMultiplier = 0.37;
            $internetMultiplier = 0.48;

        }else if($datearr[2] === "25"){
            $walkInMultiplier = 0.22;
            $phoneInMultiplier = 0.33;
            $internetMultiplier = 0.45;

        }else if($datearr[2] === "26"){
            $walkInMultiplier = 0.17;
            $phoneInMultiplier = 0.36;
            $internetMultiplier = 0.47;

        }else if($datearr[2] === "27"){
            $walkInMultiplier = 0.20;
            $phoneInMultiplier = 0.38;
            $internetMultiplier = 0.42;

        }else if($datearr[2] === "28"){
            $walkInMultiplier = 0.18;
            $phoneInMultiplier = 0.37;
            $internetMultiplier = 0.45;

        }else if($datearr[2] === "29"){
            $walkInMultiplier = 0.22;
            $phoneInMultiplier = 0.33;
            $internetMultiplier = 0.45;

        }else if($datearr[2] === "30"){
            $walkInMultiplier = 0.20;
            $phoneInMultiplier = 0.35;
            $internetMultiplier = 0.45;

        }else {
            $walkInMultiplier = 0.18;
            $phoneInMultiplier = 0.31;
            $internetMultiplier = 0.51;
        }

        array_push($walkInArray, $value * $walkInMultiplier);
        array_push($phoneInArray, $value * $phoneInMultiplier);
        array_push($internetArray, $value * $internetMultiplier);

        $returnedArray = array($walkInArray, $phoneInArray, $internetArray);
    }

    return $returnedArray;
};

$getrsql="SELECT * FROM `widgets2016`
WHERE `widgetname` LIKE 'soldnew30' AND `datadate`
BETWEEN '".$endday."'
AND '".$startday."'  order by `datadate` DESC";
$getrsql_result=mysql_query($getrsql);
while ($tinfo=mysql_fetch_array($getrsql_result)){

    $valuen[$tinfo['datadate']] = $tinfo['var1'];

}

$getrsql="SELECT * FROM `widgets2016`
WHERE `widgetname` LIKE 'soldused30' AND `datadate`
BETWEEN '".$endday."'
AND '".$startday."'  order by `datadate` DESC";
$getrsql_result=mysql_query($getrsql);
while ($tinfo=mysql_fetch_array($getrsql_result)){

    $valueu[$tinfo['datadate']] = $tinfo['var1'];
}

$newCarsVisits = makeArrays($valuen);
$usedCarVisits = makeArrays($valueu);

$walkInNew = $newCarsVisits[0];
$phoneInNew = $newCarsVisits[1];
$internetNew = $newCarsVisits[2];
$walkInUsed = $usedCarVisits[0];
$phoneInUsed = $usedCarVisits[1];
$internetUsed = $usedCarVisits[2];

$arrayLength = sizeof($walkInNew);


function addValuesInArrays($firstArray, $secondArray){
    global $arrayLength;
    $finishedArray = array();
    for($i = 0; $i < $arrayLength; $i++){
        $sum = $firstArray[$i] + $secondArray[$i];
        array_push($finishedArray, $sum);
    }
    return $finishedArray;
}

$walkIn = addValuesInArrays($walkInNew, $walkInUsed);
$phoneInTotal = addValuesInArrays($phoneInNew, $phoneInUsed);
$internet = addValuesInArrays($internetNew, $internetUsed);

$totalWalkinFromSource = array($walkIn, $phoneInTotal, $internet);
echo json_encode($totalWalkinFromSource);

?>
