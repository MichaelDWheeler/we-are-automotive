<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function createRandomNumber(){
    $chance = rand(1,100);

    if($chance > 95){
        $randomNumber = rand(1,10);
    }else if($chance > 90){
        $randomNumber = rand(1,9);
    }else if($chance > 80){
        $randomNumber = rand(1,8);
    }else if($chance > 70){
        $randomNumber = rand(1,7);
    }else if($chance > 60){
        $randomNumber = rand(1,6);
    }else if($chance > 50){
        $randomNumber = rand(1,5);
    }else{
        $randomNumber = rand(1,4);
    }
    return $randomNumber;
}

$outboundCallsSkipped = array();


for($i = 0; $i <=30; $i++){
    array_push($outboundCallsSkipped, createRandomNumber());

}

echo json_encode($outboundCallsSkipped);

// [4,8,8,8,9,8,1,9,3,4,9,10,2,5,6,8,4,3,3,6,1,8,3,4,7,10,7,5,2,5,2]
?>
