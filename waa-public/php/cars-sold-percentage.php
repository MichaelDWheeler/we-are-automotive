<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

$year2016 = "17634720";
$carsSoldPerDay = $year2016/365;
$currentYear = date("Y");
$endOfYear = strtotime('31 December' . $currentYear) / 86400;
$now = strtotime('today')/86400;
$daysLeftUntilEndOfYear = $endOfYear - $now;
$daysGoneBy = 365 - $daysLeftUntilEndOfYear;
$carsSoldThisYear = ($daysGoneBy - 1) * $carsSoldPerDay;
$minutesStart = strtotime('today 06:00:00');
$minutesEnd = strtotime('today 21:00:00');
$minutesInWorkDay = ($minutesEnd - $minutesStart)/60;
$minutesNow = time();
$minutesPast = ($minutesNow - $minutesStart)/60;
$carsSoldPerMinute = $carsSoldPerDay/$minutesInWorkDay;
$carsSoldToday = $minutesPast * $carsSoldPerMinute;
$carSoldNow = $carsSoldThisYear + $carsSoldToday;
$percentage = ($carSoldNow/$year2016) * 100;


$arr = array(round($percentage));

echo json_encode($arr);

// [62]
 ?>
