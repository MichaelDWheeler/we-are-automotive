<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

$carsSoldAug92016 = 2592191;
$currentYear = date("Y");
$beginningOfYear = strtotime('1 January' . $currentYear)/86400;
$endOfYear = strtotime('31 December' . $currentYear)/86400;
$numberOfDaysToAug9 = strtotime('9 August' . $currentYear)/86400;
$daysInSale = $numberOfDaysToAug9 - $beginningOfYear;
$now = (time()/86400) - $beginningOfYear;
$carsSoldPerDay = $carsSoldAug92016/$daysInSale;
$carsSoldCurrentDS = $carsSoldPerDay * $now;
$minutesStart = strtotime('today 00:00:00');
$minutesEnd = strtotime('today 24:00:00');
$minutesInWorkDay = ($minutesEnd - $minutesStart)/60;
$carsSoldPerMinute = $carsSoldPerDay/$minutesInWorkDay;
$arr = array("arr1" =>$carsSoldCurrentDS, "arr2" => $carsSoldPerDay, "arr3" => $carsSoldPerMinute);

echo json_encode($arr);

// {"dealerSocketSales":2681130.6553443,"carsSoldPerMinute":8.1469325539002,"carsSoldPerDay":11731.582877616}
 ?>
