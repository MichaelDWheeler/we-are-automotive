<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

$lastYear = "17634720";
$jan = 0.06546;
$feb = 0.07665;
$mar = 0.09098;
$apr = 0.08591;
$may = 0.08759;
$jun = 0.08629;
$jul = 0.08629;
$aug = 0.08618;
$sep = 0.08228;
$oct = 0.08309;
$nov = 0.07518;
$dec = 0.09365;

$multiplier;

$carsSoldPerDay = $lastYear/365;
$currentYear = date("Y");
$currentMonth = date("M");

if($currentMonth == 'Jan'){
    $multiplier = $dec;
}else if($currentMonth == 'Feb'){
    $multiplier = $jan;
}else if($currentMonth == 'Mar'){
    $multiplier = $feb;
}else if($currentMonth == 'Apr'){
    $multiplier = $mar;
}else if($currentMonth == 'May'){
    $multiplier = $apr;
}else if($currentMonth == 'Jun'){
    $multiplier = $may;
}else if($currentMonth == 'Jul'){
    $multiplier = $jun;
}else if($currentMonth == 'Aug'){
    $multiplier = $jul;
}else if($currentMonth == 'Sep'){
    $multiplier = $aug;
}else if($currentMonth == 'Oct'){
    $multiplier = $sep;
}else if($currentMonth == 'Nov'){
    $multiplier = $oct;
}else{
    $multiplier = $dec;
}

$lastMonthTotal = $lastYear * $multiplier;
$endOfYear = strtotime('31 December' . $currentYear) / 86400;
$now = strtotime('today')/86400;
$daysLeftUntilEndOfYear = $endOfYear - $now;
$daysGoneBy = 365 - $daysLeftUntilEndOfYear;
$carsSoldThisYear = ($daysGoneBy - 1) * $carsSoldPerDay;
$minutesStart = strtotime('today 00:00:00');
$minutesEnd = strtotime('today 24:00:00');
$minutesInWorkDay = ($minutesEnd - $minutesStart)/60;
$minutesNow = time();
$minutesPast = ($minutesNow - $minutesStart)/60;
$carsSoldPerMinute = $carsSoldPerDay/$minutesInWorkDay;
$carsSoldToday = $minutesPast * $carsSoldPerMinute;
$carsSoldPerDay = $lastYear/365;
$dayOfMonth = date('d');
$carsSoldThisMonth = ($dayOfMonth * $carsSoldPerDay) + $carsSoldToday;
$percentage = $carsSoldThisMonth/$lastMonthTotal * 100;


$arr = array(round($percentage));

echo json_encode($arr);

// [62]
 ?>
