<?php

function getValues(){
    //each object contains the quarters for the item, not the values for each item in a quarter
    $firstQuarter = rand(25, 250);
    $secondQuarter = rand(25, 250);
    $thirdQuarter = rand(25,250);
    $fourthQuarter = rand(25,250);
    return array($firstQuarter, $secondQuarter, $thirdQuarter, $fourthQuarter);
}

$floor = getValues();
$internet = getValues();
$phone = getValues();
$total = array($floor[0] + $internet[0] + $phone[0], $floor[1] + $internet[1] + $phone[1], $floor[2] + $internet[2] + $phone[2], $floor[3] + $internet[3] + $phone[3]);


// $quarterTotals = array($floor, $internet, $phone, $total);
$quarterTotals = array([[151,159,212,153],[189,162,150,213],[182,71,44,223],[522,392,406,589]]);
echo json_encode($quarterTotals);

// [[216,93,140,152],[176,200,216,78],[210,159,141,67],[602,452,497,297]]
?>
