<? session_start(); ?>


       <?

 if ($_SESSION['userid']>0)
 {
?>
<div class="login-block">
    <div>
        Save Your Board
    </div><br>
    <form>
        <input type="hidden" class="form-control input-lg" id="userid" value="<? echo $_SESSION['userid']; ?>"><br>

        <div class="form-group m-b-15">
            <input type="boardname" class="form-control input-lg" id="name-board" placeholder="Board Name" required/>
            <p class="board-name-error margin-left-1em">
                You must name your board.
            </p>
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg login-button">Save Your Board</button>
        </div>
    </form>
    <div class="m-t-20 p-b-20">
        <a class="text-success" id="cancel-save">Cancel</a> this action.
    </div>

</div>

<script src="./js/check-login.js"></script>

<?
 }
 else
 {
?>
<div class="login-block">
    <div>
            <a class="text-success" id="cancel-login">Close this panel.</a>
         <!-- Not a member yet? Click <a class="text-success" id="register">here</a> to register. -->
    </div>
    <br>
    <form>
        <input type="email" class="form-control input-lg" id="email-address" placeholder="Email Address" required/><br>
        <p class="email-error margin-left-1em">
            The email you have entered is invalid.
        </p>
        <div class="form-group m-b-15">
            <input type="password" class="form-control input-lg" id="login-password" placeholder="Password" required/>
            <p class="password-error margin-left-1em">
                You must enter your password.
            </p>
        </div>
        <div class="form-group m-b-15">
            <input type="board-name" class="form-control input-lg" id="board-name" placeholder="Board name" required/>
            <p class="board-name-error margin-left-1em">
                You must name your board.
            </p>
        </div>
        <div class="login-buttons">
            <button type="submit" class="btn btn-success btn-block btn-lg login-button">Login Now</button>
        </div>
    </form>
    <div class="m-t-20">
        Click <a class="text-success" id="register">here</a> to register.
    </div>
</div>

<script src="./js/check-login.js"></script>
<?
 }
    ?>
