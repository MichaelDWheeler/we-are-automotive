<?php

$phone = "phone";
$freshUp = "freshUp";
$internet = "internet";
$y = "y";

$date=date('Y-m-d');
$currentYear = date("Y",strtotime($date));
$thisYear = $currentYear;
$past1year = strval($currentYear -1);
$past2year = strval($currentYear -2);
$past3year = strval($currentYear -3);
$past4year = strval($currentYear -4);
$past5year = strval($currentYear -5);

$year1 = array($y => $past5year, $phone => "25", $freshUp=>"25", $internet=>"8");
$year2 = array($y => $past4year, $phone => "23", $freshUp=>"21", $internet=>"10");
$year3 = array($y => $past3year, $phone => "22", $freshUp=>"21", $internet=>"13");
$year4 = array($y => $past2year, $phone => "18", $freshUp=>"18", $internet=>"17");
$year5 = array($y => $past1year, $phone => "13", $freshUp=>"18", $internet=>"20");
$year6 = array($y => $thisYear, $phone => "9", $freshUp=>"17", $internet=>"23");

$allYears = array($year1, $year2, $year3, $year4, $year5, $year6);

echo json_encode($allYears);

// [{"y":"2011","phone":"25","freshUp":"25","internet":"8"},{"y":"2012","phone":"23","freshUp":"21","internet":"10"},{"y":"2013","phone":"22","freshUp":"21","internet":"13"},{"y":"2014","phone":"18","freshUp":"18","internet":"17"},{"y":"2015","phone":"13","freshUp":"18","internet":"20"},{"y":"2016","phone":"9","freshUp":"17","internet":"23"}]
?>
