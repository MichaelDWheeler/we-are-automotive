<?php
//cars sold per month based off last year

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
date_default_timezone_set("America/Los_Angeles");

$lastYear = "17634720";
$carsSoldPerDay = $lastYear/365;
$currentYear = date("Y");
$endOfYear = strtotime('31 December' . $currentYear) / 86400;
$now = strtotime('today')/86400;
$daysLeftUntilEndOfYear = $endOfYear - $now;
$daysGoneBy = 365 - $daysLeftUntilEndOfYear;
$carsSoldThisYear = ($daysGoneBy - 1) * $carsSoldPerDay;
$minutesStart = strtotime('today 00:00:00');
$minutesEnd = strtotime('today 24:00:00');
$minutesInWorkDay = ($minutesEnd - $minutesStart)/60;
$minutesNow = time();
$minutesPast = ($minutesNow - $minutesStart)/60;
$carsSoldPerMinute = $carsSoldPerDay/$minutesInWorkDay;
$carsSoldToday = $minutesPast * $carsSoldPerMinute;
$carsSoldPerDay = $lastYear/365;
$dayOfMonth = date('d');
$carsSoldThisMonth = ($dayOfMonth * $carsSoldPerDay) + $carsSoldToday;

$arr = array('arr1' => (floor($carsSoldThisMonth)), 'arr2'  => "354068", 'arr3' => (floor($carsSoldPerMinute)));

echo json_encode($arr);

// {"arr1":790901,"arr2":"354068","arr3":33}

 ?>
