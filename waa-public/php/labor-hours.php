<?php
function randomFloat($min = 0.70, $max = 2.5) {
    return $english_format_number = number_format($min + mt_rand() / mt_getrandmax() * ($max - $min), 2, '.', '');
}

$month1 = (float)randomFloat();
$month2 = (float)randomFloat();
$month3 = (float)randomFloat();
$month4 = (float)randomFloat();
$month5 = (float)randomFloat();
$month6 = (float)randomFloat();

$currentMonth = date('F Y');
$past1month = date('F Y', mktime(0,0,0, date('m')-1, 1, date('Y')));
$past2month = date('F Y', mktime(0,0,0, date('m')-2, 1, date('Y')));
$past3month = date('F Y', mktime(0,0,0, date('m')-3, 1, date('Y')));
$past4month = date('F Y', mktime(0,0,0, date('m')-4, 1, date('Y')));
$past5month = date('F Y', mktime(0,0,0, date('m')-5, 1, date('Y')));

$monthsBilled = array($currentMonth, $past1month, $past2month, $past3month, $past4month, $past5month);
$hoursBilled = array($month1, $month2, $month3, $month4, $month5, $month6);

$combineArrays = array($monthsBilled, $hoursBilled);

echo json_encode($combineArrays);

// [["August 2016","July 2016","June 2016","May 2016","April 2016","March 2016"],[1.68,2.32,1.31,1.89,1.48,1.7]]
?>
