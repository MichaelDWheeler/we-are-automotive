<?php

date_default_timezone_set("America/Los_Angeles");

$outboundCallsPerDay = 325689;
$startofday=strtotime('today 12am');
$endofday=strtotime('today 11:59pm');
$now=time();

$nowsecondsofworkday=$now-$startofday; //how many milliseconds have elapsed since work day
$secondsinaworkday=$endofday-$startofday; //how many total seconds are in the work day

$callspersecond=$outboundCallsPerDay/$secondsinaworkday; //total of calls that are made per second

$callsMade = $nowsecondsofworkday * $callspersecond;
$percent = $callsMade/$outboundCallsPerDay;

$percentCompleted = round($percent * 100);
$outboundCallsData = array($percentCompleted, round($callsMade),$callspersecond,$outboundCallsPerDay);

echo json_encode($outboundCallsData);
// [71,232139,3.7721681723419,325689]
?>
