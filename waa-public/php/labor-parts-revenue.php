<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function createRandomNumber(){
    $sometimes = rand(1,100);
    if($sometimes > 95){
        $randomNumber = rand(99,5000);
    }else{
        $randomNumber = rand(99,1200);
    }
    return $randomNumber;
}

$laborData = array();
$partsData = array();


for($i = 0; $i <=30; $i++){
    array_push($laborData, createRandomNumber());
    array_push($partsData, createRandomNumber());
}

$laborVsParts = array($laborData, $partsData);

echo json_encode($laborVsParts);

// [[816,1166,780,847,426,653,215,214,1132,1122,397,4703,389,324,267,729,258,692,411,149,1060,682,197,829,1037,442,1048,921,685,142,941],[632,221,379,1139,160,625,839,849,416,436,915,130,976,838,426,4274,558,782,935,605,182,524,1046,288,1069,318,4106,337,864,920,970]]
?>
