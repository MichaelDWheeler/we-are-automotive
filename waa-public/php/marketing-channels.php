<?php

$labelValue = array("Radio", "Newspaper", "Dealer Website", "3rd Party");
$totalProfit = array(2056, 1541, 1653, 1740);
$costPerSold = array(4902, 3301, 231, 3098);
$marketingChannelData = array($labelValue, $totalProfit, $costPerSold);
echo json_encode($marketingChannelData);

// [["Radio","Newspaper","Dealer Website","AutoTrader","Autobytel","BlackBook","CarGurus","Cars.com","Edmunds","TrueCar","Dealix","KBB"],[2056,1541,1653,1740,1800,1949,1912,2607,1066,1111,1756,1575],[2343,2101,133,970,244,64,129,206,431,119,217,497]]
?>
