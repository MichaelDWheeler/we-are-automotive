var morrisCounter = 2;
var jsonMorrisDonut, apptSold, apptConfirmed, apptShow, donutTimer, morrisDataLength, donutTimer;

var rotateData = function(){
    $('#morris-donut').empty();
    if(morrisCounter === morrisDataLength){
        morrisCounter = 0;
    }
    drawDonut();
    window.m.select(morrisCounter);
    morrisCounter++;
};

var morrisVariation = function(morrisValue){

    var chance = Math.floor(Math.random() * 100);
    if (chance > 20 && chance <= 30){
        return morrisValue - 1;
    }else if (chance > 10 && chance <=20){
        return morrisValue -2;
    }else if (chance > 2 && chance <=10){
        return morrisValue - 3;
    }else if (chance === 2){
        return morrisValue - 4;
    }else if (chance === 1){
        return morrisValue - 5;
    }else if(chance > 70 && chance <= 80){
        return morrisValue + 1;
    }else if(chance > 80 && chance <= 90){
        return morrisValue + 2;
    }else if(chance > 90 && chance <= 98){
        return morrisValue + 3;
    }else if(chance === 99){
        return morrisValue + 4;
    }else if(chance === 100){
        return morrisValue + 5;
    }else{
        return morrisValue;
    }
};


var drawDonut = function(){
    apptSold = jsonMorrisDonut[0];
    apptConfirmed = jsonMorrisDonut[1];
    apptShow = jsonMorrisDonut[2];
    window.m = Morris.Donut({
        element: 'morris-donut',
        resize: true,
        data: [
            {label: "Appt Confirmed %", value: morrisVariation(apptConfirmed)},
            {label: "Appt Show %", value: morrisVariation(apptShow)},
            {label: "Appt Sold %", value: morrisVariation(apptSold)}
        ],
        colors: ["#3bafda", "#00b19d", "#ededed"],
        labelColor: '#fff'
    });
    morrisDataLength = window.m.data.length;
    donutTimer = window.setTimeout(function(){
        rotateData();
    },3000);
};


var getAppointmentData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/appointment-data.php', true);
    xhr.send();
    xhr.onload = function(){
        jsonMorrisDonut = JSON.parse(xhr.response);
        drawDonut();
    };
};
