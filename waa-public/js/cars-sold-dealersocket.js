var carSoldDealerSocket = function(){
    'use strict';

    var number = 0;
    var carsSoldDs, soldPerMinute;

    var numberWithCommas = function(dsSalesNumber){
        return dsSalesNumber.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };
    var randomDelayResult;
    var randomDelay = function(){
        randomDelayResult = Math.random() * 5000 + 1000;
        return randomDelayResult;
    };

    var makeNewNumber = function(addNumber){
    var targetNumber = number + addNumber;
        var makeNewNumberInterval = window.setInterval(function(){
            if(number < Math.round(targetNumber)){
                number++;
                printNumber();
            }else{
                number = targetNumber;
                window.clearInterval(makeNewNumberInterval);
            }
        }, 300);

    };

    var addCarsOverTime = function(){
        var numberOfCarsPerSecond =  soldPerMinute/60;
        var carsSoldDealerSocketYTD = window.setTimeout(function(){
            var addNumber = (randomDelayResult * (numberOfCarsPerSecond))/1000;
            makeNewNumber(addNumber);
            addCarsOverTime();
        },randomDelay());
    };

    var printNumber = function(){
        $('.dealersocket-sales-number').text(numberWithCommas(Math.round(number)));
    };

    var delay = 100;

    var countLast10 = function(){
        var countLast10Timer = window.setTimeout(function(){
            printNumber();
            if(number != carsSoldDs){
                delay = delay + delay * 0.175;
                ++number;
                countLast10();
            }else{
                addCarsOverTime();
            }
        },delay);
    };


    var countUp = function(){
        var countTimer = window.setTimeout(function(){
            var countInterval = window.setInterval(function(){
                if(number < carsSoldDs - 10){
                    number = number + Math.floor(Math.random() * 20000);
                    if(number > carsSoldDs - 10){
                        number = carsSoldDs - 10;
                    }
                    printNumber();
                }else{
                    clearInterval(countInterval);
                    countLast10();
                }
            }, 0);
        }, 1500);
    };

    var getDealerSocketCarsSold = function(){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/cars-sold-out-of-dealersocket.php', true);
        xhr.send();
        xhr.onload = function(){
            var json = JSON.parse(xhr.response);
            carsSoldDs = Math.round(json.dealerSocketSales);
            soldPerMinute = json.carsSoldPerMinute;
            countUp();
        };
    }();
};
