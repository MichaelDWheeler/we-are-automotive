    var drawLineChartNoFill = function(){
        $('#line-chart-no-fill').sparkline(new7days, {
            type: 'line',
            width: $('#line-chart-no-fill').width(),
            height: '250',
            margin: '0 auto 0 auto',
            chartRangeMax: 20,
            lineColor: '#3bafda',
            fillColor: 'transparent',
            highlightLineColor: 'rgba(0,0,0,.1)',
            highlightSpotColor: 'rgba(0,0,0,.2)'
        });

        $('#line-chart-no-fill').sparkline(used7days, {
           type: 'line',
           width: $('#line-chart-no-fill').width(),
           height: '250',
           chartRangeMax: 20,
           lineColor: '#fff',
           fillColor: 'transparent',
           composite: true,
           highlightLineColor: 'rgba(0,0,0,1)',
           highlightSpotColor: 'rgba(0,0,0,1)'
       });
       $('#line-chart-no-fill').off();
    };

    var new7days = [];
    var used7days = [];
    
    var getNewVsUsedData = function(){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/new-vs-used.php', true);
        xhr.send();
        xhr.onload = function(){
            var json = JSON.parse(xhr.response);
            new7days = json[0];
            used7days = json[1];
            var arrayLength = new7days.length;
            drawLineChartNoFill();
            var newCarsSoldTotal =0;
            var usedCarSoldTotal = 0;
            for(var i = 0; i < arrayLength;i++){
                newCarsSoldTotal += parseInt(new7days[i]);
                usedCarSoldTotal += parseInt(used7days[i]);
            }
            $('.new-sold').text(newCarsSoldTotal);
            $('.used-sold').text(usedCarSoldTotal);
        };
    };
