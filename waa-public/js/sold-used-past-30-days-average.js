var soldUsedPast30DaysAverage = function () {

    var soldUsedColorMap = [];
    var yellowColumn = 0;
    var pinkColumn = 1;
    var tealColumn = 2;

    var createSoldUsedColorMap = function () {
        for (var i = 0; i <= 30; i++) {
            soldUsedColorMap.push('#3bafda');
        }
    }();

    var changeSoldUsedBarColors = function () {
        var defaultSoldUsedColorMap = soldUsedColorMap.slice();
        defaultSoldUsedColorMap[yellowColumn] = "#ffaa00";
        defaultSoldUsedColorMap[pinkColumn] = "#f76397";
        defaultSoldUsedColorMap[tealColumn] = "#00b19d";
        return (defaultSoldUsedColorMap);
    };



    var soldUsedPast30DaysAverage = function (arr, arrNum) {
        if (yellowColumn >= 31) {
            yellowColumn = 0;
        }
        if (pinkColumn >= 31) {
            pinkColumn = 0;
        }
        if (tealColumn >= 31) {
            tealColumn = 0;
        }
        $('.date-used-1').text(arr[yellowColumn]);
        $('.date-used-1').css('color', '#ffaa00');
        $('.date-used-1-amount').text(arrNum[yellowColumn]);
        $('.date-used-2').text(arr[pinkColumn]);
        $('.date-used-2').css('color', '#f76397');
        $('.date-used-2-amount').text(arrNum[pinkColumn]);
        $('.date-used-3').text(arr[tealColumn]);
        $('.date-used-3').css('color', '#00b19d');
        $('.date-used-3-amount').text(arrNum[tealColumn]);

        var soldUsedPast30DaysTimer = window.setTimeout(function () {
            ++yellowColumn;
            ++pinkColumn;
            ++tealColumn;
            soldUsedPast30DaysAverage(arr, arrNum);
            drawSoldUsedChart(arrNum);
        }, 5000);
    };

    var getSoldUsedPast30DaysAvergage = function () {
        var arr = [];
        var arrNum = [];

        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/target-bar-line.php', true);
        xhr.send();
        xhr.onload = function () {
            json = JSON.parse(xhr.response);
            jsonLength = json[0].length;
            for (var i = 0; i < jsonLength; i++) {
                arr.push(json[0][i]);
                arrNum.push(json[1][i]);
            }
            soldUsedPast30DaysAverage(arr, arrNum);
            $('#target-bar-line').sparkline(arrNum, soldUsedProp);
        };
    };

    var soldUsedProp = {
        type: 'bar',
        height: '250',
        barWidth: '10',
        barSpacing: '3',
        chartRangeMin: '0',
        colorMap: changeSoldUsedBarColors()
    };

    var drawSoldUsedChart = function (arrNum) {
        soldUsedProp.colorMap = changeSoldUsedBarColors();
        $('#target-bar-line').sparkline(arrNum, soldUsedProp);
    };
    getSoldUsedPast30DaysAvergage();
};