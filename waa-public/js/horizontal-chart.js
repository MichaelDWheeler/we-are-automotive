var backProfit = [];
var frontProfit = [];

var dataHorizontal = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September', 'October', 'November', 'December'],
    series: []
};

var propertiesHorizontal = {
    seriesBarDistance: 10,
    reverseData: true,
    horizontalBars: true,
    height: '420px',
    axisY: {
        offset: 70
    }
};

var frontBackProfit = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/front-back-profit.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        backProfit = json[0];
        frontProfit = json[1];
        dataHorizontal.series[0] = backProfit;
        dataHorizontal.series[1] = frontProfit;
        new Chartist.Bar('#horizontal-bar-chart', dataHorizontal, propertiesHorizontal);
    };
};
