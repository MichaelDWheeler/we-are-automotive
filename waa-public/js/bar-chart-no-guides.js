var colorMap2 = [];

var createColorMap2 = function () {
    for (var i = 0; i <= 30; i++) {
        colorMap2.push('#3bafda');
    }
}();

var changeBarDateColors = function (var1, var2, var3) {
    var defaultColorMap2 = colorMap2.slice();
    defaultColorMap2[var1] = "#ffaa00";
    defaultColorMap2[var2] = "#f76397";
    defaultColorMap2[var3] = "#00b19d";
    return (defaultColorMap2);
};

var displayBarChartData = function (element, arr, arrNum, dateElement, var1, var2, var3) {
    if (var1 >= 31) {
        var1 = 0;
    }
    if (var2 >= 31) {
        var2 = 0;
    }
    if (var3 >= 31) {
        var3 = 0;
    }
    $('.' + dateElement + '-1').text(arr[var1]);
    $('.' + dateElement + '-1').css('color', '#ffaa00');
    $('.' + dateElement + '-1-amount').text(arrNum[var1]);
    $('.' + dateElement + '-2').text(arr[var2]);
    $('.' + dateElement + '-2').css('color', '#f76397');
    $('.' + dateElement + '-2-amount').text(arrNum[var2]);
    $('.' + dateElement + '-3').text(arr[var3]);
    $('.' + dateElement + '-3').css('color', '#00b19d');
    $('.' + dateElement + '-3-amount').text(arrNum[var3]);
    
   var barChartNoGuidesTimer = window.setTimeout(function () {
            ++var1;
            ++var2;
            ++var3;
            displayBarChartData(element, arr, arrNum, dateElement, var1, var2, var3);
            drawBarChart2(element, arrNum, var1, var2, var3);
    }, 5000);
};

var getBarChartNoGuidesData = function (url, element, dateElement, var1, var2, var3) {
    var arr = [],
        arrNum = [];

    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.send();
    xhr.onload = function () {
        json = JSON.parse(xhr.response);
        jsonLength = json[0].length;
        for (var i = 0; i < jsonLength; i++) {
            arr.push(json[0][i]);
            arrNum.push(json[1][i]);
        }
        displayBarChartData(element, arr, arrNum, dateElement, var1, var2, var3);
        $(element).sparkline(arrNum, bc2prop);
    };
};

var bc2prop = {
    type: 'bar',
    height: '250',
    barWidth: '10',
    barSpacing: '3',
    chartRangeMin: '0',
    colorMap: changeBarDateColors()
};

var drawBarChart2 = function (element, arrNum, var1, var2, var3) {
    bc2prop.colorMap = changeBarDateColors(var1, var2, var3);
    $(element).sparkline(arrNum, bc2prop);
};
