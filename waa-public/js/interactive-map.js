var hideTitle = false;

if($('#interactive-map').length){
var interactiveMap = echarts.init(document.getElementById('interactive-map'));
var dontSlowScroll = 0;


option = {
    //    backgroundColor: '#12151c',
    color: ['gold', 'aqua', 'lime'],
    title: {
        text: 'COLLECTING DATA FROM DEALERSOCKET LOCATIONS',
        subtext: '',
        x: 'left',
        textStyle: {
            fontWeight: 'normal',
            fontSize: '14',
            color: '#fff'
        }
    },
    tooltip: {
        trigger: 'item',
        formatter: '{b}'
    },
    legend: {
        orient: 'vertical',
        x: 'right',
        data: ['DEALERSOCKET DATA USAGE BY STATE', 'DEALERSOCKET DATA RESOURCES', 'DATA USAGE AND COLLECTION'],
        selectedMode: 'single',
        selected: {
            'DEALERSOCKET DATA USAGE BY STATE' : false,
            'DEALERSOCKET DATA RESOURCES': false,
            'DATA USAGE AND COLLECTION': false
        },
        textStyle: {
            color: '#fff'
        }
    },
    toolbox: {
        show: true,
        orient: 'horizontal',
        x: 'right',
        y: 'top',
        feature: {
            restore: { show: false, title: 'Refresh' },
            //magicType: { show: true, type: ['force', 'chord'] },
            includeDealership: { show: false, title: 'Include Your Dealership' }
        }
    },
    dataRange: {
        min: 0,
        max: 100,
        calculable: true,
        color: ['#ff3333', 'orange', 'yellow', 'lime', 'aqua'],
        textStyle: {
            color: '#fff'
        }
    },
    series: [
        {
            name: 'USA',
            type: 'map',
            roam: false,
            hoverable: false,
            mapType: 'northAmerica',
            itemStyle: {
                normal: {
                    borderColor: 'rgba(100,149,237,1)',
                    borderWidth: 0.5,
                    areaStyle: {
                        color: '#12151c'
                    }
                }
            },
            data: [],
            markLine: {
                smooth: true,
                symbol: ['none', 'circle'],
                symbolSize: 1,
                itemStyle: {
                    normal: {
                        color: '#fff',
                        borderWidth: 1,
                        borderColor: 'rgba(30,144,255,0.5)'
                    }
                },
                data: [],
            },
            geoCoord: {
                // controls the transfer indicators
                'Alabama': [-86.90229799999997, 32.3182314],
                'Alaska': [-149.4936733, 64.2008413],
                'Alberta': [-116.5765035, 53.9332706],
                'Arizona': [-111.09373110000001, 34.0489281],
                'Arkansas': [-91.8318334, 35.20105],
                'British Columbia': [-127.64762059999998, 53.7266683],
                'California': [-119.41793239999998, 36.778261],
                'Colorado': [-105.78206740000002, 39.5500507],
                'Connecticut': [-73.08774900000003, 41.6032207],
                'Delaware': [-75.52766989999998, 38.9108325],
                'Florida': [-81.51575350000002, 27.6648274],
                'Georgia': [-82.90007509999998, 32.1656221],
                'Guam': [144.79373099999998, 13.444304],
                'Hawaii': [-155.58278180000002, 19.8967662],
                'Idaho': [-114.74204079999998, 44.0682019],
                'Illinois': [-89.39852830000001, 40.6331249],
                'Indiana': [-86.13490189999999, 40.2671941],
                'Iowa': [-93.09770200000003, 41.8780025],
                'Kansas': [-98.48424649999998, 39.011902],
                'Kentucky': [-84.27001789999997, 37.8393332],
                'Louisiana': [-91.96233269999999, 30.9842977],
                'Maine': [-69.44546889999998, 45.253783],
                'Manitoba': [-98.8138763, 53.7608608],
                'Maryland': [-76.6412712, 39.0457549],
                'Massachusetts': [-71.38243740000001, 42.4072107],
                'Michigan': [-85.60236429999998, 44.3148443],
                'Minnesota': [-94.68589980000002, 46.729553],
                'Mississippi': [-89.39852830000001, 32.3546679],
                'Missouri': [-91.8318334, 37.9642529],
                'Montana': [-110.36256579999997, 46.8796822],
                'Nebraska': [-99.90181310000003, 41.4925374],
                'Nevada': [-116.41938900000002, 38.8026097],
                'New Brunswick': [-74.45181880000001, 40.4862157],
                'New Hampshire': [-71.57239529999998, 43.1938516],
                'New Jersey': [-74.4056612, 40.0583238],
                'New Mexico': [-105.87009009999997, 34.5199402],
                'New York': [-74.00594130000002, 40.7127837],
                'Newfoundland and Labrador': [-57.66043639999998, 53.1355091],
                'North Carolina': [-79.01929969999998, 35.7595731],
                'North Dakota': [-101.00201190000001, 47.5514926],
                'Nova Scotia': [-63.74431100000004, 44.68198659999999],
                'Ohio': [-82.90712300000001, 40.4172871],
                'Oklahoma': [-97.09287699999999, 35.0077519],
                'Ontario': [-117.65088760000003, 34.0633443],
                'Oregon': [-120.55420119999997, 43.8041334],
                'Pennsylvania': [-77.19452469999999, 41.2033216],
                'Prince Edward Island': [-63.416813599999955, 46.510712],
                'Québec': [-73.5491361, 52.9399159],
                'Queensland, Australia': [142.70279559999994, -20.9175738],
                'Rhode Island': [-71.4774291, 41.5800945],
                'Saskatchewan': [-106.4508639, 52.9399159],
                'South Carolina': [-81.1637245, 33.836081],
                'South Dakota': [-99.90181310000003, 43.9695148],
                'Tennessee': [-86.5804473, 35.5174913],
                'Texas': [-99.90181310000003, 31.9685988],
                'Utah': [-111.09373110000001, 39.3209801],
                'Vermont': [-72.57784149999998, 44.5588028],
                'Virginia': [-78.65689420000001, 37.4315734],
                'Washington': [-77.03687070000001, 38.9071923],
                'West Virginia': [-80.45490259999997, 38.5976262],
                'Wisconsin': [-88.78786780000001, 43.7844397],
                'Dallas, TX': [-96.79698789999998, 32.7766642],
                'Draper UT': [-111.86382259999999, 40.5246711],
                'Wyoming': [-107.29028390000002, 43.0759678],
                'Houston, TX': [-95.3698028, 29.7604267],
                'Jackson, MS' :[-90.18481029999998, 32.2987573],
                'Nashville, TN': [-86.78160159999999, 36.1626638],
                'Oshkosh, WI': [-88.54261359999998, 44.0247062],
                'Provo, UT': [-111.65853370000002, 40.2338438],
                'Salt Lake City, UT': [-111.89104739999999, 40.7607793],
                'San Clemente, CA': [-117.611992, 33.426971],
                'Fort Worth, TX' : [-97.3307658, 32.7554883]
            }
        },
        {
            name: 'DEALERSOCKET DATA USAGE BY STATE',
            type: 'map',
            mapType: 'northAmerica',
            data: [],
            markLine: {
                smooth: true,
                effect: {
                    show: true,
                    scaleSize: 1,
                    period: 30,
                    color: '#fff',
                    shadowBlur: 10
                },
                itemStyle: {
                    normal: {
                        borderWidth: 1,
                        lineStyle: {
                            type: 'solid',
                            shadowBlur: 10
                        }
                    }
                },
                data: [
                    [{ name: 'San Clemente, CA' }, { name: 'Alabama', value: 73 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Alaska', value: 7 }],
                    [{ name: 'Draper UT' }, { name: 'Alberta', value: 70 }],
                    [{ name: 'Dallas, TX' }, { name: 'Arizona', value: 135 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Arkansas', value: 69 }],
                    [{ name: 'Draper UT' }, { name: 'British Columbia', value: 48 }],
                    [{ name: 'San Clemente, CA' }, { name: 'California', value: 343 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Colorado', value: 89 }],
                    [{ name: 'Dallas, TX' }, { name: 'Connecticut', value: 35 }],
                    [{ name: 'Dallas, TX' }, { name: 'Delaware', value: 20 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Florida', value: 432 }],
                    [{ name: 'Draper UT' }, { name: 'Georgia', value: 177 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Guam', value: 1 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Hawaii', value: 25 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Idaho', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Illinois', value: 189 }],
                    [{ name: 'Salt Lake City, UT' },{ name: 'Indiana', value: 90 }],
                    [{ name: 'Houston, TX' }, { name: 'Iowa', value: 34 }],
                    [{ name: 'Dallas, TX' }, { name: 'Kansas', value: 34 }],
                    [{ name: 'Dallas, TX' }, { name: 'Kentucky', value: 59 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Alabama', value: 135 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Alaska', value: 90 }],
                    [{ name: 'Draper UT' }, { name: 'Arizona', value: 80 }],
                    [{ name: 'Dallas, TX' }, { name: 'California', value: 356 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Connecticut', value: 73 }],
                    [{ name: 'Draper UT' }, { name: 'Delaware', value: 15 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Florida', value: 40 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Georgia', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Hawaii', value: 20 }],
                    [{ name: 'Dallas, TX' }, { name: 'Idaho', value: 10 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Illinois', value: 55 }],
                    [{ name: 'Draper UT' }, { name: 'Indiana', value: 20 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Iowa', value: 40 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Kansas', value: 25 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Kentucky', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Louisiana', value: 69 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Maine', value: 5 }],
                    [{ name: 'Houston, TX' }, { name: 'Manitoba', value: 22 }],
                    [{ name: 'Dallas, TX' }, { name: 'Maryland', value: 125 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Massachusetts', value: 68 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Minnesota', value: 25 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Mississippi', value: 39 }],
                    [{ name: 'Provo, UT' }, { name: 'Missouri', value: 92 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Montana', value: 7 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Nebraska', value: 26 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Nevada', value: 39 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New Hampshire', value: 19 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New Jersey', value: 202 }],
                    [{ name: 'Nashville, TN' }, { name: 'New Mexico', value: 49 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New York', value: 230 }],
                    [{ name: 'Provo, UT' }, { name: 'North Carolina', value: 148 }],
                    [{ name: 'San Clemente, CA' }, { name: 'North Dakota', value: 2 }],
                    [{ name: 'Jackson, MS' }, { name: 'Ohio', value: 155 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Oklahoma', value: 93 }],
                    [{ name: 'Jackson, MS' }, { name: 'Oregon', value: 43 }],
                    [{ name: 'Jackson, MS' }, { name: 'Newfoundland and Labrador', value: 5 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Nova Scotia', value: 17 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Ontario', value: 124 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Pennsylvania', value: 146 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Prince Edward Island', value: 2 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Québec', value: 98 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Queensland, Australia', value: 1 }],
                    [{ name: 'Jackson, MS' }, { name: 'Rhode Island', value: 18 }],
                    [{ name: 'Jackson, MS' }, { name: 'Saskatchewan', value: 23 }],
                    [{ name: 'San Clemente, CA' }, { name: 'South Carolina', value: 104 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'South Dakota', value: 6 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Tennessee', value: 117 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Texas', value: 1363 }],
                    [{ name: 'Nashville, TN' }, { name: 'Utah', value: 84 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Vermont', value: 4 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Virginia', value: 182 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Washington', value: 124 }],
                    [{ name: 'San Clemente, CA' }, { name: 'West Virginia', value: 28 }],
                    [{ name: 'Houston, TX' }, { name: 'Wisconsin', value: 45 }],
                    [{ name: 'Provo, UT' }, { name: 'Wyoming', value: 84 }]
                ]
            },
            markPoint: {
                symbol: 'emptyCircle',
                symbolSize: function (v) {
                    return 10 + v / 100;
                },
                effect: {
                    show: true,
                    shadowBlur: 0
                },
                itemStyle: {
                    normal: {
                        label: { show: false }
                    },
                    emphasis: {
                        label: { position: 'top' }
                    }
                },
                data: [
                    { name: 'Alabama', value: 73 },
                    { name: 'Alaska', value: 7 },
                    { name: 'Alberta', value: 70 },
                    { name: 'Arizona', value: 135 },
                    { name: 'Arkansas', value: 69 },
                    { name: 'British Columbia', value: 48 },
                    { name: 'California', value: 343 },
                    { name: 'Colorado', value: 89 },
                    { name: 'Connecticut', value: 35 },
                    { name: 'Delaware', value: 20 },
                    { name: 'Florida', value: 432 },
                    { name: 'Georgia', value: 177 },
                    { name: 'Guam', value: 1 },
                    { name: 'Hawaii', value: 25 },
                    { name: 'Idaho', value: 30 },
                    { name: 'Illinois', value: 189 },
                    { name: 'Indiana', value: 90 },
                    { name: 'Iowa', value: 34 },
                    { name: 'Kansas', value: 34 },
                    { name: 'Kentucky', value: 59},
                    { name: 'Louisiana', value: 69 },
                    { name: 'Maine', value: 5 },
                    { name: 'Manitoba', value: 22 },
                    { name: 'Maryland', value: 125 },
                    { name: 'Massachusetts', value: 68 },
                    { name: 'Michigan', value: 77 },
                    { name: 'Minnesota', value: 25 },
                    { name: 'Mississippi', value: 39 },
                    { name: 'Missouri', value: 92 },
                    { name: 'Montana', value: 7 },
                    { name: 'Nebraska', value: 26 },
                    { name: 'Nevada', value: 39 },
                    { name: 'New Brunswick', value: 15 },
                    { name: 'New Hampshire', value: 19 },
                    { name: 'New Jersey', value: 202 },
                    { name: 'New Mexico', value: 49 },
                    { name: 'New York', value: 230 },
                    { name: 'Newfoundland and Labrador', value: 5 },
                    { name: 'North Carolina', value: 148 },
                    { name: 'Nova Scotia', value: 17 },
                    { name: 'Ohio', value: 155 },
                    { name: 'Oklahoma', value: 93 },
                    { name: 'Ontario', value: 124 },
                    { name: 'Oregon', value: 43 },
                    { name: 'Pennsylvania', value: 146 },
                    { name: 'Prince Edward Island', value: 2 },
                    { name: 'Québec', value: 98 },
                    { name: 'Queensland, Australia', value: 1 },
                    { name: 'Rhode Island', value: 18 },
                    { name: 'Saskatchewan', value: 23 },
                    { name: 'South Carolina', value: 104 },
                    { name: 'Tennessee', value: 117 },
                    { name: 'Texas', value: 1363 },
                    { name: 'Utah', value: 84 },
                    { name: 'Vermont', value: 4 },
                    { name: 'Virginia', value: 182 },
                    { name: 'Washington', value: 124 },
                    { name: 'West Virginia', value: 28 },
                    { name: 'Wisconsin', value: 45 }
                ]
            }
        },
        {
            name: 'DEALERSOCKET DATA RESOURCES',
            type: 'map',
            mapType: 'northAmerica',
            data: [],
            markLine: {
                smooth: true,
                effect: {
                    show: true,
                    scaleSize: 5,
                    period: 30,
                    color: '#fff',
                    shadowBlur: 5
                },
                itemStyle: {
                    normal: {
                        borderWidth: 1,
                        lineStyle: {
                            type: 'solid',
                            shadowBlur: 10
                        }
                    }
                },
                data: [
                    [{ name: 'Alabama' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Alaska' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Alberta' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Arizona' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Arkansas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'British Columbia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'California' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Colorado' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Connecticut' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Delaware' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Florida' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Georgia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Guam' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Hawaii' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Idaho' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Illinois' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Indiana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Iowa' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Kansas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Kentucky' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Louisiana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Maine' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Manitoba' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Maryland' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Massachusetts' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Michigan' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Minnesota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Mississippi' },{ name: 'San Clemente, CA' }],
                    [{ name: 'Missouri' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Montana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nebraska' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nevada' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Brunswick' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Hampshire' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Jersey' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Mexico' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New York' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Newfoundland and Labrador' }, { name: 'San Clemente, CA' }],
                    [{ name: 'North Carolina' }, { name: 'San Clemente, CA' }],
                    [{ name: 'North Dakota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nova Scotia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Ohio' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Oklahoma' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Ontario' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Oregon' },{ name: 'San Clemente, CA' }],
                    [{ name: 'Pennsylvania' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Prince Edward Island' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Québec' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Queensland, Australia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Rhode Island' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Saskatchewan' }, { name: 'San Clemente, CA' }],
                    [{ name: 'South Carolina' }, { name: 'San Clemente, CA' }],
                    [{ name: 'South Dakota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Tennessee' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Texas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Utah' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Vermont' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Virginia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Washington' }, { name: 'San Clemente, CA' }],
                    [{ name: 'West Virginia' }, { name: 'San Clemente, CA' }]
                ]
            },
            markPoint: {
                symbol: 'emptyCircle',
                symbolSize: function (v) {
                    return 10 + v / 10;
                },
                effect: {
                    show: true,
                    shadowBlur: 0
                },
                itemStyle: {
                    normal: {
                        label: { show: false }
                    },
                    emphasis: {
                        label: { position: 'top' }
                    }
                },
                data: []
            }
        },
        {
            name: 'DATA USAGE AND COLLECTION',
            type: 'map',
            mapType: 'northAmerica',
            data: [],
            markLine: {
                smooth: true,
                effect: {
                    show: true,
                    scaleSize: 1,
                    period: 30,
                    color: '#fff',
                    shadowBlur: 10
                },
                itemStyle: {
                    normal: {
                        borderWidth: 1,
                        lineStyle: {
                            type: 'solid',
                            shadowBlur: 10
                        }
                    }
                },
                data: [
                    [{ name: 'San Clemente, CA' }, { name: 'Alabama', value: 73 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Alaska', value: 7 }],
                    [{ name: 'Draper UT' }, { name: 'Alberta', value: 70 }],
                    [{ name: 'Dallas, TX' }, { name: 'Arizona', value: 135 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Arkansas', value: 69 }],
                    [{ name: 'Draper UT' }, { name: 'British Columbia', value: 48 }],
                    [{ name: 'San Clemente, CA' }, { name: 'California', value: 343 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Colorado', value: 89 }],
                    [{ name: 'Dallas, TX' }, { name: 'Connecticut', value: 35 }],
                    [{ name: 'Dallas, TX' }, { name: 'Delaware', value: 20 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Florida', value: 432 }],
                    [{ name: 'Draper UT' }, { name: 'Georgia', value: 177 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Guam', value: 1 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Hawaii', value: 25 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Idaho', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Illinois', value: 189 }],
                    [{ name: 'Salt Lake City, UT' },{ name: 'Indiana', value: 90 }],
                    [{ name: 'Houston, TX' }, { name: 'Iowa', value: 34 }],
                    [{ name: 'Dallas, TX' }, { name: 'Kansas', value: 34 }],
                    [{ name: 'Dallas, TX' }, { name: 'Kentucky', value: 59 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Alabama', value: 135 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Alaska', value: 90 }],
                    [{ name: 'Draper UT' }, { name: 'Arizona', value: 80 }],
                    [{ name: 'Dallas, TX' }, { name: 'California', value: 356 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Connecticut', value: 73 }],
                    [{ name: 'Draper UT' }, { name: 'Delaware', value: 15 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Florida', value: 40 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Georgia', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Hawaii', value: 20 }],
                    [{ name: 'Dallas, TX' }, { name: 'Idaho', value: 10 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Illinois', value: 55 }],
                    [{ name: 'Draper UT' }, { name: 'Indiana', value: 20 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Iowa', value: 40 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Kansas', value: 25 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Kentucky', value: 30 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Louisiana', value: 69 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Maine', value: 5 }],
                    [{ name: 'Houston, TX' }, { name: 'Manitoba', value: 22 }],
                    [{ name: 'Dallas, TX' }, { name: 'Maryland', value: 125 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Massachusetts', value: 68 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Minnesota', value: 25 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Mississippi', value: 39 }],
                    [{ name: 'Provo, UT' }, { name: 'Missouri', value: 92 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Montana', value: 7 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Nebraska', value: 26 }],
                    [{ name: 'Fort Worth, TX' }, { name: 'Nevada', value: 39 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New Hampshire', value: 19 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New Jersey', value: 202 }],
                    [{ name: 'Nashville, TN' }, { name: 'New Mexico', value: 49 }],
                    [{ name: 'San Clemente, CA' }, { name: 'New York', value: 230 }],
                    [{ name: 'Provo, UT' }, { name: 'North Carolina', value: 148 }],
                    [{ name: 'San Clemente, CA' }, { name: 'North Dakota', value: 2 }],
                    [{ name: 'Jackson, MS' }, { name: 'Ohio', value: 155 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Oklahoma', value: 93 }],
                    [{ name: 'Jackson, MS' }, { name: 'Oregon', value: 43 }],
                    [{ name: 'Jackson, MS' }, { name: 'Newfoundland and Labrador', value: 5 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Nova Scotia', value: 17 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Ontario', value: 124 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Pennsylvania', value: 146 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Prince Edward Island', value: 2 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Québec', value: 98 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Queensland, Australia', value: 1 }],
                    [{ name: 'Jackson, MS' }, { name: 'Rhode Island', value: 18 }],
                    [{ name: 'Jackson, MS' }, { name: 'Saskatchewan', value: 23 }],
                    [{ name: 'San Clemente, CA' }, { name: 'South Carolina', value: 104 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'South Dakota', value: 6 }],
                    [{ name: 'Salt Lake City, UT' }, { name: 'Tennessee', value: 117 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Texas', value: 1363 }],
                    [{ name: 'Nashville, TN' }, { name: 'Utah', value: 84 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Vermont', value: 4 }],
                    [{ name: 'San Clemente, CA' }, { name: 'Virginia', value: 182 }],
                    [{ name: 'Oshkosh, WI' }, { name: 'Washington', value: 124 }],
                    [{ name: 'San Clemente, CA' }, { name: 'West Virginia', value: 28 }],
                    [{ name: 'Houston, TX' }, { name: 'Wisconsin', value: 45 }],
                    [{ name: 'Provo, UT' }, { name: 'Wyoming', value: 84 }],
                    [{ name: 'Alabama' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Alaska' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Alberta' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Arizona' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Arkansas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'British Columbia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'California' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Colorado' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Connecticut' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Delaware' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Florida' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Georgia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Guam' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Hawaii' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Idaho' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Illinois' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Indiana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Iowa' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Kansas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Kentucky' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Louisiana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Maine' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Manitoba' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Maryland' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Massachusetts' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Michigan' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Minnesota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Mississippi' },{ name: 'San Clemente, CA' }],
                    [{ name: 'Missouri' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Montana' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nebraska' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nevada' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Brunswick' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Hampshire' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Jersey' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New Mexico' }, { name: 'San Clemente, CA' }],
                    [{ name: 'New York' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Newfoundland and Labrador' }, { name: 'San Clemente, CA' }],
                    [{ name: 'North Carolina' }, { name: 'San Clemente, CA' }],
                    [{ name: 'North Dakota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Nova Scotia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Ohio' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Oklahoma' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Ontario' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Oregon' },{ name: 'San Clemente, CA' }],
                    [{ name: 'Pennsylvania' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Prince Edward Island' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Québec' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Queensland, Australia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Rhode Island' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Saskatchewan' }, { name: 'San Clemente, CA' }],
                    [{ name: 'South Carolina' }, { name: 'San Clemente, CA' }],
                    [{ name: 'South Dakota' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Tennessee' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Texas' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Utah' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Vermont' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Virginia' }, { name: 'San Clemente, CA' }],
                    [{ name: 'Washington' }, { name: 'San Clemente, CA' }],
                    [{ name: 'West Virginia' }, { name: 'San Clemente, CA' }]
                ]
            }
        }
    ]
};

var locationStorage = option.series[0].markLine.data;
// option.series[0].markLine.data = [];

var drawMap = function(option){
    interactiveMap.clear();
    interactiveMap.setOption(option);
};

var toggleMaps = function(option){
    option.legend.selected = {
        'DEALERSOCKET DATA USAGE BY STATE' : true,
        'DEALERSOCKET DATA RESOURCES': false,
        'DATA USAGE AND COLLECTION': false
    };
    if(hideTitle === false){
        option.title.text = "DEALERSOCKET DATA USAGE BY STATE";
    }else{
        option.title.text = "";
    }
    if(stopDrawing === false){
        drawMap(option);
    }
    resourcesTimer = window.setTimeout(function(){
        option.legend.selected = {
            'DEALERSOCKET DATA USAGE BY STATE' : false,
            'DEALERSOCKET DATA RESOURCES': true,
            'DATA USAGE AND COLLECTION': false
        };

        if(hideTitle === false){
            option.title.text = "DEALERSOCKET DATA RESOURCES";
        }else{
            option.title.text = "";
        }

        if(stopDrawing === false){
            drawMap(option);
        }

        combinedTimer = window.setTimeout(function(){
            option.legend.selected = {
                'DEALERSOCKET DATA USAGE BY STATE' : false,
                'DEALERSOCKET DATA RESOURCES': false,
                'DATA USAGE AND COLLECTION': true
            };
            if(hideTitle === false){
                option.title.text = "DATA USAGE AND COLLECTION";
            }else{
                option.title.text = "";
            }
            if(stopDrawing === false){
                drawMap(option);
            }
        },10000);
    }, 10000);
};

var showTitleCheck = function(){
    var width = $(document).width();
    if (width < 1515){
        hideTitle = true;
        option.title.text = "";
    }
}();

var stopDrawing = false;
$('#interactive-map').click(function(){
    if(catagoryClicked === 'DEALERSOCKET DATA USAGE BY STATE'){
        option.legend.selected = {
            'DEALERSOCKET DATA USAGE BY STATE' : true,
            'DEALERSOCKET DATA RESOURCES': false,
            'DATA USAGE AND COLLECTION': false
        };
        if(hideTitle === false){
            option.title.text = "DEALERSOCKET DATA USAGE BY STATE";
        }else{
            option.title.text = "";
        }
        drawMap(option);
        stopDrawing = true;
    }
    if(catagoryClicked === 'DEALERSOCKET DATA RESOURCES'){
        option.legend.selected = {
            'DEALERSOCKET DATA USAGE BY STATE' : false,
            'DEALERSOCKET DATA RESOURCES': true,
            'DATA USAGE AND COLLECTION': false
        };
        if(hideTitle === false){
            option.title.text = "DEALERSOCKET DATA RESOURCES";
        }else{
            option.title.text = "";
        }
        drawMap(option);
        stopDrawing = true;
    }
    if(catagoryClicked === 'DATA USAGE AND COLLECTION'){
        option.legend.selected = {
            'DEALERSOCKET DATA USAGE BY STATE' : false,
            'DEALERSOCKET DATA RESOURCES': false,
            'DATA USAGE AND COLLECTION': true
        };
        if(hideTitle === false){
            option.title.text = "DATA USAGE AND COLLECTION";
        }else{
            option.title.text = "";
        }
        drawMap(option);
        stopDrawing = true;
    }
});

var mapTimer = 30000;

var delayTimer = window.setTimeout(function(){
    option.series[0].markLine.data = [
        [{ name: 'Dallas, TX' }, { name: 'San Clemente, CA' }],
        [{ name: 'Draper UT' }, { name: 'San Clemente, CA' }],
        [{ name: 'Fort Worth, TX' }, { name: 'San Clemente, CA' }],
        [{ name: 'Houston, TX' }, { name: 'San Clemente, CA' }],
        [{ name: 'Jackson, MS' }, { name: 'San Clemente, CA' }],
        [{ name: 'Nashville, TN' }, { name: 'San Clemente, CA' }],
        [{ name: 'Oshkosh, WI' }, { name: 'San Clemente, CA' }],
        [{ name: 'Provo, UT' }, { name: 'San Clemente, CA' }],
        [{ name: 'Salt Lake City, UT' },{ name: 'San Clemente, CA' }],
        [{ name: 'San Clemente, CA' }, { name: 'Draper UT' }],
        [{ name: 'San Clemente, CA' }, { name: 'Fort Worth, TX' }],
        [{ name: 'San Clemente, CA' }, { name: 'Houston, TX' }],
        [{ name: 'San Clemente, CA' }, { name: 'Jackson, MS' }],
        [{ name: 'San Clemente, CA' }, { name: 'Nashville, TN' }],
        [{ name: 'San Clemente, CA' }, { name: 'Oshkosh, WI' }],
        [{ name: 'San Clemente, CA' }, { name: 'Provo, UT' }],
        [{ name: 'San Clemente, CA' }, { name: 'Salt Lake City, UT' }]
    ];
    interactiveMap.setOption(option);

    var delayTimer2 = window.setTimeout(function(){
        option.series[0].markLine.data = [];
        toggleMaps(option);
        var animationSlowsScrollTimer = window.setInterval(function(){
            if (dontSlowScroll === 30){
                dontSlowScroll = 0;
            }
            dontSlowScroll++;
        }, 1000);
    },500);
    var mapChange = window.setInterval(function(){
        if(stopDrawing === false){
            toggleMaps(option);

        }
    },30000);
}, 7000);

interactiveMap.setOption(option);
}
