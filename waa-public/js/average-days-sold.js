var MorrisLineChart = function() {};
var dataImported;

    //creates line chart
    MorrisLineChart.prototype.createLineChart = function(element, data, xkey, ykeys, labels, opacity, Pfillcolor, Pstockcolor, lineColors) {
        Morris.Line({
          element: element,
          data: data,
          xkey: xkey,
          ykeys: ykeys,
          labels: labels,
          fillOpacity: opacity,
          pointFillColors: Pfillcolor,
          pointStrokeColors: Pstockcolor,
          behaveLikeLine: true,
          gridLineColor: '#262c3b',
          hideHover: 'auto',
          resize: true, //defaulted to true
          lineColors: lineColors,
          gridTextColor: '#fff'

        });
    },
    MorrisLineChart.prototype.init = function() {
        //create line chart
        var $data  = dataImported;
        this.createLineChart('average-days-sold', $data, 'y', ['phone', 'freshUp', 'internet'], ['Phone', 'Fresh Up', 'Internet'], ['0.1'], ['#ffffff'], ['#999999'], ["#00b19d", "#64b5f6", "#dcdcdc"]);
    },
    //init
    $.MorrisLineChart = new MorrisLineChart, $.MorrisLineChart.Constructor = MorrisLineChart


//initializing

var getAverageSoldData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/average-days-sold.php', true);
    xhr.send();
    xhr.onload = function(){
        dataImported = JSON.parse(xhr.response);
        $.MorrisLineChart.init();
    };
};
