var soldNewPast30DaysAverage = function () {

    var soldNewColorMap = [];
    var yellowColumn = 0;
    var pinkColumn = 1;
    var tealColumn = 2;

    var createSoldNewColorMap = function () {
        for (var i = 0; i <= 30; i++) {
            soldNewColorMap.push('#3bafda');
        }
    }();

    var changeSoldNewBarColors = function () {
        var defaultsoldNewColorMap = soldNewColorMap.slice();
        defaultsoldNewColorMap[yellowColumn] = "#ffaa00";
        defaultsoldNewColorMap[pinkColumn] = "#f76397";
        defaultsoldNewColorMap[tealColumn] = "#00b19d";
        return (defaultsoldNewColorMap);
    };



    var soldNewPast30DaysAverage = function (arr, arrNum) {
        if (yellowColumn >= 31) {
            yellowColumn = 0;
        }
        if (pinkColumn >= 31) {
            pinkColumn = 0;
        }
        if (tealColumn >= 31) {
            tealColumn = 0;
        }
        $('.date-display-1').text(arr[yellowColumn]);
        $('.date-display-1').css('color', '#ffaa00');
        $('.date-display-1-amount').text(arrNum[yellowColumn]);
        $('.date-display-2').text(arr[pinkColumn]);
        $('.date-display-2').css('color', '#f76397');
        $('.date-display-2-amount').text(arrNum[pinkColumn]);
        $('.date-display-3').text(arr[tealColumn]);
        $('.date-display-3').css('color', '#00b19d');
        $('.date-display-3-amount').text(arrNum[tealColumn]);

        var soldNewPast30DaysTimer = window.setTimeout(function () {
            ++yellowColumn;
            ++pinkColumn;
            ++tealColumn;
            soldNewPast30DaysAverage(arr, arrNum);
            drawSoldNewChart(arrNum);
        }, 5000);
    };

    var getSoldNewPast30DaysAvergage = function () {
        var arr = [];
        var arrNum = [];

        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/bar-chart-no-guides.php', true);
        xhr.send();
        xhr.onload = function () {
            json = JSON.parse(xhr.response);
            jsonLength = json[0].length;
            for (var i = 0; i < jsonLength; i++) {
                arr.push(json[0][i]);
                arrNum.push(json[1][i]);
            }
            soldNewPast30DaysAverage(arr, arrNum);
            $('#sold-new-past-30-days-average').sparkline(arrNum, soldNewProp);
        };
    };

    var soldNewProp = {
        type: 'bar',
        height: '250',
        barWidth: '10',
        barSpacing: '3',
        chartRangeMin: '0',
        colorMap: changeSoldNewBarColors()
    };

    var drawSoldNewChart = function (arrNum) {
        soldNewProp.colorMap = changeSoldNewBarColors();
        $('#sold-new-past-30-days-average').sparkline(arrNum, soldNewProp);
    };
    getSoldNewPast30DaysAvergage();
};