var dealerSocketPie = function(){
var dealersocketSold = echarts.init(document.getElementById('dealersocket-pie'));

var dealersocketPieDate = function(){
    var d = new Date().getFullYear();
    var year = d - 1;
    return year.toString();
};

var labelTopDS = {
    normal: {
        label: {
            show: true,
            position: 'center',
            formatter: '{b}',
            textStyle: {
                baseline: 'top',
                fontSize: '14'
            }
        },
        labelLine: {
            show: false
        }
    }
};
var labelDSFormatter = {
    normal: {
        label: {
            formatter: function (params) {
                return 100 - params.value + '%';
            },
            textStyle: {
                baseline: 'bottom',
                fontSize: '14',
            }
        }
    },
};
var labelDSBottom = {
    normal: {
        color: 'rgba(182, 194, 201, 0.6)',
        label: {
            show: true,
            position: 'center'
        },
        labelLine: {
            show: false
        }
    },
    emphasis: {
        color: 'rgba(0,0,0,0)'
    }
};
var dsRadius = [40, 50];

var setDSOptions = function(dealersocketSoldData){
    var dsData = dealersocketSoldData[0];

    optionDSPercentage = {
        color: ['#A4B0BC'],
        legend: {
            show: false,
            x: 'center',
            y: 'center',
            data: [
                'null'
            ],
            textStyle: {
                color: '#eee'
            }
        },
        title: {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                color: '#eee'
            }
        },
        toolbox: {
            show: true,
            feature: {
                dataView: { show: false, readOnly: false },
                magicType: {
                    show: false,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            width: '20%',
                            height: '30%',
                            itemStyle: {
                                normal: {
                                    label: {
                                        formatter: function (params) {
                                            return 'other\n' + params.value + '%\n';
                                        },
                                        textStyle: {
                                            baseline: 'middle'
                                        }
                                    }
                                },
                            }
                        }
                    }
                },
                restore: { show: false },
                includeDealership: { show: false }
            }
        },
        series: [
            {
                type: 'pie',
                center: ['50%', '50%'],
                radius: dsRadius,
                x: '0%', // for funnel
                itemStyle: labelDSFormatter,
                data: [
                    { name: 'other', value: 100 - dsData, itemStyle: labelDSBottom },
                    { name: dealersocketPieDate() + ' Sales', value: dsData, itemStyle: labelTopDS }
                ]
            }
        ]
    };
    dealersocketSold.setOption(optionDSPercentage);
};



var getPercentage = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/ds-sold-percentage.php');
    xhr.send();
    xhr.onload = function(){
        var dealersocketSoldData = JSON.parse(xhr.response);
        setDSOptions(dealersocketSoldData);

    };
}();

};
