var labelValues = [];
var costPerSold = [];
var totalProfit = [];


var dataLP = {
    labels: [],
    series: []
};

var propertiesLP = {
    height: '420px',
    axisX: {
        // On the x-axis start means top and end means bottom
        position: 'start'
    },
    axisY: {
        // On the y-axis start means left and end means right
        position: 'end'
    }
};


var checkMarketingContainer = function(json){
    if($('#chart-20').width() > 750){
        dataLP.labels = labelValues;
        dataLP.series[0] = costPerSold;
        dataLP.series[1] = totalProfit;
    }else if($('#chart-20').width() < 450){
        dataLP.labels = labelValues.slice(0,3);
        dataLP.series[0] = costPerSold.slice(0,3);
        dataLP.series[1] = totalProfit.slice(0,3);
    }else{
        dataLP.labels = labelValues.slice(0,5);
        dataLP.series[0] = costPerSold.slice(0,5);
        dataLP.series[1] = totalProfit.slice(0,5);
    }
    new Chartist.Bar('#label-placement-chart', dataLP, propertiesLP);
};

var marketingChannels = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/marketing-channels.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        labelValues = json[0];
        costPerSold = json[1];
        totalProfit = json[2];
        checkMarketingContainer(json);
    };
};
