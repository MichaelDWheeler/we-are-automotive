;
var carsSold = function (element) {
    'use strict';
    var number = 0;

    var numbersWithCommas = function (number) {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    var randomDelayResult;

    var randomDelay = function(){
        randomDelayResult = Math.random() * 5000 + 1000;
        return randomDelayResult;
    };

    var makeNewNumber = function (addNumber, el) {
        var targetNumber = number + addNumber;
        var makeNewNumberInterval = window.setInterval(function () {
            if (number < Math.round(targetNumber)) {
                ++number;
                printNumber(el);
            } else {
                number = targetNumber;
                window.clearInterval(makeNewNumberInterval);
            }
        }, 300);
    };

    var addCarsOverTime = function (json, el) {
        var numberOfCarsPerSecond = json.arr3 / 60;
        var addCarsOverTimeDelay = window.setTimeout(function () {
            var addNumber = (randomDelayResult * (numberOfCarsPerSecond) / 1000);
            makeNewNumber(addNumber, el);
            addCarsOverTime(json, el);
        }, randomDelay());
    };

    var printNumber = function (el) {
        $(el).text(numbersWithCommas(Math.round(number)));
    };

    var delay = 100;

    var countLast10 = function (json, el) {
        var countLast10Timer = window.setTimeout(function () {
            printNumber(el);
            if (number != json.arr1) {
                delay = delay + delay * 0.175;
                ++number;
                countLast10(json, el);
            } else {
                addCarsOverTime(json, el);
            }
        }, delay);
    };

    var countUp = function (json, el) {
        
        var countTimer = window.setTimeout(function () {
            var countInterval = window.setInterval(function () {
                if (number < json.arr1 - 10) {
                    number = number + Math.floor(Math.random() * 100000);
                    if (number > json.arr1 - 10) {
                        number = json.arr1 - 10;
                    }
                    printNumber(el);
                } else {
                    clearInterval(countInterval);
                    countLast10(json, el);
                }
            }, 0);
        }, 1500);
    };

    var getCarsSold = function (url, el) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.send();
        xhr.onload = function () {
            var json = JSON.parse(xhr.response);
            //arr1 = json.arr1;
            //arr2 = json.arr2;
            //arr3 = json.arr3;
            countUp(json, el);
        };
    };

    var carsSold, carsSoldElement;

    if (element === '#cars-sold-ytd-block') {
        carsSold = './php/cars-sold-ytd.php';
        carsSoldElement = '.ytd-sales-number';
        
    } else if (element === '#cars-sold-month-block') {
        carsSold = './php/cars-sold-month.php';
        carsSoldElement = '.month-sales-number';
        
    } else if (element === '#dealersocket-sales-block') {
        carsSold = './php/cars-sold-out-of-dealersocket.php';
        carsSoldElement = '.dealersocket-sales-number';

    }

    getCarsSold(carsSold, carsSoldElement);
};

