var usersNow,
    usersOnSiteTimer,
    usersOnSiteStorage = [],
    intervalRunning = false,
    data = [],
    totalPoints = 60;

var usersNowVariance = function(){
    var operator = Math.floor(Math.random() * 2) + 1;
    var changeByNumber = parseInt(Math.floor(Math.random() * (usersNow / 50)));
    if (operator === 1){
        usersNow += changeByNumber;
    }
    if(operator === 2){
        usersNow -= changeByNumber;
    }
    return usersNow;
};


var variance = function(){
    var total;
    var operator = Math.floor(Math.random() * 2) + 1;
    if (operator === 1){
        total = Math.floor(Math.random() * 200);
    }else{
        total = 0 - Math.floor(Math.random() * 200);
    }
    return total;
};

var getCurrentTime = function(){
    var baseUsers, additionalUsers, combined, adjusted,adjustment;
    var d = new Date();
    var n = d.getTimezoneOffset();
    var h = d.getUTCHours();
    var m = d.getMinutes();
    additionalUsers = Math.round(m * 0.8333);
    control = 1400;
    if(h === 0 || h === 20){
        baseUsers = 4250;
        combined = baseUsers + additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 1 || h === 19){
        baseUsers = 4000;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 2|| h === 18){
        baseUsers = 3750;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 3 || h === 17){
        baseUsers = 3500;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 4 || h === 16){
        baseUsers = 3250;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 5 || h === 15){
        baseUsers = 3000;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 6 || h === 14){
        baseUsers = 2750;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 7 || h === 13){
        baseUsers = 2500;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 8 || h === 12){
        baseUsers = 2250;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 9 || h === 11){
        baseUsers = 2000;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 10){
        baseUsers = 1750;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 21 || h == 23){
        baseUsers = 4500;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }
    if(h === 22){
        baseUsers = 4750;
        combined = baseUsers - additionalUsers + variance();
        usersNow = combined;
    }

};

var usersOnlineDelay = function(){
    var delay = Math.random() * 7000 + 2000;
    return delay;
};

var usersOnline = function(){
    getCurrentTime();
    var getUserNumbers = window.requestAnimationFrame(function () {
        if (flotUpdate()) {
            getCurrentTime();
        }
    });   
}();

var handleLiveUpdatedChart = function () {
    "use strict";
    if (intervalRunning === true){
        window.clearTimeout(usersOnSiteTimer);
    }else{
        intervalRunning = true;
    }

    var update = function() {
        plot.setData([ getUsers() ]);
        // since the axes don't change, we don't need to call plot.setupGrid()
        // plot.setupGrid();
        plot.draw();
        usersOnSiteTimer = setTimeout(update, updateInterval);

    };

    var getUsers = function () {
        if (usersOnSiteStorage.length >= 60){
            data = usersOnSiteStorage;
        }
        if (data.length > 0) {
            data = data.slice(1);
            usersOnSiteStorage = data.slice();
        }

        // get users
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50;
            var y = parseInt(usersNowVariance());
            if (y < 0) {
                y = 0;
            }
            if (y > 8000) {
                y = 8000;
            }
            data.push(y);
        }

        // zip the generated y values with the x values
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]]);
        }
        return res;
    };

    if ($('#live-updated-chart').length !== 0) {
        // setup control widget
        var updateInterval = 500;
        $("#updateInterval").val(updateInterval).change(function () {
            var v = $(this).val();
            if (v && !isNaN(+v)) {
                updateInterval = +v;
                if (updateInterval < 1) {
                    updateInterval = 1;
                }
                if (updateInterval > 2000) {
                    updateInterval = 2000;
                }
                $(this).val("" + updateInterval);
            }
        });

        // setup plot
        var options = {
            series: { shadowSize: 0, color: 'rgba(66, 174, 214, 1)', lines: { show: true, fill:true } }, // drawing is faster without shadows
            yaxis: { min: 0, max: 8000, tickColor: '#23252a' },
            xaxis: { show: false, tickColor: '#23252a' },
            grid: {
                borderWidth: 0,
                borderColor: '#23252a'
            }
        };
        var plot = $.plot($("#live-updated-chart"), [ getUsers() ], options);
        update();
    }
};

var Chart = function () {
    "use strict";
    return {
        //main function
        init: function () {
                handleLiveUpdatedChart();
        }
    };
}();
