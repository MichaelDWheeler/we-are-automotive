var minutePie = function(){
var carsSoldMinute = echarts.init(document.getElementById('minute-pie'));
var minuteJson = {};

var labelTopMinute = {
    normal: {
        label: {
            show: true,
            position: 'center',
            formatter: '{b}',
            textStyle: {
                baseline: 'bottom'
            }
        },
        labelLine: {
            show: false
        }
    }
};
var labelMinuteFormatter = {
    normal: {
        label: {
            formatter: function (params) {
                return 100 - params.value + '%';
            },
            textStyle: {
                baseline: 'top',
                fontSize: '14'
            }
        }
    },
};
var labelBottomMinute = {
    normal: {
        color: 'rgba(182, 194, 201, 0.6)',
        label: {
            show: true,
            position: 'center'
        },
        labelLine: {
            show: false
        }
    },
    emphasis: {
        color: 'rgba(0,0,0,0)'
    }
};
var minuteRadius = [40, 50];

var setMinuteOptions = function(carsSoldMinuteData){

    var minuteData = carsSoldMinuteData[0];

    optionMInutePercentage = {
        color: ['#FB6698'],
        legend: {
            show: false,
            x: 'center',
            y: 'center',
            data: [
                'null'
            ],
            textStyle: {
                color: '#eee'
            }
        },
        title: {
            text: '',
            subtext: '',
            x: 'center',
            textStyle: {
                color: '#eee'
            }
        },
        toolbox: {
            show: true,
            feature: {
                dataView: { show: false, readOnly: false },
                magicType: {
                    show: false,
                    type: ['pie', 'funnel'],
                    option: {
                        funnel: {
                            width: '20%',
                            height: '30%',
                            itemStyle: {
                                normal: {
                                    label: {
                                        formatter: function (params) {
                                            return 'other\n' + params.value + '%\n';
                                        },
                                        textStyle: {
                                            baseline: 'middle'
                                        }
                                    }
                                },
                            }
                        }
                    }
                },
                restore: { show: false },
                includeDealership: { show: false }
            }
        },
        series: [
            {
                type: 'pie',
                center: ['50%', '50%'],
                radius: minuteRadius,
                x: '0%', // for funnel
                itemStyle: labelMinuteFormatter,
                data: [
                    { name: 'other', value: 100 - minuteData, itemStyle: labelBottomMinute },
                    { name: 'vs. Last Year', value: minuteData, itemStyle: labelTopMinute }
                ]
            }
        ]
    };
    carsSoldMinute.setOption(optionMInutePercentage);
};


var getPercentage = function(){

    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/cars-sold-percentage.php');
    xhr.send();
    xhr.onload = function(){
        var carsSoldMinuteData = JSON.parse(xhr.response);
        carsSoldMinuteData[0] = carsSoldMinuteData[0];
        minuteJson = carsSoldMinuteData.slice();
        setMinuteOptions(carsSoldMinuteData);
        // minutePercentages(carsSoldMinuteData);
    };
}();
};
