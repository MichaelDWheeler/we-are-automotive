var gridstackOptions = {
    cell_height: 70,
    vertical_margin: 0,
    resizable: {
        handles: 'e, se, s, sw, w'
    },
    animate: true,
    removable: true,
    removeTimeout: 0,
};
$('.grid-stack').gridstack(gridstackOptions);


$('.grid-stack').on('resizestop', function (e) {
    allowUserSave();
    var element = e.target;
    var id = $(element).find('.box-prop', element).attr('id');
    switch (id) {
        case 'chart-1':
            interactiveMap.resize();
            break;
        case 'chart-2':
            $('#live-updated-chart').empty();
            handleLiveUpdatedChart();
            break;
        case 'chart-3':
            drawSparkline();
            break;
        case 'chart-4':
            drawBarChart2();
            break;
        case 'chart-6':
            drawLineChartNoFill();
            break;
        case 'chart-7':
            drawLineChartNoFill();
            break;
        case 'chart-8':
            drawLineChartFill();
            break;
        case 'chart-9':
            $('#average-days-sold').empty();
            $.MorrisLineChart.init();
            break;
        case 'chart-10':
            // Does not need to do anything on resize as it is on a timeout and will redraw after 3 seconds
            break;
        case 'chart-11':
            drawLineChartFill();
            break;
        case 'chart-12':
            drawBarChartNoShow();
            break;
        case 'chart-13':
            proceedPie2 = false;
            var restartPie2Timer = window.setTimeout(function () {
                proceedPie2 = true;
                drawPieChart2();
                changePC2Colors();
            }, 2100);
            break;
        case 'chart-14':
            drawLineChartNoFill2();
            break;
        case 'chart-15':
            phoneBusinessHours.resize();
            break;
        case 'chart-16':
            drawTargetBarLine();
            break;
        case 'chart-17':
            new Chartist.Bar('#horizontal-bar-chart', dataHorizontal, propertiesHorizontal);
            break;
        case 'chart-18':
            getProspectsSoldData();
            break;
        case 'chart-19':
            new Chartist.Bar('#labor-hours', dataLH, propertiesLH);
            break;
        case 'chart-20':
            checkMarketingContainer();
            break;
    }
});
