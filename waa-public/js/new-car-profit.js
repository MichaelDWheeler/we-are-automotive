var pie1Properties = {
    type: 'pie',
    width: '250',
    height: '250',
    sliceColors: ['#3bafda', '#306079']
};

var proceedPie1 = true;
var checkPie1Status = function(){
    if (proceedPie1 === true){
        return true;
    }else{
        return false;
    }
};
var degrees = 0;

var displayNewProfit = function(){
    $('.new-front').text(pieChart1Data[0]);
    $('.new-back').text(pieChart1Data[1]);
    $('.new-total').text(pieChart1Data[0] + pieChart1Data[1]);
};

var coinToss = function(){
    var operator = Math.floor(Math.random() *2) + 1;
    if (operator === 1){
        return true;
    }else{
        return false;
    }
};

var fluctuateAmount = function(){
    var amountToFluctuate0 = Math.floor(Math.random() * (pieChart1Data[0] * 0.05))+1;
    var amountToFluctuate1 = Math.floor(Math.random() * (pieChart1Data[1] * 0.05))+1;
    if (coinToss() === true){
        pieChart1Data[0] = pieChart1Control[0] + amountToFluctuate0;
    }else{
        pieChart1Data[0] = pieChart1Control[0] - amountToFluctuate0;
    }

    if (coinToss() === true){
        pieChart1Data[1] = pieChart1Control[1] + amountToFluctuate1;
    }else{
        pieChart1Data[1] = pieChart1Control[1] - amountToFluctuate1;
    }
};

var changePC1Colors = function(){
        if(degrees >= 360){
            degrees = 0;
        }else{
            degrees += 10;
        }
        var pieChart1Timer1 = window.setTimeout(function(){
            displayNewProfit();
            pie1Properties.sliceColors = ['#42ccff', '#306079'];
            $('.new-front').css('color', '#42ccff');
            $('.new-back').css('color', '#fff');
            if(checkPie1Status() === true){
                drawPieChart1();
                var pieChart1Timer1 = window.setTimeout(function(){
                    pie1Properties.sliceColors = ['#3bafda', '#5cc7ff'];
                    $('.new-front').css('color', '#fff');
                    $('.new-back').css('color', '#5cc7ff');
                    if(checkPie1Status() === true){
                        drawPieChart1();
                        fluctuateAmount();
                        changePC1Colors();
                        $('#new-car-profit').css({'transform':'rotate('+ degrees + 'deg)', '-ms-transform' : 'rotate('+ degrees + 'deg)', '-webkit-transform':'rotate('+ degrees + 'deg)'});
                    }else{
                        return;
                    }
                }, 2000);
            }else{
                return;
            }
        }, 2000);
};
var pieChart1Data =[];

var getPieChartData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/new-car-profit.php', true);
    xhr.send();
    xhr.onload = function(){
        pieChart1Data = JSON.parse(xhr.response);
        pieChart1Control = pieChart1Data.slice();
        fluctuateAmount();
        $('#new-car-profit').sparkline(pieChart1Data, pie1Properties);
    };
};

var drawPieChart1 = function(){
    $('#new-car-profit').sparkline(pieChart1Data, pie1Properties);
};

getPieChartData();
changePC1Colors();
