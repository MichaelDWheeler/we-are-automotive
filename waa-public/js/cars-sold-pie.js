var carsSoldPie = function (element) {
    
    var csJson = {},
        el,
        pieColor;

    var labelTop = {
        normal: {
            label: {
                show: true,
                position: 'center',
                formatter: '{b}',
                textStyle: {
                    baseline: 'bottom'
                }
            },
            labelLine: {
                show: false
            }
        }
    };

    var labelFromatter = {
        normal: {
            label: {
                formatter: function (params) {
                    return 100 - params.value + '%';
                },
                textStyle: {
                    baseline: 'top',
                    fontSize: '14'
                }
            }
        },
    };

    var labelBottom = {
        normal: {
            color: 'rgba(182, 194, 201, 0.6)',
            label: {
                show: true,
                position: 'center'
            },
            labelLine: {
                show: false
            }
        },
        emphasis: {
            color: 'rgba(0,0,0,0)'
        }
    };

    var ytdRadius = [40, 50];

    var setCarSalesOptions = function (carsSoldYTDData, el, pieColor) {
        var data1 = carsSoldYTDData[0];

        optionCarPercentage = {
            color: [pieColor],
            legend: {
                show: false,
                x: 'center',
                y: 'center',
                data: [],
                textStyle: {
                    color: '#eee'
                }
            },
            title: {
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    color: '#eee'
                }
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: { show: false, readOnly: false },
                    magicType: {
                        show: false,
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                width: '20%',
                                height: '30%',
                                itemStyle: {
                                    normal: {
                                        label: {
                                            formatter: function (params) {
                                                return 'other\n' + params.value + '%\n';
                                            },
                                            textStyle: {
                                                baseline: 'middle'
                                            }
                                        }
                                    },
                                }
                            }
                        }
                    },
                    restore: { show: false },
                    includeDealership: { show: false }
                }
            },
            series: [
                {
                    type: 'pie',
                    center: ['50%', '50%'],
                    radius: ytdRadius,
                    x: '0%', // for funnel
                    itemStyle: labelFromatter,
                    data: [
                        { name: 'other', value: 100 - data1, itemStyle: labelBottom },
                        { name: 'vs. Last Year', value: data1, itemStyle: labelTop }
                    ]
                }
            ]
        };
        el.setOption(optionCarPercentage);
    };

    var getPercentage = function (url, el, pieColor) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.send();
        xhr.onload = function () {
            var carsSoldYTDData = JSON.parse(xhr.response);
            csJson = carsSoldYTDData.slice();
            setCarSalesOptions(carsSoldYTDData, el, pieColor);
        };
    };

    var carsSold, carsSoldElement;

    if (element === '#cars-sold-pie') {
        el = echarts.init(document.getElementById('cars-sold-pie'));
        url = './php/cars-sold-percentage.php';
        pieColor = '#1AAA95';
    } else if (element === '#month-pie') {
        el = echarts.init(document.getElementById('month-pie'));
        url = './php/cars-sold-month-percentage.php';
        pieColor = '#3DB4DA';
    } else if (element === '#minute-pie') {
        el = echarts.init(document.getElementById('minute-pie'));
        url = './php/cars-sold-percentage.php';
        pieColor = '#FB6698';
    } else if (element === '#dealersocket-pie') {
        el = echarts.init(document.getElementById('dealersocket-pie'));
        url = './php/ds-sold-percentage.php';
        pieColor = '#A4B0BC';
    }
    getPercentage(url, el, pieColor);
};
