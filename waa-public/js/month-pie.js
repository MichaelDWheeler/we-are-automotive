var monthPie = function () {
    var carsSoldMonth = echarts.init(document.getElementById('month-pie'));

    var labelTopMonth = {
        normal: {
            label: {
                show: true,
                position: 'center',
                formatter: '{b}',
                textStyle: {
                    baseline: 'bottom'
                }
            },
            labelLine: {
                show: false
            }
        }
    };
    var labelMonthFormatter = {
        normal: {
            label: {
                formatter: function (params) {
                    return 100 - params.value + '%';
                },
                textStyle: {
                    baseline: 'top',
                    fontSize: '14'
                }
            }
        },
    };
    var labelBottomMonth = {
        normal: {
            color: 'rgba(182, 194, 201, 0.6)',
            label: {
                show: true,
                position: 'center'
            },
            labelLine: {
                show: false
            }
        },
        emphasis: {
            color: 'rgba(0,0,0,0)'
        }
    };
    var monthRadius = [40, 50];

    var setMonthOptions = function (carsSoldMonthData) {
        var monthData = carsSoldMonthData[0];

        optionMonthPercentage = {
            color: ['#3DB4DA'],
            legend: {
                show: false,
                x: 'center',
                y: 'center',
                data: [
                    'null'
                ],
                textStyle: {
                    color: '#eee'
                }
            },
            title: {
                text: '',
                subtext: '',
                x: 'center',
                textStyle: {
                    color: '#eee'
                }
            },
            toolbox: {
                show: true,
                feature: {
                    dataView: { show: false, readOnly: false },
                    magicType: {
                        show: false,
                        type: ['pie', 'funnel'],
                        option: {
                            funnel: {
                                width: '20%',
                                height: '30%',
                                itemStyle: {
                                    normal: {
                                        label: {
                                            formatter: function (params) {
                                                return 'other\n' + params.value + '%\n';
                                            },
                                            textStyle: {
                                                baseline: 'middle'
                                            }
                                        }
                                    },
                                }
                            }
                        }
                    },
                    restore: { show: false },
                    includeDealership: { show: false }
                }
            },
            series: [
                {
                    type: 'pie',
                    center: ['50%', '50%'],
                    radius: monthRadius,
                    x: '0%', // for funnel
                    itemStyle: labelMonthFormatter,
                    data: [
                        { name: 'other', value: 100 - monthData, itemStyle: labelBottomMonth },
                        { name: 'vs. Last Month', value: monthData, itemStyle: labelTopMonth }
                    ]
                }
            ]
        };
        carsSoldMonth.setOption(optionMonthPercentage);
    };


    var getPercentage = function () {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/cars-sold-month-percentage.php');
        xhr.send();
        xhr.onload = function () {
            var carsSoldMonthData = JSON.parse(xhr.response);
            setMonthOptions(carsSoldMonthData);
        };
    }();

};
