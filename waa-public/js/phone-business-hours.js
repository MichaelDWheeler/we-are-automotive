
if($('#phone-business-hours').length){

var phoneBusinessHours = echarts.init(document.getElementById('phone-business-hours'));

var phoneArray = [];
var addPhoneData = function(phoneData){
    var phoneDataLength = phoneData.length;
    for(var i = 0; i < phoneDataLength; i ++){
        phoneArray.push("");
    }
};

pbhOptions = {
    color: ['#3bafda', '#80deea'],
    grid: {
        borderWidth: 0
    },
    tooltip: {
        trigger: 'axis'
    },
    calculable: true,
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            data: phoneArray,
            axisLabel: {           // Axis text labels
                show: true,
                // formatter: null,
                textStyle: {       // The remaining properties using the global default text style
                    color: 'white'
                }
            },
            splitLine: { show: false },
        }
    ],
    yAxis: [
        {
            type: 'value',
            axisLabel: {           // Axis text labels
                show: true,
                // formatter: null,
                textStyle: {       // The remaining properties using the global default text style
                    color: 'white'
                }
            },
            splitLine: { show: false },
        }
    ],
    series: [
        {
            name: 'Minutes',
            type: 'line',
            smooth: true,
            itemStyle: { normal: { areaStyle: { type: 'default' } } },
            data: [],
        }
    ]
};

var phoneBusinessDelay = function(){
    var delay = Math.random() * 5000;
    return delay;
};


var drawPhoneBusiness = function(phoneData){
    pbhOptions.series[0].data = phoneData;
    phoneBusinessHours.setOption(pbhOptions);
    var phoneBusinessHoursTimer = window.setTimeout(function(){
            changePhoneData(phoneData);
    }, phoneBusinessDelay());
};

var anomaly = function(){
    var chance = Math.random() * 100;
    return chance;
};

var getAverage = function(phoneData){
    var sum = 0;
    var phoneDataLength = phoneData.length;
    for (var i = 0; i < phoneDataLength; i++){
        sum += parseInt(phoneData[i]);
    }
    var average = parseFloat(sum/phoneDataLength).toFixed(2);
    $('.response-time').text(average);
};

var changePhoneData = function(phoneData){
    phoneData.shift();
    if(anomaly() > 97){
        phoneData.push(Math.floor(Math.random() * 15) + 1);
    }else{
        phoneData.push(Math.floor(Math.random() * 9) + 3);
    }
    getAverage(phoneData);
    drawPhoneBusiness(phoneData);
};


var getPhoneData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/phone-business-hours.php', true);
    xhr.send();
    xhr.onload = function(){
        var phoneData = JSON.parse(xhr.response);
        addPhoneData(phoneData);
        drawPhoneBusiness(phoneData);
        changePhoneData(phoneData);
    };
};


}
