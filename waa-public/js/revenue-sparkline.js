


var drawSparkline = function (el, firstArray, secondArray) {
    $(el).sparkline(firstArray, {
        type: 'line',
        width: $(el).width(),
        height: '225',
        chartRangeMax: 5000,
        chartRangeMin: 0,
        lineColor: 'rgb(255,255,255)',
        fillColor: 'rgba(255,255,255,0.3)',
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });

    $(el).sparkline(secondArray, {
        type: 'line',
        width: $(el).width(),
        height: '225',
        chartRangeMax: 5000,
        chartRangeMin: 0,
        lineColor: 'rgb(93, 156, 236)',
        fillColor: 'rgba(93, 156, 236, 0.3)',
        composite: true,
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });
    $(el).off();
};





var firstTotal = function (textReplaced) {
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var laborNumber;
    if (sometimes > 95) {
        laborNumber = Math.floor(Math.random() * 5000) + 99;
    } else {
        laborNumber = Math.floor(Math.random() * 1200) + 99;
    }
    $(textReplaced).text(laborNumber);
    return (laborNumber);
};

var displayDelay2 = function () {
    var delay = Math.random() * 1200;
    return delay;
};

var secondTotal = function (textReplaced) {
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var partsNumber;
    if (sometimes > 95) {
        partsNumber = Math.floor(Math.random() * 5000) + 99;
    } else {
        partsNumber = Math.floor(Math.random() * 1200) + 99;
    }
    $(textReplaced).text(partsNumber);
    return (partsNumber);
};

var drawChartIntervalFirst = function (el, textReplaced, firstArray, secondArray) {
    var chartIntervalLabor = window.setTimeout(function () {
        firstArray.shift();
        firstArray.push(firstTotal(textReplaced));
        drawSparkline(el, firstArray, secondArray);
        drawChartIntervalFirst(el, textReplaced, firstArray, secondArray);
    }, displayDelay2());
};

var drawChartIntervalSecond = function (el, textReplaced, firstArray, secondArray) {
    var chartIntervalParts = window.setTimeout(function () {
        secondArray.shift();
        secondArray.push(secondTotal(textReplaced));
        drawSparkline(el, firstArray, secondArray);
        drawChartIntervalSecond(el, textReplaced, firstArray, secondArray);

    }, displayDelay2());
};

var getRevenue = function (url, el, firstArray, secondArray, firstText, secondText) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.send();
    xhr.onload = function () {
        var json = JSON.parse(xhr.response);
        firstArray = json[0];
        secondArray = json[1];
        
        drawSparkline(el, firstArray, secondArray);
        drawChartIntervalFirst(el, firstText, firstArray, secondArray);
        drawChartIntervalSecond(el, secondText, firstArray, secondArray);
    };
};

var initSparkline = function () {
    var url = './php/labor-parts-revenue.php';
    var el = '#revenue-sparkline';
    var firstText = '.labor-sold';
    var secondText = '.parts-sold';
    var salesArray = [];
    var serviceArray = [];
    getRevenue(url, el, salesArray, serviceArray, firstText, secondText);

}();

var initSparkline2 = function () {
    var url = './php/sales-sevice-data.php';
    var el = '#revenue-sparkline-2';
    var firstText = '.sales-sold';
    var secondText = '.service-sold';
    var laborArray = [];
    var partsArray = [];
    getRevenue(url, el, laborArray, partsArray, firstText, secondText);
}();
