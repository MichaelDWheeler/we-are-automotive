
    var callsCompleted = function(response){
        var amount = response[1];
        var delayTime = function(){
            var delay = Math.floor(Math.random() * 3000);
            return delay;
        };

        var increaseCallsMade = window.setInterval(function(){
            amount = amount + response[2];
        }, 1000);


        var runPrintCalls = function(){
            var printCalls = window.setTimeout(function(){
                $('.completed').text(Math.floor(amount));
                $('.outbound-calls-per-day').val(Math.round(($('.completed').text()/response[3])*100));
                runPrintCalls();
            }, delayTime());
        };

        runPrintCalls();
    };

var getCallsPerDay = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/outbound-calls-per-day.php', true);
    xhr.send();
    xhr.onload = function(){
        var response = JSON.parse(xhr.response);
        $('.outbound-calls-per-day').val(Math.round((response[1]/response[3])*100));
        $('.outbound-calls-per-day').trigger('change');
        $('.completed').text(response[1]);
        callsCompleted(response);
    };
};
