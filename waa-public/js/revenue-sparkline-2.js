var sales30Days = [];
var service30Days = [];

var drawSparkline2 = function() {
    $('#revenue-sparkline-2').sparkline(sales30Days, {
        type: 'line',
        width: $('#revenue-sparkline-2').width(),
        height: '225',
        chartRangeMax: 5000,
        chartRangeMin: 0,
        lineColor: '#fff',
        fillColor: 'rgba(255,255,255,0.3)',
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });

    $('#revenue-sparkline-2').sparkline(service30Days, {
        type: 'line',
        width: $('#revenue-sparkline-2').width(),
        height: '225',
        chartRangeMax: 5000,
        chartRangeMin: 0,
        lineColor: '#5d9cec',
        fillColor: 'rgba(93, 156, 236, 0.3)',
        composite: true,
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });


};

var getSalesServiceData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/sales-sevice-data.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        sales30Days = json[0];
        service30Days = json[1];
        drawSparkline2();
    };
};

getSalesServiceData();

var salesSold = function(){
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var salesNumber;
    if (sometimes > 95){
        salesNumber = Math.floor(Math.random() * 5000) + 99;
    }else{
        salesNumber = Math.floor(Math.random() * 1200) + 99;
    }
    $('.sales-sold').text(salesNumber);
    return (salesNumber);
};

var serviceSold = function(){
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var serviceNumber;
    if(sometimes > 95){
        serviceNumber = Math.floor(Math.random() * 5000) + 99;
    }else{
        serviceNumber = Math.floor(Math.random() * 1200) + 99;
    }
    $('.service-sold').text(serviceNumber);
    return (serviceNumber);
};

var displayDelay = function(){
    var delay = Math.random() * 1200;
    return delay;
};

var getSales30Days = function(){
    sales30Days.shift();
    sales30Days.push(salesSold());
    drawSparkline2();
    return;
};

var drawChartIntervalSales = function(){
    var chartIntervalSales = window.setTimeout(function(){
        getSales30Days();
        drawChartIntervalSales();
    },displayDelay());
};

var getService30Days= function(){
    service30Days.shift();
    service30Days.push(serviceSold());
    drawSparkline2();
    return;
};

var drawChartIntervalService = function(){
        var chartIntervalService = window.setTimeout(function(){
            getService30Days();
            drawChartIntervalService();
        },displayDelay());
};
