var mobile, hideTitle;
var alreadyDisplayed = false;
var loadContent = document.getElementById('login-content');
var saveTemplate = false;
var MOBILE_VIEW = 480;
var flotUpdateNow = true;
var scrollToTopTimer;

var createScrollAnchors= function(){
    var windowHeight = $(window).height() -100;
    var gridStackElements = $('.grid-stack-item').length;
    $('.grid-stack-item').eq(0).addClass('scrollElement');
    var j = 0;
    for (var i = 1; i < gridStackElements; i++){
        var getScrollElements = $('.scrollElement');
        var thisElementPosition = $('.grid-stack-item').eq(i).position().top;
        var previousScrollElement = $('.scrollElement').eq(j).position().top;
        if(thisElementPosition >= previousScrollElement + (windowHeight/2)){
            $('.grid-stack-item').eq(i).addClass('scrollElement');
            j++;
        }
    }
};

var flotUpdate = function(){
    if (flotUpdateNow === true){
        return true;
    }else{
        return false;
    }
};

var findValue = function(jsonValue) {
    var newVal;
    var x = Math.floor(Math.random() * 4) + 1;
    if (x === 1) {
        newVal = parseInt(jsonValue) + parseInt((Math.floor(Math.random() * 5)));
    } else if (x === 2) {
        newVal = parseInt(jsonValue) - parseInt((Math.floor(Math.random() * 5)));
    } else {
        newVal = parseInt(jsonValue);
    }
    return (newVal);
};

var createVarience = function(dial, response) {
    var delay = Math.random() * 5000 + 2000;
    $(dial).val(response);
    var varienceInterval = window.setInterval(function() {
        $(dial).val(findValue(response));
        $('.knob').trigger('change');
    }, delay);
};

var drawCharts = function () {
    if ($('#interactive-map').length && $('#interactive-map').is(":visible")) {
        interactiveMap.resize();
    }
    if ($('#live-updated-chart').length >= 0 && flotUpdate() && $('#live-updated-chart').is(":visible")) {
        $('#live-updated-chart').empty();
        handleLiveUpdatedChart();
    }
    //if ($('#sold-new-past-30-days-average').length && $('#sold-new-past-30-days-average').is(":visible")) {
    //    soldUsedPast30DaysAverage();
    //}
    if ($('#revenue-sparkline').length && $('#revenue-sparkline').is(":visible")) {
        var el = '#revenue-sparkline';
        drawSparkline(el);
    }
    if ($('#line-chart-no-fill').length && $('#line-chart-no-fill').is(":visible")) {
        drawLineChartNoFill();
    }
    if ($('#line-chart-fill').length && $('#line-chart-fill').is(":visible")) {
        drawLineChartFill();
    }
    //if ($('#target-bar-line').length && $('#target-bar-line').is(":visible")){
    //    var url = './php/target-bar-line.php';
    //    var element = '#target-bar-line';
    //    var dateElement = 'date-used';
    //    getBarChartNoGuidesData(url, element, dateElement);
    //}
    if ($('#revenue-sparkline-2').length && $('#revenue-sparkline-2').is(":visible")) {
        var el = '#revenue-sparkline-2';
        drawSparkline(el);
    }
    if ($('#phone-business-hours').length && $('#phone-business-hours').is(":visible")) {
        phoneBusinessHours.resize();
    }
    if ($('#line-chart-no-fill-2').length && $('#line-chart-no-fill-2').is(":visible")) {
        drawLineChartNoFill2();
    }
    if ($('#line-chart-fill-2').length && $('#line-chart-fill-2').is(":visible")) {
        drawLineChartFill2();
    }
    if ($('#morris-donut').length && $('#morris-donut').not(':has(svg)').length) {
        window.clearTimeout(donutTimer);
        $('#morris-donut').empty();
        getAppointmentData();
    }
    if ($('#horizontal-bar-chart').length && $('#horizontal-bar-chart').is(":visible")) {
        new Chartist.Bar('#horizontal-bar-chart', dataHorizontal, propertiesHorizontal);
    }
    if ($('#prospects-sold').length && $('#prospects-sold').is(":visible")) {
        new Chartist.Bar('#prospects-sold', quartersData, propertiesFourBars, optionsFourBars);
    }
    if ($('#distributed-series').length && $('#distributed-series').is(":visible")) {
        new Chartist.Bar('#distributed-series', dataDSeries, optionsDOptions);
    }
    if ($('#label-placement-chart').length && $('#distributed-series').is(":visible")) {
        new Chartist.Bar('#label-placement-chart', dataLP, propertiesLP);
    }
};

var allowUserSave = function(){
    $('.brand-container').addClass('animated fadeOutLeft');
    $('.refresh-manager').css('display', 'block');
    $('.save-template').css('display', 'block');
    if($('.save-text').hasClass('fadeOutRight')){
        $('.save-text').removeClass('fadeOutRight');
    }
    if($('.fa-save').hasClass('fadeOutLeft')){
        $('.fa-save').removeClass('fadeOutLeft');
    }
    $('.save-text').addClass('fadeInRight');
    $('.fa-save').addClass('animated fadeInLeft');
};
var fixSizes = function(){
    if ($('#interactive-map').length && $('#interactive-map').is(":visible")) {
        interactiveMap.resize();
    }
    if ($('#live-updated-chart').length >= 0 && flotUpdate() && $('#live-updated-chart').is(":visible")) {
        $('#live-updated-chart').empty();
        handleLiveUpdatedChart();
    }
    if ($('#revenue-sparkline').length && $('#revenue-sparkline').is(":visible")) {
        drawSparkline();
    }
    if ($('#sold-new-past-30-days-average').length && $('#sold-new-past-30-days-average').is(":visible")){
        soldUsedPast30DaysAverage();
    }
    if ($('.target-revenue-line-no-fill').length && $('.target-revenue-line-no-fill').is(":visible")) {
        drawLineChartNoFill();
    }
    if ($('#line-chart-fill').length && $('#line-chart-fill').is(":visible")) {
        drawLineChartFill();
    }
    if ($('#target-bar-line').length && $('#target-bar-line').is(":visible")){
        drawTargetBarLine1();
    }
    if($('#average-days-sold').length && $('#average-days-sold').is(":visible")){
        $('#average-days-sold').empty();
        getAverageSoldData();
    }
    if ($('#morris-donut').length && $('#morris-donut').not(':has(svg)').length) {
        window.clearTimeout(donutTimer);
        $('#morris-donut').empty();
        getAppointmentData();
    }
    if($('#bar-chart-no-guides-2').length && $('#average-days-sold').is(":visible")){
        drawBarChartNoShow();
    }

    if ($('#used-car-profit').length && $('#used-car-profit').is(":visible")){
        proceedPie2 = false;
        var restartPie2Timer = window.setTimeout(function(){
            proceedPie2 = true;
            drawPieChart2();
            changePC2Colors();
        },2100);
    }

    if ($('#line-chart-no-fill-2').length && $('#line-chart-no-fill-2').is(":visible")) {
        drawLineChartNoFill2();
    }

    if ($('#phone-business-hours').length && $('#line-chart-no-fill-2').is(":visible")) {
        phoneBusinessHours.resize();
    }

    if ($('#target-bar-line-2').length && $('#target-bar-line-2').is(":visible")) {
        drawTargetBarLine();
    }

    if ($('#horizontal-bar-chart').length && $('#horizontal-bar-chart').is(":visible")) {
        new Chartist.Bar('#horizontal-bar-chart', dataHorizontal, propertiesHorizontal);
    }
    if ($('#prospects-sold').length && $('#prospects-sold').is(":visible")) {
        new Chartist.Bar('#prospects-sold', quartersData, propertiesFourBars, optionsFourBars);
    }

    if ($('#labor-hours').length && $('#labor-hours').is(":visible")) {
        new Chartist.Bar('#labor-hours', dataLH, propertiesLH);
    }
    if ($('#label-placement-chart').length && $('#label-placement-chart').is(":visible")) {
        new Chartist.Bar('#label-placement-chart', dataLP, propertiesLP);
    }

};
(function() {

    var getScripts = function() {
        if ($('#dealership-overview').length) {
            dealershipOverview();
        }
        if ($('#cars-sold-ytd-block').length) {
            var carsSoldYTD = '#cars-sold-ytd-block';
            carsSold(carsSoldYTD);
        }
        if ($('#cars-sold-pie').length) {
            var carsSoldYTDPie = '#cars-sold-pie';
            carsSoldPie(carsSoldYTDPie);
        }
        if ($('#cars-sold-month-block').length) {
            var carsSoldMonth = '#cars-sold-month-block';
            carsSold(carsSoldMonth);
        }
        if ($('#month-pie').length) {
            var carsSoldMonthPie = '#month-pie';
            carsSoldPie(carsSoldMonthPie);
        }
        if ($('#cars-sold-minute-block').length) {
            carsSoldMinute();
        }
        if ($('#minute-pie').length) {
            var carsSoldMinutePie = '#minute-pie';
            carsSoldPie(carsSoldMinutePie);
        }
        if ($('#dealersocket-sales-block').length) {
            var dealserSocketSales = "#dealersocket-sales-block";
            carsSold(dealserSocketSales);
        }
        if ($('#dealersocket-pie').length) {
            var dealerSocketSalesPie = '#dealersocket-pie';
            carsSoldPie(dealerSocketSalesPie);
        }
        if ($('#interactive-map').length) {
            var interactiveMapTimer = window.setTimeout(function() {
                interactiveMap.setOption(option);
            }, 0);
        }
        if ($('#phone-business-hours').length) {
            getPhoneData();
        }

        if ($('#revenue-sparkline, #sold-new-past-30-days-average, #new-car-profit, #line-chart-no-fill, #line-chart-fill, #revenue-sparkline-2, #bar-chart-no-guides-2, #used-car-profit, #line-chart-no-fill-2, #line-chart-fill-2, #target-bar-line-2').length) {

            if ($('#sold-new-past-30-days-average').length) {
                soldNewPast30DaysAverage();
            }
            if ($('#new-car-profit').length) {
                getPieChartData();
                changePC1Colors();
            }
            if ($('#line-chart-no-fill').length) {
                getNewVsUsedData();
            }
            if ($('#line-chart-fill').length) {
                getLineChartData();
            }
            if ($('#target-bar-line').length) {      
                soldUsedPast30DaysAverage();
            }
            if ($('#bar-chart-no-guides-2').length) {
                getAppointmentNoShowData();
            }
            if ($('#used-car-profit').length) {
                getPieChartData2();
                changePC2Colors();
            }
            if ($('#line-chart-no-fill-2').length) {
                drawBase();
                drawInternetLeadTotal();
                drawPhoneLeadTotal();
                drawFreshUpTotal();
            }
            if ($('#target-bar-line-2').length) {
                getOutboundCallsSkippedData();
            }

        }
        if ($('#average-days-sold, #morris-donut').length) {
            if ($('#average-days-sold').length && $('#average-days-sold').not(':has(svg)').length) {
                getAverageSoldData();
            }
            if ($('#morris-donut').length && $('#morris-donut').not(':has(svg)').length) {
                getAppointmentData();
            }

        }
        if ($('#horizontal-bar-chart').length) {
            frontBackProfit();
        }
        if ($('#prospects-sold').length) {
            getProspectsSoldData();
        }
        if ($('#labor-hours').length) {
            laborHours();
        }
        if ($('#label-placement-chart').length) {
            marketingChannels();
        }
        if ($('#live-updated-chart').length >= 0) {
                Chart.init();
        }
        if ($('.knob').length) {

                if ($('.outbound-calls-per-day')) {
                    getCallsPerDay();
                }
                if ($('.closed-ro-appt').length) {
                    getApptRoPercent();
                }
                if ($('.closed-ro-future').length) {
                    getFutureApptSet();
                }
                if ($('.sold-with-trade').length) {
                    soldWithTrade();
                }
                if ($('.sold-return-customer').length) {
                    soldReturnCustomer();
                }
                if ($('.sold-fresh-up').length) {
                    soldFreshUp();
                }
                if ($('.sold-phone-up').length) {
                    soldPhoneUp();
                }
                if ($('.sold-internet').length) {
                    soldInternet();
                }
                $('.knob').knob({
                    "readOnly": true
                });

        }

    };

    //displays titles when hovering over icons
    var showIconTitles = function() {
        $('.icon-container').mouseover(function() {
            $(':nth-child(3)', this).css('display', 'block');
        });
        $('.icon-container').mouseout(function() {
            $(':nth-child(3)', this).css('display', 'none');
        });
    }();

    //sets height of login window
    var setHeight = function() {
        var docHeight = $(window).height();
        $('.right-content').css('height', docHeight + 'px');
    }();

    //adds style borders to log in form if user is scrolled to bottom
    $(window).scroll(function() {
        var docHeight = $(window).height();
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('.right-content').css('height', docHeight - 120 + 'px');
        } else if ($(window).scrollTop() + $(window).height() >= $(document).height() - 20) {
            var offset = $(window).scrollTop() + $(window).height() - $(document).height();
            var rightContentHeight = $(window).height() - 120 - offset;
            $('.right-content').css('height', rightContentHeight + 'px');
        } else {
            $('.right-content').css('height', docHeight + 'px');
        }
    });

    var openForView = function() {
        $('.customize-text, .login-text').css('display', 'none');
        if ($(window).width() <= MOBILE_VIEW) {
            $('.charts-displayed').attr('class', 'charts-displayed animated moveRightMobile');
        } else {
            $('.charts-displayed').attr('class', 'charts-displayed animated moveRight');
        }
        $('.right-content').css('display', 'block');
        if ($(window).width() <= MOBILE_VIEW) {
            $('.right-content').attr('class', 'right-content animated-out moveLeftMobile');
        } else {
            $('.right-content').attr('class', 'right-content animated-out moveLeft');
        }
    };

    var backdropWidth = function(){
        if($(".box-prop").length){
            return $(".box-prop").width() + 75 + "px";
        }else{
            return;
        }
    };

    var setBackdrop = function() {
        var width = $(window).width();
        $('.backdrop').css({
            'width': backdropWidth(),
            "display": "block"
        });
    };

    var setFormWidth = function() {
        if ($(document).width() <= MOBILE_VIEW) {
            $('.right-content').css('width', $(document).width() - 40 + "px");
            setBackdrop();
        } else {
            setBackdrop();
            return;
        }
    };

    $('.save-template').click(function() {
        saveTemplate = true;
        $('#login-waa').click();
        $('.save-text').removeClass('fadeInRight');
        $('.fa-save').removeClass('animated fadeInLeft');
        $('.save-text').addClass('fadeOutRight');
        $('.fa-save').addClass('animated fadeOutLeft');
        var disappearTimer = window.setTimeout(function() {
            $('.save-template').css('display', 'none');
        }, 1500);

    });

    //opens login form
    $('#login-waa, .login-text').click(function() {
        setFormWidth();
        openForView();
        var loadContent = document.getElementById('login-content');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/login.php', true);
        xhr.send();
        xhr.onload = function() {
            var htmlContent = xhr.response;
            $(loadContent).empty();
            $(loadContent).append(htmlContent);
            if (saveTemplate){
                $('#board-name').css('display', 'block');
            }
        };
    });

    //opens register form
    $('#customize-waa, .customize-text').click(function() {
        setFormWidth();
        openForView();
        var loadContent = document.getElementById('login-content');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './ajax/register.html', true);
        xhr.send();
        xhr.onload = function() {
            $(loadContent).empty();
            var htmlContent = xhr.response;
            $(loadContent).append(htmlContent);
        };
    });

    //sets spacing between header and main Content
    var waaHeaderSpacing = function() {
        var waaHeaderHeight = $('.waa-heading').height();
        $('.main-container').css('top', waaHeaderHeight + 'px');
    }();

    var showClosingElement = function() {
        $('.box-prop').mouseover(function() {
            $('.close-x', this).first().css('display', 'block');
        });
        $('.box-prop').mouseout(function() {
            $('.close-x', this).first().css('display', 'none');
        });
    }();

    var hideChart = function() {
        $('.close-x').click(function(e) {
            e.target = this;
            target = e.target;
            if ($(target).parent().attr('class', 'donut-accented')) {
                window.clearTimeout(donutTimer);
            }
            if ($(target).parent().attr('class', 'real-time-chart')) {
                flotUpdateNow = false;
            }
            $(this).parent().parent().parent().css('display', 'none');
            allowUserSave();
        });
    }();

    $('.close-dar-x').click(function() {
        $('.download-dar').css('display', 'none');
    });

    var checkWidth = function() {

        var width = $(window).width();
        if (width > 480) {
            $('.charts-displayed').css('width', width - 80 + 'px');
        };
        if (width <= 480) {
            $('.left-navigation').css('display', 'none');
            $('.grid-stack-12').removeClass("m-l-140 m-r-20");
            $('.automotive-metrics-grid').attr('data-gs-height', '34');
            $('.dealership-overview-grid').attr('data-gs-height', '17');
            return;
        }else if(width <= 840 && width >= 768) {
            $('.automotive-metrics-grid').attr('data-gs-height', '16');
            $('.dealership-overview-grid').attr('data-gs-height', '9');
            return;
        }else if(width <= 991) {
            $('.automotive-metrics-grid').attr('data-gs-height', '33');
            $('.dealership-overview-grid').attr('data-gs-height', '16');

            return;
        }else {
            $('.left-navigation').css('display', 'block');
            $('.dealership-overview-grid').attr('data-gs-height', '5');
            $('.automotive-metrics-grid').attr('data-gs-height', '10');
        }
    };

    $('.mobile-nav').click(function() {
        $('.left-navigation').css('display', 'block');
    });

    var closeLeftNavigation = function() {
        $(document).click(function(e) {
            if (!$(e.target).is('.mobile-nav') && $('.left-navigation').css('display') === 'block' && $('.mobile-nav').css('display') === 'block') {
                $('.left-navigation').css('display', 'none');
            }
        });
    }();

    var boxPositionControl = [],
    boxPositions = [];

    var setBoxPositionsControl = function(){
        var numberOfBoxes = $('.grid-stack-item').length;
        for (var i = 0; i < numberOfBoxes; i++){
            var boxData = $('.grid-stack-item').eq(i).data();
            boxPositionControl.push(boxData);
        }
        boxPosition = boxPositionControl.slice();
        checkForPositionChanges();
    };

    var setBoxPositions = function(){
        boxPositions = [];
        var numberOfBoxes = $('.grid-stack-item').length;
        for (var i = 0; i < numberOfBoxes; i++){
            var boxData = $('.grid-stack-item').eq(i).data();
            boxPositions.push(boxData);
        }
    };

    var haveArraysChanged = function(){
        setBoxPositions();
        var arrayLength = boxPositionControl.length;
        if(boxPositionControl.length !== boxPositions.length){
            return true;
        }
        for(var i = 0; i < arrayLength; i++){
            if(boxPositionControl[i] != boxPositions[i]){
                return true;
            }
        }
    };

    var checkForPositionChanges = function(){
        window.setInterval(function(){
            if (haveArraysChanged()) {
                drawCharts();
                allowUserSave();
            }
        }, 300);
    };


    //checks for resize
    $(function () {
        var timer_id;
        $(window).resize(function() {
            clearTimeout(timer_id);
            timer_id = setTimeout(function() {
                checkWidth();
                drawCharts();
            }, 300);
        });
    });

    var revertFullScreen = function() {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitCancelFullScreen) {
            document.webkitCancelFullScreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        }
    };

    $('.close-fullscreen').click(function() {
        $(this).css('display', 'none');
        $('.expand-fullscreen').css('display', 'block');
        revertFullScreen();
    });

    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            if ($('.close-fullscreen').length) {
                $('.close-fullscreen').css('display', 'none');
                $('.expand-fullscreen').css('display', 'block');
            }
        }
    });

    $('.expand-fullscreen').click(function() {
        $(this).css('display', 'none');
        $('.close-fullscreen').css('display', 'block');
        var requestFullScreen = function(element) {
            var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullScreen || element.msRequestFullscreen;

            if (requestMethod) {
                requestMethod.call(element);
            } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
                var wscript = new ActiveXObject("WScript.Shell");
                if (wscript !== null) {
                    wscript.SendKeys("{F11}");
                }
            }
        };

        var docElm = document.documentElement;
        if (docElm.requestFullscreen) {
            docElm.requestFullscreen();
        } else if (docElm.mozRequestFullScreen) {
            docElm.mozRequestFullScreen();
        } else if (docElm.webkitRequestFullScreen) {
            docElm.webkitRequestFullScreen();
        } else if (docElm.msRequestFullscreen){
            alert('Internet Explorer users must press "F11" to enter/exit fullscreen mode');
        }

    });

    var stopScroll = false;
    var scrollDown = true;
    var continueScroll = function() {
        if (stopScroll === false) {
            return true;
        } else {
            return false;
        }
    };
    var elementPosition = 1;
    var scrollToNext = function() {
        checkForScroll(elementPosition);
        checkScrollElement(elementPosition);
        elementPosition = scrollToElement(elementPosition);
        scrollDirection(elementPosition, scrollToNext);
        $('.fa-toggle-on').click(function() {
            turnOffScroll();
        });
    };

    var checkForScroll = function(elementPosition){
        if (elementPosition >= 1) {
            stopScrolling = true;
        } else {
            stopScrolling = false;
        }
    };

    var checkScrollElement = function(elementPosition){
        $('html, body').animate({
            scrollTop: $(".scrollElement").eq(elementPosition).offset().top - 80
        }, 5000);
    };

    var scrollToElement = function(elementPosition){
        if (scrollDown === true && elementPosition < $(".scrollElement").length) {
            elementPosition++;
            return elementPosition;
        } else {
            elementPosition--;
            return elementPosition;
        }
    };

    var scrollElement = function(){
        return ($('.scrollElement').length);
    };
    var scrollTimer;
    var scrollDirection = function(elementPosition){
        scrollTimer = window.setTimeout(function() {
            if (elementPosition === scrollElement() - 1) {
                scrollDown = false;
                elementPosition = elementPosition - 1;
            }
            if (elementPosition === 0) {
                scrollDown = true;
            }
            stopScrolling = false;
            if (continueScroll()) {
                scrollToNext();
            }
        }, 30000);
    };

    var turnOffScroll = function(){
        stopScroll = true;
        $('.fa-toggle-on').css('display', 'none');
        $('.fa-toggle-off').css('display', 'block');
        $('.autoscroll-status').text("Off");
        window.clearTimeout(scrollTimer);
        window.clearTimeout(scrollToTopTimer);
    };



    var scrollWindow = function() {
        scrollToTopTimer = window.setTimeout(function() {
            if (continueScroll()) {
                scrollToNext();
            }
        }, 5000);
    };

    $('.fa-toggle-off').click(function() {
        stopScroll = false;
        $('.fa-toggle-off').css('display', 'none');
        $('.fa-toggle-on').css('display', 'block');
        $('.autoscroll-status').text("On");
        if(dontSlowScroll == null){
            scrollWindow();
        }else{
            var scrollWhenAnimationWontSlow = window.setInterval(function(){
                if (dontSlowScroll === 19){
                    scrollWindow();
                    window.clearInterval(scrollWhenAnimationWontSlow);
                }
            },200);
        }
    });

    var getShareUrls = function(){
        var url = window.location.href;
        $('a[href="https://www.facebook.com/sharer/sharer.php?u=http://www.we-are-automotive.com/"]').attr('href', 'https://www.facebook.com/sharer/sharer.php?u=' + url);
        $('a[href="https://www.linkedin.com/shareArticle?mini=true&url=www.we-are-automotive.com&title=We%20Are%20Automotive&summary=&source="]').attr('href', 'https://www.linkedin.com/shareArticle?mini=true&url='+url+'&title=We%20Are%20Automotive&summary=&source=');
        $('a[href="https://plus.google.com/share?url=http://www.we-are-automotive.com"]').attr('href', 'https://plus.google.com/share?url=' + url);

    };

    $('#logout-waa').click(function(){
        window.location.href = "./php/logout.php";
    });

    $('.refresh-manager, .refresh-text').click(function(){
        var currentLocation = window.location;
        window.location.href = currentLocation;
    });

    var loadUserTemplate = function(json){
        var firstArray = json[0];
        var parsed = (JSON.parse(firstArray));
        var gridStack = $('.grid-stack-item');
        var plength = parsed.length;

        gridStack.each(function(index){
            var count = 0;
            var gridstackId = $(this).attr('data-custom-id');
            for(var i=0; i < plength; i++){
                if($(this).attr('data-custom-id') === parsed[i].id){
                    $(this).attr('data-gs-x', parsed[i].x);
                    $(this).attr('data-gs-y', parsed[i].y);
                    $(this).attr('data-gs-width', parsed[i].width);
                    $(this).attr('data-gs-height', parsed[i].height);
                    gridstackOptions.staticGrid = true;

                }
                var equals = Number(parsed[i].id) == Number(gridstackId);
                if(equals === true){
                    count++;
                }
            }
            if(count === 0){
                $(this).css('display', 'none');
            }
        });
        var fixSizesTimer = window.setTimeout(function(){
            fixSizes();
        },3000);
    };

    var changeGridStackHeight = function(gridStackHeight, docHeight, json){
        $('body').css('height', docHeight);
        $('.grid-stack').attr('data-gs-current-height', gridStackHeight);
        $('.grid-stack').css('height', docHeight - 105 + 'px');
        loadUserTemplate(json);
        $('.backdrop').css('height', docHeight - 105 + 'px');

        $('.blocker').css({
            'display' : 'block',
            'height' : docHeight - 105 + 'px'
        });
    };

    var loadBoard = function(board){
        var data = {};
        data.board = board;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', './php/indexsharebuild.php', true);
        xhr.send(JSON.stringify(data));
        xhr.onload = function(){
            if(this.readyState === 4 && this.status === 200){
                var json = JSON.parse(xhr.response);
                var docHeight = json[1];
                var gridStackHeight = json[2];
                $('body').css('height', docHeight);
                changeGridStackHeight(gridStackHeight, docHeight, json);
            }
        };
    };

    var checkForLoadedBoard = function(){
        var board = location.search;
        if(board.indexOf('?userboardid=')!== -1){
            board = board.toString().replace("?userboardid=", "");
            loadBoard(board);
        }else{
            return;
        }

    };

    $(document).ready(function() {
        checkWidth();
        checkForLoadedBoard();
        getScripts();
        createScrollAnchors();
        getShareUrls();
        setBoxPositionsControl();
        $('.dealership-overview').css('visibility', 'visible');
    });
}());
