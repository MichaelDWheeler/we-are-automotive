var dealershipOverview = function(){
    'use strict';

    var findValue = function(jsonValue){
        var newVal;
        var x = Math.floor(Math.random()*6)+1;
        if(x === 1){
            newVal = parseInt(jsonValue) + parseInt((Math.floor(Math.random()*3)));
        }else if (x === 2){
            newVal = parseInt(jsonValue) - parseInt((Math.floor(Math.random()*3)));
        }else{
            newVal = parseInt(jsonValue);
        }
        return(newVal);
    };

    var delay = function(){
        var setDelay = Math.random()*5000 + 5000;
        return setDelay;
    };

    var changePercentages = function(json){
        var varienceTimer = window.setTimeout(function(){
                $('.appt-sold').val(findValue(json.appointmentSold));
                $('.vehicle-serviced').val(findValue(json.vehiclesServiced));
                $('.return-customer').val(findValue(json.returnCustomers));
                $('.visits-with-turn').val(findValue(json.visitsWithTurn));
                $('.knob').trigger('change');
                changePercentages(json);
        },delay());
    };

    var displayValues = function(json){
        $('.appt-sold').empty();
        $('.appt-sold').val(json.appointmentSold);
        json.appointmentSold = 39;
        $('.vehicle-serviced').empty();
        $('.vehicle-serviced').val(json.vehiclesServiced);
        json.vehiclesServiced = 27;
        $('.return-customer').empty();
        $('.return-customer').val(json.returnCustomers);
        json.returnCustomers = 21;
        $('.visits-with-turn').empty();
        $('.visits-with-turn').val(json.visitsWithTurn);
        json.visitsWithTurn = 60;
        changePercentages(json);
    };

    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/dealership-overview.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        displayValues(json);
    };
};
