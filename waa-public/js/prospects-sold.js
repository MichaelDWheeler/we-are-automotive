var quarter1 =[],
    quarter2 =[],
    quarter3= [],
    quarter4 =[];

var quartersData = {
        labels: ['Quarter 1', 'Quarter 2', 'Quarter 3', 'Quarter 4'],
        series: []
    };

    var propertiesFourBars ={
        stackBars: true,
        height: '420px',
        axisX: {
            position: 'start'
        },
        axisY: {
            offset: 20
        }
    };

    var optionsFourBars = [
        // Options override for media > 400px
        ['screen and (min-width: 400px)', {
            reverseData: true,
            horizontalBars: true,
            axisX: {
                labelInterpolationFnc: Chartist.noop
            },
            axisY: {
                offset: 60
            }
        }],
        // Options override for media > 800px
        ['screen and (min-width: 800px)', {
            stackBars: false,
            seriesBarDistance: 10
        }],
        // Options override for media > 1000px
        ['screen and (min-width: 1000px)', {
            reverseData: false,
            horizontalBars: false,
            seriesBarDistance: 15
        }]
    ];

var setQuarters = function(){
    var d = new Date();
    var setMonth = d.getMonth();
    var setYear = d.getFullYear();
    var lastYear = setYear - 1;

    if(setMonth === 0 || setMonth === 1 || setMonth === 2){
        quartersData.labels[0] = ['Quarter 2 ' + lastYear];
        quartersData.labels[1] = ['Quarter 3 ' + lastYear];
        quartersData.labels[2] = ['Quarter 4 ' + lastYear];
        quartersData.labels[3] = ['Quarter 1 ' + setYear];
    }else if(setMonth === 3 || setMonth === 4 || setMonth === 5){
        quartersData.labels[0] = ['Quarter 3 ' + lastYear];
        quartersData.labels[1] = ['Quarter 4 ' + lastYear];
        quartersData.labels[2] = ['Quarter 1 ' + setYear];
        quartersData.labels[3] = ['Quarter 2 ' + setYear];
    }else if(setMonth === 6 || setMonth === 7 || setMonth === 8){
        quartersData.labels[0] = ['Quarter 4 ' + lastYear];
        quartersData.labels[1] = ['Quarter 1 ' + setYear];
        quartersData.labels[2] = ['Quarter 2 ' + setYear];
        quartersData.labels[3] = ['Quarter 3 ' + setYear];
    }else{
        quartersData.labels[0] = ['Quarter 1 ' + setYear];
        quartersData.labels[1] = ['Quarter 2 ' + setYear];
        quartersData.labels[2] = ['Quarter 3 ' + setYear];
        quartersData.labels[3] = ['Quarter 4 ' + setYear];
    }
    new Chartist.Bar('#prospects-sold', quartersData, propertiesFourBars, optionsFourBars);

};

var getProspectsSoldData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/prospects-sold.php', true);
    xhr.send();
    xhr.onload = function () {
        var json = JSON.parse(xhr.response);
        quartersData.series[0] = json[0];
        quartersData.series[1] = json[1];
        quartersData.series[2] = json[2];
        quartersData.series[3] = json[3];
        setQuarters();
    };
};
