var carsSoldMinute = function(){
    'use strict';
    var numberValue, soldPerMinute, soldPerMinuteControl, oldNumber;
    var delay = 100;
    var number = 0;
    var slowCountInterval;

    var printNumber = function(){
        $('.minute-sales-number').text(number);
    };

    var printNewNumber = function(){
        $('.minute-sales-number').text(oldNumber);
    };


    var finishCountUp = function(){
        var finishCountUpTimer = window.setTimeout(function(){
            printNumber();
            if(number != soldPerMinuteControl){
                delay = delay + delay * 0.175;
                number++;
                finishCountUp();
            }else{
                getNumber();
            }
        },delay);
    };

    var countUp = function(){
        var countUpTimer = window.setTimeout(function(){
            printNewNumber();
            if(oldNumber != number){
                delay = delay + delay * 0.175;
                oldNumber++;
                countUp();
            }else{
                getNumber();
            }
        },delay);
    };

    var countDown = function(){
        var countUpTimer = window.setTimeout(function(){
            printNewNumber();
            if(oldNumber != number){
                delay = delay + delay * 0.175;
                oldNumber--;
                countDown();
            }else{
                getNumber();
            }
        },delay);
    };

    var checkNumber = function(){
        if(number < soldPerMinuteControl - 5){
            number = soldPerMinuteControl - 5;
        }
        if(number > soldPerMinuteControl + 5){
            number = soldPerMinuteControl + 5;
        }
    };

    var changeSoldPerMinute = function(){
        var randomDelay = Math.random() * 5000 + 1000;
        var changeValueTimer = window.setTimeout(function(){
            checkNumber();
            if(number > oldNumber){
                countUp();
            }else if(number < oldNumber){
                countDown();
            }else{
                getNumber();
            }

        },randomDelay);
    };

    var determineValue = function(){
        oldNumber = number;
        switch(numberValue){
            case '0': //0
            number += 0;
            changeSoldPerMinute();
            break;
            case '1': //4
            number += 4;
            changeSoldPerMinute();
            break;
            case '2': //3
            number += 3;
            changeSoldPerMinute();
            break;
            case '3': //2
            number += 2;
            changeSoldPerMinute();
            break;
            case '4': //1
            number += 1;
            changeSoldPerMinute();
            break;
            case '5': //0
            number += 0;
            changeSoldPerMinute();
            break;
            case '6': //-1
            number -= 1;
            changeSoldPerMinute();
            break;
            case '7': //-2
            number -= 2;
            changeSoldPerMinute();
            break;
            case '8': //-3
            number -= 3;
            changeSoldPerMinute();
            break;
            case '9': //-4
            number -= 4;
            changeSoldPerMinute();
            break;
        }
    };


    var getNumber = function(){
        delay = 100;
        var numberArray = [];
        var d = new Date();
        var n = d.getSeconds().toString();
        for(var i = 0; i < n.length; i++){
            numberArray.push(n[i]);
        }
        if(numberArray.length === 2){
            numberArray.shift();
        }
        numberValue = numberArray[0];
        determineValue();
    };

    var slowCountUp = function(){
        var countTimer = window.setTimeout(function(){
            slowCountInterval = window.setInterval(function(){
                    if(number < soldPerMinuteControl - 10){
                        number++;
                        printNumber();
                    }else{
                        clearInterval(slowCountInterval);
                        finishCountUp();
                    }
            }, 100);
        }, 1500);
    };

    var getCarsPerMinute = function(){
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/cars-sold-minute.php', true);
        xhr.send();
        xhr.onload = function(){
            soldPerMinute = Math.round(JSON.parse(xhr.response));
            soldPerMinuteControl = soldPerMinute;
            slowCountUp();
        };
    }();
};
