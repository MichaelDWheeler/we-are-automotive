var base = [], internetLead = [], phoneLead = [], freshUp = [];

var drawLineChartNoFill2 = function(){
    $('#line-chart-no-fill-2').empty();
    $('#line-chart-no-fill-2').sparkline(internetLead, {
        type: 'line',
        width: $('#line-chart-no-fill-2').width(),
        height: '245',
        margin: '0 auto 0 auto',
        chartRangeMax: 4,
        chartRangeMin: 0,
        lineColor: '#3bafda',
        fillColor: 'rgba(59,175,218,.2)',
        minSpotColor: '#1c1f26',
        maxSpotColor: '#1c1f26',
        spotColor: '#1c1f26',
        highlightLineColor: '#1c1f26',
        highlightSpotColor: '#1c1f26'
    });

    $('#line-chart-no-fill-2').sparkline(phoneLead, {
        type: 'line',
        width: $('#line-chart-no-fill-2').width(),
        height: '245',
        composite: true,
        chartRangeMax: 4,
        chartRangeMin: 0,
        lineColor: '#f76397',
        minSpotColor: '#1c1f26',
        maxSpotColor: '#1c1f26',
        spotColor: '#1c1f26',
        fillColor: 'rgba(247,99,151,.2)',
        highlightLineColor: '#1c1f26',
        highlightSpotColor: '#1c1f26'
    });

    $('#line-chart-no-fill-2').sparkline(freshUp, {
        type: 'line',
        width: $('#line-chart-no-fill-2').width(),
        height: '245',
        composite: true,
        chartRangeMax: 4,
        chartRangeMin: 0,
        lineColor: '#fff',
        fillColor: 'rgba(255,255,255,.2)',
        minSpotColor: '#1c1f26',
        maxSpotColor: '#1c1f26',
        spotColor: '#1c1f26',
        highlightLineColor: '#1c1f26',
        highlightSpotColor: '#1c1f26'
    });
    $('#line-chart-no-fill-2').sparkline(base, {
        type: 'line',
        width: $('#line-chart-no-fill-2').width(),
        height: '245',
        margin: '0 auto 0 auto',
        composite: true,
        chartRangeMax: 4,
        chartRangeMin: 0,
        lineColor: '#1c1f26',
        fillColor: 'transparent',
        minSpotColor: '#1c1f26',
        maxSpotColor: '#1c1f26',
        spotColor: '#1c1f26',
        highlightLineColor: '#1c1f26',
        highlightSpotColor: '#1c1f26'
    });
    $('#line-chart-no-fill-2').off();
};

var getVisitsSoldData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/visits-sold.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        internetLead = json[0];
        phoneLead = json[1];
        freshUp = json[2];
        base = json[3];
        drawLineChartNoFill2();
    };
}();

var visitsSoldDelay = function(){
    var delay = Math.random() * 3000;
    return delay;
};

var randomPercentage = function(){
    var percent = Math.floor(Math.random() * 100 + 1)/ 100;
    return percent;
};
var baseTotal = function(){
    var baseNumber = 0;
    return baseNumber;
};

var internetLeadTotal = function(){
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var internetNumber;
    if (sometimes > 98){
         internetNumber = Math.floor(Math.random() * 3 * randomPercentage()) + 1;
    }else if(sometimes > 78){
         internetNumber = Math.floor((Math.random() * 3) * 0.48) + 1;
    }else{
        internetNumber = 0;
    }
    $('.internetLeadVS').text(internetNumber);
    return (internetNumber);
};

var phoneLeadTotal = function(){
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var phoneLeadNumber;
    if (sometimes > 98){
         phoneLeadNumber = Math.floor(Math.random() * 3 * randomPercentage()) + 1;
    }else if(sometimes > 85){
         phoneLeadNumber = Math.floor((Math.random() * 3) * 0.48) + 1;
     }else{
         phoneLeadNumber = 0;
    }
    $('.phoneLeadVS').text(phoneLeadNumber);
    return (phoneLeadNumber);
};

var freshUpTotal = function(){
    var sometimes = Math.floor(Math.random() * 100 + 1);
    var freshUpNumber;
    if (sometimes > 98){
         freshUpNumber = Math.floor(Math.random() * 3 * randomPercentage()) + 1;
    }else if (sometimes > 90){
         freshUpNumber = Math.floor(Math.random() * 3 *0.48) + 1;
    }else{
         freshUpNumber = 0;
    }
    $('.freshUpLeadVS').text(freshUpNumber);

    return (freshUpNumber);
};

var drawInternetLeadTotal = function(){
        var internetLeadTotalTimer = window.setTimeout(function(){
            internetLead.shift();
            internetLead.push(internetLeadTotal());
            drawLineChartNoFill2();
            drawInternetLeadTotal();
        },visitsSoldDelay());
};
var drawBase = function(){

            base.shift();
            base.push(baseTotal());
            drawLineChartNoFill2();

};
var drawPhoneLeadTotal = function(){
        var phoneLeadTotalTimer = window.setTimeout(function(){
            phoneLead.shift();
            phoneLead.push(phoneLeadTotal());
            drawLineChartNoFill2();
            drawPhoneLeadTotal();
        },visitsSoldDelay());
};

var drawFreshUpTotal = function(){
        var freshUpTotalTimer = window.setTimeout(function(){
            freshUp.shift();
            freshUp.push(freshUpTotal());
            drawLineChartNoFill2();
            drawFreshUpTotal();
        },visitsSoldDelay());
};
