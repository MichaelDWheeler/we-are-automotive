var totalVisits = [];
var walkIn = [], phoneIn = [], internet = [];

var getLineChartData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET','./php/store-visits-7-days.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        var walkInTotal = 0, phoneInTotal = 0, internetWalkInTotal = 0, groupTotal;
        walkIn = json[0];
        phoneIn = json[1];
        internetTotal = json[2];
        for(var i = 0; i <=6; i++){
            groupTotal = (walkIn[i] + phoneIn[i] +internetTotal[i]);
            walkInTotal += walkIn[i];
            phoneInTotal += phoneIn[i];
            internetWalkInTotal += internetTotal[i];
            totalVisits.push(groupTotal);
        }
        drawLineChartFill();
        $('.walk-in-total').text(parseFloat(walkInTotal).toFixed(2));
        $('.phone-total').text(parseFloat(phoneInTotal).toFixed(2));
        $('.internet-total').text(parseFloat(internetWalkInTotal).toFixed(2));
    };
};


var getWalkIn = function(){
    return walkIn;
};

var getPhoneIn = function(){
    return phoneIn;
};

var getInternetTotal = function(){
    return internetTotal;
};

var getTotalVisits = function(){
    console.log ('here ' + totalVisits);
    return totalVisits;
};

var drawLineChartFill = function(){
    $('#line-chart-fill').sparkline(getWalkIn(), {
        type: 'line',
        width: $('#line-chart-fill').width(),
        height: '250',
        // chartRangeMax: 300,
        chartRangeMin: 0,
        lineColor: '#3bafda',
        fillColor: 'rgba(59,175,218,0.3)',
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });

    $('#line-chart-fill').sparkline(getPhoneIn(), {
        type: 'line',
        width: $('#line-chart-fill').width(),
        height: '250',
        // chartRangeMax: 300,
        chartRangeMin: 0,
        lineColor: '#fff',
        fillColor: 'rgba(59,175,218,0.3)',
        composite: true,
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });

    $('#line-chart-fill').sparkline(getInternetTotal(), {
        type: 'line',
        width: $('#line-chart-fill').width(),
        height: '250',
        // chartRangeMax: 300,
        chartRangeMin: 0,
        lineColor: 'rgb(0, 177, 157)',
        fillColor: 'rgba(59,175,218,0.3)',
        composite: true,
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)',
    });


};
