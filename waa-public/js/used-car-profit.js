var pie2Properties = {
    type: 'pie',
    width: '250',
    height: '250',
    sliceColors: ['#3bafda', '#5cc7ff']
};

var proceedPie2 = true;
var checkPie2Status = function(){
    if (proceedPie2 === true){
        return true;
    }else{
        return false;
    }
};
var degrees2 = 0;

var displayUsedProfit = function(){
    $('.used-front').text(pieChart2Data[0]);
    $('.used-back').text(pieChart2Data[1]);
    $('.used-total').text(pieChart2Data[0] + pieChart2Data[1]);
};

var coinToss2 = function(){
    var operator2 = Math.floor(Math.random() *2) + 1;
    if(operator2 === 1){
        return true;
    }else{
        return false;
    }
};

var fluctuateAmount2 = function(){
    var amountToFluctuate0PC2 = Math.floor(Math.random() * (pieChart2Data[0] * 0.05))+1;
    var amountToFluctuate1PC2 = Math.floor(Math.random() * (pieChart2Data[1] * 0.05))+1;
    if (coinToss2() === true){
        pieChart2Data[0] = pieChart2Control[0] + amountToFluctuate0PC2;
    }else{
        pieChart2Data[0] = pieChart2Control[0] - amountToFluctuate0PC2;
    }

    if (coinToss2() === true){
        pieChart2Data[1] = pieChart2Control[1] + amountToFluctuate1PC2;
    }else{
        pieChart2Data[1] = pieChart2Control[1] - amountToFluctuate1PC2;
    }
};

var changePC2Colors = function(){
    if(degrees2 >= 360){
        degrees2 = 0;
    }else{
        degrees2 += 10;
    }
    var pieChart2Timer1 = window.setTimeout(function(){
            displayUsedProfit();
            pie2Properties.sliceColors = ['#3bafda', '#306079'];
            $('.used-front').css('color', '#3bafda');
            $('.used-back').css('color', '#fff');
            if(checkPie2Status() === true){
                drawPieChart2();
                var pieChart2Timer1 = window.setTimeout(function(){
                    pie2Properties.sliceColors = ['#306079', '#5cc7ff'];
                    $('.used-front').css('color', '#fff');
                    $('.used-back').css('color', '#5cc7ff');
                    if(checkPie2Status() === true){
                        drawPieChart2();
                        fluctuateAmount2();
                        changePC2Colors();
                        $('#used-car-profit').css({'transform':'rotate('+ degrees2 + 'deg)', '-ms-transform' : 'rotate('+ degrees2 + 'deg)', '-webkit-transform':'rotate('+ degrees2 + 'deg)'});
                    }else{
                        return;
                    }
                }, 2000);
            }else{
                return;
            }
    }, 2000);
};

var pieChart2Data =[];

var getPieChartData2 = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/pieChartData2.php', true);
    xhr.send();
    xhr.onload = function(){
        pieChart2Data = JSON.parse(xhr.response);
        pieChart2Control = pieChart2Data.slice();
        fluctuateAmount2();
        $('#used-car-profit').sparkline(pieChart2Data, pie2Properties);
    };
};

var drawPieChart2 = function(){
    $('#used-car-profit').sparkline(pieChart2Data, pie2Properties);
};
