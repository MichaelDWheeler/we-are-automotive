var apptNoShowData = [],
    apptNoShowNumbers = [];

var bcns = 0;

var colorMapNoShow = [];

var createColorMapNoShow = function(){
    for(var i = 0; i <=30; i++){
        colorMapNoShow.push('#3bafda');
    }
}();

var changeBarDataColor = function(){
    var defaultColorMapNoShow = colorMapNoShow.slice();
    defaultColorMapNoShow[bcns] = '#fff';
    return(defaultColorMapNoShow);
};

var displayBarChartNoShowData = function(){
    if(bcns >= 31){
        bcns = 0;
    }

    $('.date-display').text(apptNoShowData[bcns]);
    $('.appt-no-show').text(apptNoShowNumbers[bcns]);

    var barChartNoShowTimer = window.setTimeout(function(){
        ++bcns;
        displayBarChartNoShowData();
        drawBarChartNoShow();
    }, 5000);
};


var getAppointmentNoShowData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET','./php/appointment-no-show.php', true);
    xhr.send();
    xhr.onload = function(){
        var count = 0;
        json = JSON.parse(xhr.response);
        jsonLength = json[0].length;
        for (var i = 0; i < jsonLength; i++){
            apptNoShowData.push(json[0][i]);
            apptNoShowNumbers.push(json[1][i]);
        }
        displayBarChartNoShowData();
        $('#bar-chart-no-guides-2').sparkline(apptNoShowNumbers, apptNoShowProp);
    };
};

var apptNoShowProp= {
    type: 'bar',
    height: '225',
    barWidth: '10',
    barSpacing: '3',
    colorMap: changeBarDataColor()
};


var drawBarChartNoShow = function(){
    apptNoShowProp.colorMap = changeBarDataColor();
    $('#bar-chart-no-guides-2').sparkline(apptNoShowNumbers, apptNoShowProp);
    $('#bar-chart-no-guides-2').off();
};
