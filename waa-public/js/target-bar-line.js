    var targetBarLineData = [];
    var targetBarLineNumbers = [];

    var tbld1 = 0;
    var tbld2 = 1;
    var tbld3 = 2;

    var colorMapTbl =  [];

    var createColorMap = function(){
        for (var i = 0; i <= 30; i++){
            colorMapTbl.push('#3bafda');
        }
    };

    var changeTargetBarColors = function(){
        var defaultColorMapTbl = colorMapTbl.slice();
        defaultColorMapTbl[tbld1] = "#ffaa00";
        defaultColorMapTbl[tbld2] = "#f76397";
        defaultColorMapTbl[tbld3] = "#00b19d";
        return(defaultColorMapTbl);
    };

    var displayTargetBarLineData = function(){
        if (tbld1 >=31){
            tbld1 = 0;
        }
        if (tbld2 >=31){
            tbld2 = 0;
        }
        if (tbld3 >=31){
            tbld3 = 0;
        }

        $('.date-used-1').text(targetBarLineData[tbld1]);
        console.log(targetBarLineData[tbld1], tbld1);
        $('.date-used-1').css('color', '#ffaa00');
        $('.date-used-1-amount').text(targetBarLineNumbers[tbld1]);
        $('.date-used-2').text(targetBarLineData[tbld2]);
        $('.date-used-2').css('color', '#f76397');
        $('.date-used-2-amount').text(targetBarLineNumbers[tbld2]);
        $('.date-used-3').text(targetBarLineData[tbld3]);
        $('.date-used-3').css('color', '#00b19d');
        $('.date-used-3-amount').text(targetBarLineNumbers[tbld3]);

        var targetBarLineTimer1 = window.setTimeout(function(){
            ++tbld1;
            ++tbld2;
            ++tbld3;
            displayTargetBarLineData();
            drawTargetBarLine1();
        }, 5000);
    };

    var targetBarLineProp = {
        type: 'bar',
        height: '250',
        barWidth: '10',
        barSpacing: '3',
        barColor: '#3bafda',
        chartRangeMin: '0',
        colorMap: changeTargetBarColors()
    };

    var getTargetBarLineData = function(){
        createColorMap();
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/target-bar-line.php', true);
        xhr.send();
        xhr.onload = function(){
            var count = 0;
            json = JSON.parse(xhr.response);
            jsonLength = json[0].length;
            for (var i = 0; i < jsonLength; i++){
                targetBarLineData.push(json[0][i]);
                targetBarLineNumbers.push(json[1][i]);
            }
            displayTargetBarLineData();
            $('#target-bar-line').sparkline(targetBarLineNumbers, targetBarLineProp);
        };
    };

    var drawTargetBarLine1 = function () {
        console.log('here');
        targetBarLineProp.colorMap = changeTargetBarColors();
        $('#target-bar-line').empty();
        $('#target-bar-line').sparkline(targetBarLineNumbers, targetBarLineProp);
        $('#target-bar-line').off();
    };
