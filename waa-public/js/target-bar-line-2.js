var outboundCallsSkipped = [];
var outboundColorMap = [];

var getOutboundCallsSkippedData = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/outbound-calls-skipped.php', true);
    xhr.send();
    xhr.onload = function(){
        outboundCallsSkipped = JSON.parse(xhr.response);
        drawTargetBarLine();
    };
};

var createColorMapTbl2 = function(){
    for (var i = 0; i <= 30; i++){
        outboundColorMap.push('#00b19d');
    }
    outboundColorMap[30] ='#fff';
    return outboundColorMap;
};


var tblprop = {
    type: 'bar',
    height: '245',
    barWidth: '10',
    barSpacing: '3',
    colorMap: createColorMapTbl2()
};

var drawTargetBarLine = function(){
    $('#target-bar-line-2').sparkline(outboundCallsSkipped, tblprop);
    $('#target-bar-line-2').sparkline(outboundCallsSkipped, {
        type: 'line',
        width: $('#target-bar-line-2').width(),
        height: '245',
        lineColor: '#fb6d9d',
        fillColor: 'transparent',
        composite: true,
        highlightLineColor: 'rgba(0,0,0,.1)',
        highlightSpotColor: 'rgba(0,0,0,.2)'
    });
    $('#target-bar-line-2').off();
};

var outboundCallsSkippedTimer = window.setInterval(function(){
    var newNumber;
    var chance = Math.floor(Math.random()*100 + 1);

    if(chance > 95){
        newNumber = Math.floor(Math.random() * 10) + 1;
    }else if(chance > 90){
        newNumber = Math.floor(Math.random() * 9) + 1;
    }else if(chance > 80){
        newNumber = Math.floor(Math.random() * 8) + 1;
    }else if(chance > 70){
        newNumber = Math.floor(Math.random() * 7) + 1;
    }else if(chance > 60){
        newNumber = Math.floor(Math.random() * 6) + 1;
    }else if(chance > 50){
        newNumber = Math.floor(Math.random() * 5) + 1;
    }else{
        newNumber = Math.floor(Math.random() * 4) + 1;
    }

    outboundCallsSkipped.shift();
    outboundCallsSkipped.push(newNumber);
    drawTargetBarLine();
    $('.callsSkipped').text(newNumber);
},1000);
