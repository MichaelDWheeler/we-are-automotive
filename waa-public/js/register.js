(function () {
    'use strict';
    var width = $('body').width();
    var chartsDisplayedWidth = $('.charts-displayed').width();
    //creates a hashcode for password
    String.prototype.hashCode = function () {
        var hash = 0, i, chr, len;
        if (this.length === 0) return hash;
        for (i = 0, len = this.length; i < len; i++) {
            chr = this.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash * (len * len * len);
    };

    $('#login, #reg-done').click(function () {
        var loadContent = document.getElementById('login-content');
        var xhr = new XMLHttpRequest();
        xhr.open('GET', './php/login.php', true);
        xhr.send();
        xhr.onload = function () {
            var htmlContent = xhr.response;
            $(loadContent).empty();
            $(loadContent).append(htmlContent);
        };
    });

    var displayLoginLink = function () {
        var timer = window.setTimeout(function () {
            $('.login-link-2').fadeIn();
        }, 500);

    };

    var checkFirstName = function () {
        if ($('#first-name').val().length < 3) {
            return false;
        } else {
            $('#first-name').css('border', '');
        }
    };

    var checkLastName = function () {
        if ($('#last-name').val().length < 3) {
            return false;
        } else {
            $('#last-name').css('border', '');
        }
    };


    var emailCheck;


    var validateEmail = function (loginEmail) {
        var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        return re.test($(loginEmail).val());
    };

    var checkEmail = function (loginEmail) {
        console.log(loginEmail);
        if (validateEmail(loginEmail)) {
            $('#email-address').css('border', '');
            return true;
        } else {
            return false;
        }
    };

    var checkForm = function (data) {
        var loginEmail = $('#email-address');
        if (checkFirstName() === false) {
            $('#first-name').css('border', '3px solid red');
            return false;
        }
        if (checkLastName() === false) {
            $('#last-name').css('border', '3px solid red');
            return false;
        }
        if (checkEmail(loginEmail) === false) {
            $('#email-address').css('border', '3px solid red');
            return false;
        }
        if (data.loginPassword !== data.confirmPassword) {
            $('#login-password').css('border', '1px solid red');
            $('#confirm-password').css('border', '1px solid red');
            $('#login-password').val('');
            $('#confirm-password').val('');
            return false;
        } else {
            $('#login-password').css('border', '');
            $('#confirm-password').css('border', '');
        }
    };

    $('#register-button').on('keypress click', function (e) {
        if (e.which == 13 || e.type === 'click') {
            e.preventDefault();
            var data = {};
            data.firstName = $('#first-name').val();
            data.lastName = $('#last-name').val();
            data.emailAddress = $('#email-address').val();
            data.dealership = $('#dealership').val();
            data.position = $('#position').val();
            data.loginPassword = $('#login-password').val().hashCode().toString();
            data.confirmPassword = $('#confirm-password').val().hashCode().toString();
            if (checkForm(data) !== false) {
                var xhr = new XMLHttpRequest();
                xhr.open('POST', './php/waa-registration.php', true);
                xhr.send(JSON.stringify(data));
                xhr.onload = function () {
                    $('.login-link-1').css("visibility", "hidden");
                    $('.login-block').slideUp(displayLoginLink());
                };
            } else {
                console.log('the passwords do not match');
            }
        }
    });

    //cancels registration
    $('#cancel-registration').click(function () {
        $('.right-content').attr('class', 'right-content animated hidePanel');
        var delayTimer = window.setTimeout(function () {
            $('.charts-displayed').attr('class', 'charts-displayed animated returnDefault');
            if (width <= 720) {
                $('.backdrop').css('width', width + 'px');
            } else {
                $('.backdrop').css('width', chartsDisplayedWidth + 'px');
            }
        }, 600);
    });

}());
