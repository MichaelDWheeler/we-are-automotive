(function(){
    'use strict';
    var width = $('body').width();
    var chartsDisplayedWidth = $('.charts-displayed').width();

    // convert password to hashCode
    String.prototype.hashCode = function() {
        var hash = 0, i, chr, len;
        if (this.length === 0) return hash;
        for (i = 0, len = this.length; i < len; i++) {
            chr   = this.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash * (len * len * len);
    };

    var saveUserTemplate = function(){
        var res = _.map($('.grid-stack .grid-stack-item:visible').not("[style='none']"), function (el) {
            el = $(el);
            var node = {};
            node.x = el.attr('data-gs-x');
            node.y = el.attr('data-gs-y');
            node.width = el.attr('data-gs-width');
            node.height = el.attr('data-gs-height');
            node.id = el.attr('data-custom-id');
            return node;
        });
        saveTemplate = false;
        return(res);
    };

    var changeLoginButtons = function(){
        $('.login-manager').css('display', 'none');
        $('.logout-manager').css('display', 'block');
    };

 var saveBoard = function(data){
     data.boardName = $('#name-board').val();
     data.pageheight = $(document).height();
     data.layout = saveUserTemplate();
     data.saveTemplate = true;
     data.gridstackHeight = $('.grid-stack').data("gsCurrentHeight");
     var xhr = new XMLHttpRequest();
     xhr.open('POST', './php/saveboard.php', true);
     xhr.send(JSON.stringify(data));
     xhr.onload = function(){
         if(this.readyState === 4 && this.status === 200){
             var json = JSON.parse(xhr.response);
             if(json === 'success'){
                 window.location.href="./userprofile.php";
             }else{

                 alert("There is no match in the system.");
             }
     }
         cancelLogin();
     };

 };

 var loginSave = function(data){
     var loginPassword = $('#login-password').val();
     data.loginEmail = $('#email-address').val();
     data.loginPassword = loginPassword.hashCode().toString();
     data.boardName = $('#board-name').val();
     data.pageheight = $(document).height();
     data.layout = saveUserTemplate();
     data.saveTemplate = true;
     data.gridstackHeight = $('.grid-stack').data("gsCurrentHeight");
     var xhr = new XMLHttpRequest();
     xhr.open('POST', './php/adminloginform.php', true);
     xhr.send(JSON.stringify(data));
     xhr.onload = function(){
         if(this.readyState === 4 && this.status === 200){
             var json = JSON.parse(xhr.response);
             if(json === 'success'){
                 window.location.href="./userprofile.php";
             }else{

                 alert("There is no match in the system.");
             }
     }
         cancelLogin();
     };
 };

    var login = function(data){
        var loginPassword = $('#login-password').val();
        data.loginEmail = $('#email-address').val();
        data.loginPassword = loginPassword.hashCode().toString();
        var xhr = new XMLHttpRequest();
        xhr.open('POST', './php/adminloginform.php', true);
        xhr.send(JSON.stringify(data));
        xhr.onload = function(){
            if(this.readyState === 4 && this.status === 200){
                var json = JSON.parse(xhr.response);
                if(json === 'success'){
                    window.location.href="./userprofile.php";
                }else{

                    alert("There is no match in the system.");
                }
        }
            cancelLogin();
        };
    };

    //check login credentials
    $('.login-button').on('keypress click', function(e){
        e.preventDefault();
        if(e.which == 13 || e.type === 'click'){
            var data = {};
            if(saveTemplate === true && $('#name-board').length){
                saveBoard(data);
            }else if(saveTemplate === true){
                data.saveTemplate = true;
                loginSave(data);
            }else{
                data.saveTemplate = false;
                login(data);
            }
        }
    });

    //loads reset password section
    // $('#password-reset').click(function(){
    //     var xhr = new XMLHttpRequest();
    //     xhr.open ('GET', '/ajax/forgot-password.html', true);
    //     xhr.send();
    //     xhr.onload = function(){
    //         $(loadContent).empty();
    //         var htmlContent = xhr.response;
    //         $(loadContent).append(htmlContent);
    //     };
    // });

    //loads register section
    $('#register').click(function(){
        var xhr = new XMLHttpRequest();
        xhr.open ('GET', './ajax/register.html', true);
        xhr.send();
        xhr.onload = function(){
            $(loadContent).empty();
            var htmlContent = xhr.response;
            $(loadContent).append(htmlContent);
        };
    });
    var cancelLogin = function(){
        if (saveTemplate){
            saveTemplate = false;
        }
        $('.right-content').attr('class', 'right-content animated hidePanel');
        var delayTimer = window.setTimeout(function(){
            $('.charts-displayed').attr('class', 'charts-displayed animated returnDefault');
            if (width <= 720) {
                $('.backdrop').css('width', width + 'px');
            } else {
                $('.backdrop').css('width', chartsDisplayedWidth + 'px');
            }
        },600);
    };
    //cancels login process
    $('#cancel-login, #cancel-save').click(function(){
        cancelLogin();

    });
}());
