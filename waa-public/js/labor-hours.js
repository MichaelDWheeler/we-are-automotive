var lhMonths = [];
var lhBilled=[];

var dataLH = {
    labels: [],
    series: [
        [],
        [1,1,1,1,1,1]
    ]

};

var propertiesLH = {
    height: '420px',
    axisX: {
        // On the x-axis start means top and end means bottom
        position: 'start'
    },
    axisY: {
        // On the y-axis start means left and end means right
        position: 'end'
    }
};

var laborHours = function(){
    var xhr = new XMLHttpRequest();
    xhr.open('GET', './php/labor-hours.php', true);
    xhr.send();
    xhr.onload = function(){
        var json = JSON.parse(xhr.response);
        lhMonths = json[0];
        lhBilled = json[1];
        dataLH.labels = lhMonths;
        dataLH.series[0] = lhBilled;
        new Chartist.Bar('#labor-hours', dataLH, propertiesLH);
    };
};
