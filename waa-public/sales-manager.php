<?
session_start();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title id="page-title">DealerSocket | We Are Automotive</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="./stylesheets/all-min.css" rel="stylesheet">
	<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>
</head>
<body>

    <div class="container waa-heading">
        <div class="activateDar"></div>
        <div class="row waa-header">
            <div class="waa-logo">
                <a href="./index.php"><img class="shrink-mobile" src="./img/WeAreAutomotiveLogo-v2.png" alt="We Are Automotive"></a>
            </div>
            <div class="brand-container">
                <div class="brand-text">
                    Exclusive up-to-date market statistics<span class="larger-screen"> you can't get anywhere else.</span>
                </div>
            </div>
            <i class="fa fa-bars fa-2x mobile-nav" aria-hidden="true"></i>
            <div class="save-template">
                <i class="fa fa-save fa-2x" aria-hidden="true"></i>
                <div class="save-text animated">Save Template</div>
            </div>
            <ul class="social-links circle animated-effect-1 social-links-position">
                <li class="twitter"><a target="_blank" href="https://twitter.com/share" data-size="large" data-url="https://dev.twitter.com/web/tweet-button" data-via="twitterdev" data-related="twitterapi,twitter" data-hashtags="DealerSocket, We Are Automotive, WeAreAutomotive, Auto Dealers"
                        data-text="custom share text"><i class="fa fa-twitter"></i></a></li>
                <li class="facebook"><a target="_blank" class="facebook-share-button" href="https://www.facebook.com/sharer/sharer.php?u=http://www.we-are-automotive.com/"><i class="fa fa-facebook"></i></a></li>
                <li class="linkedin"><a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=http://www.we-are-automotive.com&title=We%20Are%20Automotive&summary=&source="><i class="fa fa-linkedin"></i></a></li>
                <li class="googleplus"><a target="_blank" href="https://plus.google.com/share?url=http://www.we-are-automotive.com"><i class="fa fa-google-plus"></i></a></li>
            </ul>
            <div class="autoscroll">
                Auto-Scroll: <span class="autoscroll-status">Off</span>
            </div>
            <div class="expand-toggle">
                <div class="toggle toggle-icons flow-right">
                    <i class="fa fa-toggle-off fa-2x" aria-hidden="true"></i>
                    <i class="fa fa-toggle-on fa-2x" aria-hidden="true"></i>
                </div>
                <i class="fa fa-expand expand-fullscreen" aria-hidden="true"></i>
                <i class="fa fa-expand close-fullscreen" aria-hidden="true"></i>
            </div>

        </div>
    </div>
    <!-- begin content -->
    <div class="p-10 main-container">
        <div class="left-navigation">
            <div class="general-manager icon-container">
                <a class="click-icons" href="./general-manager.php">
                    <i class="fa fa-group fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="gm-text">
                        <div class="v-center">
                            General Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="sales-manager icon-container">
                <a class="click-icons" href="./sales-manager.php">
                    <i class="fa fa-cubes fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="sales-text">
                        <div class="v-center">
                            Sales Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="service-manager icon-container">
                <a class="click-icons" href="./service-manager.php">
                    <i class="fa fa-cogs fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="service-text">
                        <div class="v-center">
                            Service Manager Template
                        </div>
                    </div>
                </a>
            </div>
            <div class="bdc-manager icon-container">
                <a class="click-icons" href="./bdc-manager.php">
                    <i class="fa fa-taxi fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="bdc-text">
                        <div class="v-center">
                            Business Development Center Template
                        </div>
                    </div>
                </a>
            </div>

            <div class="customize-manager icon-container" id="customize-waa">
                <i class="fa fa-code fa-2x nav-icon" aria-hidden="true"></i></br>
                <div class="customize-text">
                    <div class="v-center">
                        Customize Template
                    </div>
                </div>
            </div>
            <?

                if (isset($_SESSION['userid']))
                {
                    ?>
                    <div class="login-manager icon-container" id="logout-waa">
                        <i class="fa fa-sign-out fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="logout-text">
                            <div class="v-center">
                                Log Out
                            </div>
                        </div>
                    </div>
                    <?
                }
                else
                {
                    ?>
                    <div class="login-manager icon-container" id="login-waa">
                        <i class="fa fa-key fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="login-text">
                            <div class="v-center">
                                Login
                            </div>
                        </div>
                    </div>
                    <?
                }
                ?>
            <div class="refresh-manager icon-container" id="refresh-waa">
                <i class="fa fa-refresh fa-2x nav-icon" aria-hidden="true"></i></br>
                <div class="refresh-text">
                    <div class="v-center">
                        Refresh Board
                    </div>
                </div>
            </div>
        </div>
        <div class="charts-displayed">
            <div class="backdrop"></div>
            <div class="grid-stack grid-stack-12 m-l-140 m-r-20">
                <div class="grid-stack-item dealership-overview-grid" data-gs-x="0" data-gs-y="0" data-gs-width="12" data-gs-height="5" data-custom-id="1">
                    <div class="grid-stack-item-content">
                        <div class="box-prop" id="dealership-overview">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <h4 class="m-t-0 header-title">DEALERSHIP OVERVIEW</h4>
                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="appt-sold knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Appt Sold %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="vehicle-serviced knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Sold Vehicles Also Serviced %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="return-customer knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Be-Back %</h5>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center dealership-overview">
                                    <div class="m-b-25">
                                        <div class="box-prop">
                                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                                            <input class="visits-with-turn knob" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                            <h5 class="font-600 text-muted">Visits with Turn %</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="5" data-gs-width="6" data-gs-height="2" data-custom-id="2">
                    <div class="grid-stack-item-content">
                        <div class="box-prop top-widget" data-sortable-id="cars-sold-year" id="cars-sold-ytd-block">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <div class="single-pie">
                                <div id="cars-sold-pie"></div>
                            </div>
                            <div class="tw-stats">
                                <div class="ytd-sales-number">
                                    0 </br>
                                </div>
                                <div class="ytd-sales">
                                    Cars Sold This Year
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="7" data-gs-width="6" data-gs-height="2" data-custom-id="3">
                    <div class="grid-stack-item-content">
                        <div class="box-prop top-widget" data-sortable-id="cars-sold-month" id="cars-sold-month-block">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <div class="single-pie">
                                <div id="month-pie"></div>
                            </div>
                            <div class="tw-stats">
                                <div class="month-sales-number">
                                    0</br>
                                </div>
                                <div class="month-sales">
                                    Cars Sold This Month
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="9" data-gs-width="6" data-gs-height="2" data-custom-id="4">
                    <div class="grid-stack-item-content">
                        <div class="box-prop top-widget" data-sortable-id="cars-sold-minute" id="cars-sold-minute-block">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <div class="single-pie">
                                <div id="minute-pie"></div>
                            </div>
                            <div class="tw-stats">
                                <div class="minute-sales-number">
                                    0</br>
                                </div>
                                <div class="minute-sales">
                                    Cars Sold Per Minute
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="11" data-gs-width="6" data-gs-height="2" data-custom-id="5">
                    <div class="grid-stack-item-content">
                        <div class="box-prop top-widget" data-sortable-id="cars-sold-dealersocket" id="dealersocket-sales-block">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <div class="single-pie">
                                <div id="dealersocket-pie"></div>
                            </div>
                            <div class="tw-stats">
                                <div class="dealersocket-sales-number">
                                    0</br>
                                </div>
                                <div class="dealersocket-sales">
                                    Cars Sold Out Of DealerSocket This Year
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="6" data-gs-y="5" data-gs-width="3" data-gs-height="6" data-custom-id="8">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-line box-prop" data-sortable-id="revenue-sparkline" id="chart-3">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> USED CAR PROFIT
                            <div class="row">
                                <div id="used-car-profit"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Front</br>
                                    $<span class="used-front">0</span>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Back</br>
                                    $<span class="used-back">0</span>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Total</br>
                                    $<span class="used-total">0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="9" data-gs-y="5" data-gs-width="3" data-gs-height="6" data-custom-id="10">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-pie box-prop" data-sortable-id="target-revenue-pie" id="chart-5">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> NEW CAR PROFIT
                            <div class="row">
                                <div id="new-car-profit"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Front</br>
                                    $<span class="new-front">0</span>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Back</br>
                                    $<span class="new-back">0</span>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    Average Total</br>
                                    $<span class="new-total">0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="6" data-gs-y="11" data-gs-width="3" data-gs-height="6" data-custom-id="12">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-spike box-prop" data-sortable-id="target-revenue-spike" id="chart-7">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> SOLD USED PAST 30 DAYS AVERAGE
                            <div class="row">
                                <div id="target-bar-line"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-used-1">
                                        date</br>
                                    </div>
                                    <div class="date-used-1-amount">10</div>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-used-2">
                                        date</br>
                                    </div>
                                    <div class="date-used-2-amount">10</div>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-used-3">
                                        date</br>
                                    </div>
                                    <div class="date-used-3-amount">10</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="9" data-gs-y="11" data-gs-width="3" data-gs-height="6"  data-custom-id="9">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-bar box-prop" data-sortable-id="revenue-bar" id="chart-4">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> SOLD NEW PAST 30 DAYS AVERAGE
                            <div class="row">
                                <div id="sold-new-past-30-days-average"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-display-1">
                                        date</br>
                                    </div>
                                    <div class="date-display-1-amount">10</div>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-display-2">
                                        date</br>
                                    </div>
                                    <div class="date-display-2-amount">10</div>
                                </div>
                                <div class="col-xs-4 revenue-sl-box">
                                    <div class="date-display-3">
                                        date</br>
                                    </div>
                                    <div class="date-display-3-amount">10</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="13" data-gs-width="6" data-gs-height="8" data-custom-id="6">
                    <div class="grid-stack-item-content">
                        <div class="interactive-map-container box-prop" data-sortable-id="map" id="chart-1">
                            <i class="fa fa-times close-x" aria-hidden="true"></i>
                            <div class="scrollElement" id="interactive-map"></div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="6" data-gs-y="17" data-gs-width="6" data-gs-height="8" data-custom-id="14">
                    <div class="grid-stack-item-content">
                        <div class="line-series box-prop" data-sortable-id="line-series" id="chart-9">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> AVERAGE DAYS SOLD
                            <div class="text-center">
                                <ul class="list-inline chart-detail-list">
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #00b19d;"></i>Phone</h5>
                                    </li>
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #64b5f6;"></i>Fresh Up</h5>
                                    </li>
                                    <li>
                                        <h5><i class="fa fa-circle m-r-5" style="color: #dcdcdc;"></i>Internet</h5>
                                    </li>
                                </ul>
                            </div>
                            <div id="average-days-sold"></div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="0" data-gs-y="21" data-gs-width="3" data-gs-height="6" data-custom-id="11">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-line-no-fill box-prop scrollElement " data-sortable-id="target-revenue-line-no-fill" id="chart-6">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> CARS SOLD NEW VS USED 7 DAYS
                            <span class="text-muted m-b-30 font-13">
                                <div class="new-legend inline"></div>
                                <span class="inline text-muted">New</span>
                                <div class="used-legend inline"></div>
                                <span class="inline text-muted">Used</span>
                            </span>
                            <div class="row">
                                <div id="line-chart-no-fill"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 revenue-sl-box">
                                    New</br>
                                    <div class="new-sold">0</div>
                                </div>
                                <div class="col-xs-6 revenue-sl-box">
                                    Used</br>
                                    <div class="used-sold">0</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="grid-stack-item" data-gs-x="3" data-gs-y="21" data-gs-width="3" data-gs-height="6" data-custom-id="17">
                    <div class="grid-stack-item-content">
                        <div class="target-revenue-bar-2 box-prop" data-sortable-id="target-revenue-bar-2" id="chart-12">
                            <i class="fa fa-times close-x" aria-hidden="true"></i> APPOINTMENT NO SHOW OVER 30 DAYS
                            <span class="text-muted m-b-30 font-13">
                                <div class="used-legend inline"></div>
                                <span class="inline text-muted">Current</span>
                                <div class="row">
                                    <div id="bar-chart-no-guides-2"></div>
                                </div>
                                <div class="row metrics">
                                    <div class="col-xs-12 revenue-sl-box">
                                        <span class="date-display">Date</br></span>
                                        <div class="appt-no-show">0</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid-stack-item" data-gs-x="0" data-gs-y="27" data-gs-width="6" data-gs-height="6" data-custom-id="19">
                        <div class="grid-stack-item-content">
                            <div class="target-revenue-line-no-fill-2 box-prop" id="chart-14" data-sortable-id="target-revenue-line-no-fill-2">
                                <i class="fa fa-times close-x" aria-hidden="true"></i> VISITS SOLD
                                <span class="text-muted m-b-30 font-13">
                                    <div class="light-blue-legend inline"></div>
                                    <span class="inline text-muted">Internet Lead</span>
                                    <div class="pink-legend inline"></div>
                                    <span class="inline text-muted">Phone Lead</span>
                                    <div class="fresh-up-legend inline"></div>
                                    <span class="inline text-muted">Fresh Up</span>
                                    <div class="row">
                                        <div id="line-chart-no-fill-2"></div>
                                    </div>
                                    <div class="row metrics">
                                        <div class="col-xs-4 revenue-sl-box">
                                            <span class="light-blue">Internet Lead</span></br>
                                            <div class="internetLeadVS">0</div>
                                        </div>
                                        <div class="col-xs-4 revenue-sl-box">
                                            <span class="pink">Phone Lead</br></span>
                                            <div class="phoneLeadVS">0</div>
                                        </div>
                                        <div class="col-xs-4 revenue-sl-box">
                                            Fresh Up Lead</br>
                                            <div class="freshUpLeadVS">0</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item" data-gs-x="6" data-gs-y="25" data-gs-width="6" data-gs-height="8" data-custom-id="15">
                            <div class="grid-stack-item-content">
                                <div class="donut-accented box-prop" data-sortable-id="donut-accented" id="chart-10">
                                    <i class="fa fa-times close-x" aria-hidden="true"></i> APPOINTMENTS
                                    <div id="morris-donut"></div>
                                    <div class="text-center">
                                        <ul class="list-inline chart-detail-list">
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #64b5f6;"></i>Appt Confirmed %</h5>
                                            </li>
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color: #00b19d;"></i>Appt Show %</h5>
                                            </li>
                                            <li>
                                                <h5><i class="fa fa-circle m-r-5" style="color:  #ededed;"></i>Appt Sold %</h5>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item" data-gs-x="0" data-gs-y="33" data-gs-width="6" data-gs-height="8" data-custom-id="22">
                            <div class="grid-stack-item-content">
                                <div class="scrollElement horizontal-bar box-prop" data-sortable-id="horizontal-bar" id="chart-17">
                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                    <h4 class="m-t-0 header-title">AVERAGE FRONT PROFIT VS BACK PROFIT</h4>
                                    <span class="text-muted m-b-30 font-13">
                                        <div class="red-legend inline"></div>
                                        <span class="inline text-muted">Back profit</span>
                                        <div class="blue-legend inline"></div>
                                        <span class="inline text-muted">Front profit</span>
                                    </span>
                                    <div id="horizontal-bar-chart" class="ct-chart ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item" data-gs-x="6" data-gs-y="33" data-gs-width="6" data-gs-height="8" data-custom-id="23">
                            <div class="grid-stack-item-content">
                                <div class="vertical-bar box-prop" data-sortable-id="vertical-bar-1" id="chart-18">
                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                    <h4 class="m-t-0 header-title">AVERAGE PROSPECTS SOLD</h4>
                                    <span class="text-muted m-b-30 font-13">
                                        <div class="blue-legend inline"></div>
                                        <span class="inline text-muted">Floor</span>
                                        <div class="red-legend inline"></div>
                                        <span class="inline text-muted">Internet</span>
                                        <div class="green-legend inline"></div>
                                        <span class="inline text-muted">Phone</span>
                                        <div class="light-blue-legend inline"></div>
                                        <span class="inline text-muted">Total</span>
                                    </span>
                                    <div id="prospects-sold" class="ct-chart ct-golden-section"></div>
                                </div>
                            </div>
                        </div>
                        <div class="grid-stack-item automotive-metrics-grid" data-gs-x="0" data-gs-y="65" data-gs-width="12" data-gs-height="10" data-custom-id="26">
                            <div class="grid-stack-item-content">
                                <div class="box-prop" id="knobs">
                                    <div class="ipad-view">
                                        <i class="fa fa-times close-x" aria-hidden="true"></i>
                                        <h4 class="m-t-0 header-title">AUTOMOTIVE METRICS</h4>
                                        <p class="text-muted m-b-30 font-13">
                                            Average displayed by percentage.
                                        </p>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob outbound-calls-per-day" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3bafda" value="0" />
                                                    <h5 class="font-600 text-muted"><span class="completed">0</span> Outbound Calls Completed Today</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob closed-ro-appt" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#00b19d" value="0" />
                                                    <h5 class="font-600 f text-muted">Closed RO with Appointment Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob closed-ro-future" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#ffaa00" value="0" />
                                                    <h5 class="font-600 text-muted">Closed RO with Future Appointment Set Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-with-trade" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#3ddcf7" value="0" />
                                                    <h5 class="font-600 text-muted">Sold With Trade Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-return-customer" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#f76397" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Return Customer Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-fresh-up" data-width="150" data-height="150" data-bgColor="#505A66" value="0" data-fgColor="#7266ba" />
                                                    <h5 class="font-600 text-muted">Sold Fresh Up Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-phone-up" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#98a6ad" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Phone Up Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">
                                            <div class="m-b-25">
                                                <div class="box-prop auto-metrics">
                                                    <i class="fa fa-times close-x" aria-hidden="true"></i>
                                                    <input class="knob sold-internet" data-width="150" data-height="150" data-bgColor="#505A66" data-fgColor="#ef5350" value="0" />
                                                    <h5 class="font-600 text-muted">Sold Internet Percent</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

            </div>
        </div>
        <!-- hidden forms -->
        <div class="right-content" style="display: none;">
            <!-- begin login-header -->
            <div class="login-header">
                <div id="login-content">
                </div>
            </div>
            <!-- begin login-content -->
            <div class="login-content">
                <hr />
                <p class="text-center">
                    &copy; DealerSocket All Right Reserved 2016
                </p>
            </div>
            <!-- end login-content -->
        </div>
        <div class="blocker"></div>
        <div class="download-dar animated fadeInDownBig">
            <div class="close-dar-background"></div>
            <i class="fa fa-times-circle fa-2x close-dar-x" aria-hidden="true"></i>
            <div class="dar-container">
                <span class="dar-heading"><em><h4>Dealership Action Reports</h4></em></span><span class="dar">Shaped with data sourced from more than 6,500 dealerships around the world, the Dealership Action Report (DAR) is a resource developed by DealerSocket’s expert team to guide dealers to data-driven processes that drive sales, retention, and profitability.</span></br>
                </br>
                <div class="franchise-download">
                    <a href="http://info.dealersocket.com/DAR-Asset-Download.html" target="blank">Franchise</a>
                </div>
                <div class="independent-download">
                    <a href="http://info.dealersocket.com/idar.html" target="blank">Independent</a>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- ================== BEGIN PAGE LEVEL JS ================== -->
    	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="./js/all-min.js"></script>

    <!-- end content -->
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
    </div>
    <!-- end page container -->

    <!-- ================== BEGIN BASE JS FROM FOOTER ================== -->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3743008-14', 'auto');
  ga('send', 'pageview');

</script>

<!-- RTP tag -->
<script type='text/javascript'>
(function(c,h,a,f,i){c[a]=c[a]||function(){(c[a].q=c[a].q||[]).push(arguments)};
 c[a].a=i;var g=h.createElement("script");g.async=true;g.type="text/javascript";
 g.src=f+'?aid='+i;var b=h.getElementsByTagName("script")[0];b.parentNode.insertBefore(g,b);
 })(window,document,"rtp","//sjrtp5-cdn.marketo.com/rtp-api/v1/rtp.js","dealersocket");

rtp('send','view');
rtp('get', 'campaign',true);
</script>
<!-- End of RTP tag -->


    <!--[if lt IE 9]>
                <script src="assets/crossbrowserjs/html5shiv.js"></script>
                <script src="assets/crossbrowserjs/respond.min.js"></script>
                <script src="assets/crossbrowserjs/excanvas.min.js"></script>
                <![endif]-->
    <!-- ================== END BASE JS ================== -->

    <!-- ================== BEGIN PAGE LEVEL JS ================== -->



    <!-- ================== END PAGE LEVEL JS ================== -->

</body>

</html>
