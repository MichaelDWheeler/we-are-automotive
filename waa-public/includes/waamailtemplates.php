﻿<?





//////processor area/////
// function process_email_twopart($to,$ccgroup,$from,$replyto,$subject,$plaintext,$html)
function process_email_twopart($to,$from,$replyto,$subject,$plaintext,$html)
{

//define the subject of the email


// Generate a random boundary string
$mime_boundary = '_x'.sha1(time()).'x';

// Using the heredoc syntax to declare the headers
$headers = <<<HEADERS
From: $from
// CC: $ccgroup
MIME-Version: 1.0
Content-Type: multipart/alternative;
 boundary="PHP-alt$mime_boundary"
HEADERS;

// Use our boundary string to create plain text and HTML versions
$message = <<<MESSAGE
--PHP-alt$mime_boundary
Content-Type: text/plain; charset="iso-8859-1"
Content-Transfer-Encoding: 7bit

$plaintext

--PHP-alt$mime_boundary
Content-type: text/html; charset=iso-8859-1
Content-Transfer-Encoding: 7bit

$html

--PHP-alt$mime_boundary--
MESSAGE;

    // Send the message
    if(!mail($to, $subject, $message, $headers))
    {
    // If the mail function fails, return an error message
    echo "Something went wrong!";
    }
    else
    {
    // Return a success message if nothing went wrong
    echo "Message sent successfully. Check your email!";
    }

}






/////end processor area///


////password recovery////

///text password recovery email///


///end html welcome email///

////thank you for demoing email/////
function return_validate_email_text($userid)
{
$wtemail="

Thank you for stopping by our booth and trying out DealerSocket. I hope I answered all of your questions.

When you work in segmented, disparate silos, solutions are complicated. We built our product suite to guide and optimize the customer experience through all stages of the customer lifecycle. Everything is easier with DealerSocket’s fully integrated technology platform.

If you have any further questions, don’t hesitate to drop me a line.

Also, I’d like to offer you a copy of our latest article User Experience = Customer Experience.

http://weareautomotive.com/validateuser.php?userid=".$userid."

Best,


To learn more about DealerSocket, click here. - http://www.dealersocket.com

Call: 866.684.3157
Email: sales@dealersocket.com
Mail: 100 Avenida La Pata, San Clemente, CA 92673

Facebook: http://www.facebook.com/dealersocket
Twitter: http://www.twitter.com/dealersocket
YouTube: http://www.youtube.com/user/dealersocket
LinkedIn: http://www.linkedin.com/company/dealersocket

";
return $wtemail;
}
///end text welcome email///


///hmtl welcome email///
function return_validate_email_html($userid)
{


$whemail=' <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>We Are Automotive</title>
    <style type="text/css">

            #outlook a{padding:0;}
            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
            body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
            img{-ms-interpolation-mode:bicubic;}

            body{margin:0; padding:0;}
            img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
            table{border-collapse:collapse !important;}
            body, #bodyTable, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;}


            #bodyCell{padding:20px;}
            #templateContainer{width:600px;}


            body, #bodyTable{
                 background-color:#DEE0E2;
            }


            #bodyCell{
                 border-top:4px solid #BBBBBB;
            }


            #templateContainer{
                 border:1px solid #BBBBBB;
            }

            h1{
                 color:#202020 !important;
                display:block;
                 font-family:Helvetica;
                 font-size:26px;
                 font-style:normal;
                 font-weight:bold;
                 line-height:100%;
                 letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                 text-align:left;
            }

            h2{
                 color:#404040 !important;
                display:block;
                 font-family:Helvetica;
                 font-size:20px;
                 font-style:normal;
                 font-weight:bold;
                 line-height:100%;
                 letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                 text-align:left;
            }


            h3{
                 color:#606060 !important;
                display:block;
                 font-family:Helvetica;
                 font-size:16px;
                 font-style:italic;
                 font-weight:normal;
                 line-height:100%;
                 letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                 text-align:left;
            }


            h4{
                 color:#808080 !important;
                display:block;
                 font-family:Helvetica;
                 font-size:14px;
                 font-style:italic;
                 font-weight:normal;
                 line-height:100%;
                 letter-spacing:normal;
                margin-top:0;
                margin-right:0;
                margin-bottom:10px;
                margin-left:0;
                 text-align:left;
            }


            #templatePreheader{
                 background-color:#F4F4F4;
                 border-bottom:1px solid #CCCCCC;
            }

            .preheaderContent{
                 color:#808080;
                 font-family:Helvetica;
                 font-size:10px;
                 line-height:125%;
                 text-align:left;
            }


            .preheaderContent a:link, .preheaderContent a:visited,{
                 color:#606060;
                 font-weight:normal;
                 text-decoration:underline;
            }


            #templateHeader{
                 background-color:#F4F4F4;
                 border-top:1px solid #FFFFFF;
                 border-bottom:1px solid #CCCCCC;
            }


            .headerContent{
                 color:#505050;
                 font-family:Helvetica;
                 font-size:20px;
                 font-weight:bold;
                 line-height:100%;
                 padding-top:0;
                 padding-right:0;
                 padding-bottom:0;
                 padding-left:0;
                 text-align:left;
                 vertical-align:middle;
            }


            .headerContent a:link, .headerContent a:visited, /* Yahoo! Mail Override */ .headerContent a .yshortcuts /* Yahoo! Mail Override */{
                 color:#0097ce;
                 font-weight:normal;
                 text-decoration:underline;
            }

            #headerImage{
                height:auto;
                max-width:600px;
            }

            #templateBody{
                 background-color:#F4F4F4;
                 border-top:1px solid #FFFFFF;
                 border-bottom:1px solid #CCCCCC;
            }


            .bodyContent{
                 color:#505050;
                 font-family:Helvetica;
                 font-size:16px;
                 line-height:150%;
                padding-top:20px;
                padding-right:20px;
                padding-bottom:20px;
                padding-left:20px;
                 text-align:left;
            }


            .bodyContent a:link, .bodyContent a:visited, /* Yahoo! Mail Override */ .bodyContent a .yshortcuts /* Yahoo! Mail Override */{
                 color:#0097ce;
                 font-weight:normal;
                 text-decoration:underline;
            }

            .bodyContent img{
                display:inline;
                height:auto;
                max-width:560px;
            }


            .templateColumnContainer{width:260px;}


            #templateColumns{
                 background-color:#F4F4F4;
                 border-top:1px solid #FFFFFF;
                 border-bottom:1px solid #CCCCCC;
            }


            .leftColumnContent{
                 color:#505050;
                 font-family:Helvetica;
                 font-size:14px;
                 line-height:150%;
                padding-top:0;
                padding-right:20px;
                padding-bottom:20px;
                padding-left:20px;
                 text-align:left;
            }


            .leftColumnContent a:link, .leftColumnContent a:visited, /* Yahoo! Mail Override */ .leftColumnContent a .yshortcuts /* Yahoo! Mail Override */{
                 color:#0097ce;
                 font-weight:normal;
                 text-decoration:underline;
            }


            .rightColumnContent{
                 color:#505050;
                 font-family:Helvetica;
                 font-size:14px;
                 line-height:150%;
                padding-top:0;
                padding-right:20px;
                padding-bottom:20px;
                padding-left:20px;
                 text-align:left;
            }

            .rightColumnContent a:link, .rightColumnContent a:visited, /* Yahoo! Mail Override */ .rightColumnContent a .yshortcuts /* Yahoo! Mail Override */{
                 color:#0097ce;
                 font-weight:normal;
                 text-decoration:underline;
            }

            .leftColumnContent img, .rightColumnContent img{
                display:inline;
                height:auto;
                max-width:260px;
            }

            #templateFooter{
                 background-color:#F4F4F4;
                 border-top:1px solid #FFFFFF;
            }


            .footerContent{
                 color:#808080;
                 font-family:Helvetica;
                 font-size:10px;
                 line-height:150%;
                padding-top:20px;
                padding-right:30px;
                padding-bottom:20px;
                padding-left:30px;
                 text-align:left;
            }


            .footerContent a:link, .footerContent a:visited, /* Yahoo! Mail Override */ .footerContent a .yshortcuts, .footerContent a span /* Yahoo! Mail Override */{
                 color:#606060;
                 font-weight:normal;
                 text-decoration:underline;
            }

            @media only screen and (max-width: 480px){

                body, table, td, p, a, li, blockquote{-webkit-text-size-adjust:none !important;} /* Prevent Webkit platforms from changing default text sizes */
                body{width:100% !important; min-width:100% !important;} /* Prevent iOS Mail from adding padding to the body */


                #bodyCell{padding:10px !important;}


                .social{
                padding: 0 5px !important;
                }

                /* ======== Page Styles ======== */


                #templateContainer{
                    max-width:600px !important;
                     width:100% !important;
                }


                h1{
                     font-size:24px !important;
                     line-height:100% !important;
                }


                h2{
                     font-size:20px !important;
                     line-height:100% !important;
                }


                h3{
                     font-size:18px !important;
                     line-height:100% !important;
                }

                h4{
                     font-size:16px !important;
                     line-height:100% !important;
                }


                #templatePreheader{display:none !important;} /* Hide the template preheader to save space */


                #headerImage{
                    height:auto !important;
                     max-width:600px !important;
                     width:100% !important;
                }


                .headerContent{
                     font-size:20px !important;
                     line-height:125% !important;
                }
                .topheader{
                    display:block !important;
                    padding-top:0px !important;
                }
                .numbercenter{
                    text-align: center !important;
                    padding:0 0 20px 0!important;
                }
                .logocenter{
                    text-align: center !important;
                    padding:20px 0 0 0 !important;
                }

                .bodyContent{
                     font-size:18px !important;
                     line-height:125% !important;
                }


                .templateColumnContainer{display:block !important; width:100% !important;}


                .columnImage{
                    height:auto !important;
                     max-width:480px !important;
                     width:100% !important;
                }


                .leftColumnContent{
                     font-size:16px !important;
                     line-height:125% !important;
                }


                .rightColumnContent{
                     font-size:16px !important;
                     line-height:125% !important;
                }
                .rightColumnContentCTA{
                     font-size:16px !important;
                     line-height:125% !important;
                     padding-top: 40px !important;
                     padding-bottom: 0px !important;
                }
                .rightColumnContentCTAimg{
                     font-size:16px !important;
                     line-height:125% !important;
                     padding-top: 0px !important;
                     padding-bottom: 40px !important;
                }


                .footerContent{
                     font-size:12px !important;
                     line-height:115% !important;
                }

                .unsubscribe{
                padding-top:20px !important;
                }

                .CTA{
                     font-size:18px !important;
                     line-height:115% !important;
                }

                .footerContent a{display:block !important;} /* Place footer social and utility links on their own lines, for easier access */
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;background-color: #FFFFFF;height: 100% !important;width: 100% !important;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;background-color: #F4F4F4;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 20px;border-top: 0px solid #BBBBBB;height: 100% !important;width: 100% !important;">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 0px solid #BBBBBB;border-collapse: collapse !important;">
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #000000;border-top: 0px solid #FFFFFF;border-bottom: 0px solid #CCCCCC;border-collapse: collapse !important;">
                                        <tr mc:repeatable>
                                            <td align="center" valign="middle" class="templateColumnContainer" style="padding-top: 20px;padding-bottom: 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 260px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr align="center">
                                                        <td class="logocenter" style="padding-top: 0px;padding-right: 0px;padding-left: 30px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;text-align: left;" mc:edit="preheader_content00"><a href="http://weareautomotive.com" target="_blank"><img src="http://devsocket.com/emails/WAA/WAA_White_Medium.png" alt="DealerSocket"/></a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="middle" valign="middle" class="templateColumnContainer topheader" style="padding-top: 26px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 260px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr>
                                                        <td class="numbercenter" style="padding-top: 0px;padding-right: 30px;padding-bottom: 20px;padding-left: 0;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #FFFFFF;font-family: Helvetica;font-size: 18px;line-height: 125%;text-align: right; letter-spacing:.0em;">
                                                        866.813.1429
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                         </table>

                         <div style="padding:5px;"></div>
                         <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 0px solid #BBBBBB;border-collapse: collapse !important;">
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-top: 0px solid #FFFFFF;border-bottom: 0px solid #CCCCCC;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #505050;font-family: Helvetica;font-size: 16px;line-height: 150%;padding-top: 40px;padding-right: 30px;padding-bottom: 40px;padding-left: 30px;text-align: left;">
                                                <h2 style="display: block;font-family: Helvetica;font-size: 20px;font-style: normal; text-transform:uppercase; font-weight: bold;line-height: 100%;letter-spacing: .05em;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #404040 !important;">ACCOUNT CREATED!</h2>
                                                <h4 style="display: block;font-family: Helvetica;font-size: 14px;font-style: italic;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 30px;margin-left: 0;text-align: left;color: #808080 !important;">Click the button below to activate your We Are Automotive account.</h4>
                                                <p>Customize your dashboard. Drag and drop your widgets wherever you like.</p>
                                                <p>Once your dashboard is modified, a button will appear up top to save your changes.</p>
                                                <p>After log in, navigate to your admin panel to share any board you have created on social media or via email.</p>
                                                <p>Enjoy exclusive, real-time data that you can\'t get anywhere else in the auto industry - powered by DealerSocket.</p>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                         </table>

                            <div style="padding:5px;"></div>

                            <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 0px solid #BBBBBB;border-collapse: collapse !important;">
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background-color: #009dce; border-top: 0px solid #FFFFFF; border-bottom: 0px solid #CCCCCC; border-collapse: collapse !important;">
                                        <tbody>
                                            <tr mc:repeatable="">
                                                <td align="center" valign="top" class="templateColumnContainer" style="padding-top: 20px; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 260px;"><a href="#" style="text-decoration: none;"></a>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important;">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" class="leftColumnContent CTA" mc:edit="left_column_content" style="-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; mso-table-lspace: 0pt; mso-table-rspace: 0pt; color: #ffffff; font-family: Helvetica; font-size: 20px; line-height: 150%; letter-spacing: .2em; text-align: center; padding: 0 20px 20px 20px;"><a href="http://weareautomotive.com/validateuser.php?userid='.$userid.'" target="_blank" style="color: #fff; text-decoration: none!important;">Validate Your Account</a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>

                            <div style="padding:5px;"></div>

                      <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border: 0px solid #BBBBBB;border-collapse: collapse !important;">
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #000000;border-top: 0px solid #FFFFFF;border-bottom: 0px solid #CCCCCC;border-collapse: collapse !important;">
                                        <tr mc:repeatable>
                                            <td align="center" valign="top" class="templateColumnContainer" style="padding-top: 20px; padding-bottom: 20px; -webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 220px;">
                                                <table class="social" border="0" cellpadding="0" cellspacing="0" width="90%" style="margin-left:5%; margin-right:5%; -webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr align="center">
                                                            <td height="25" width="16%"><a href="http://www.facebook.com/dealersocket" target="_blank"><img alt="facebook" height="25" src="http://devsocket.com/emails/fb-icon.png" width="25px"/></a></td>
                                                            <td height="25" width="16%"><a href="http://www.twitter.com/dealersocket" target="_blank"><img alt="twitter" height="25" src="http://devsocket.com/emails/twitter-icon.png" width="25px"/></a></td>
                                                            <td height="25" width="16%"><a href="http://plus.google.com/108226902008531883171/posts" target="_blank"><img alt="google+" height="25" src="http://devsocket.com/emails/googleplus-icon.png" width="25px"/></a></td>
                                                            <td height="25" width="16%"><a href="http://www.youtube.com/user/dealersocket" target="_blank"><img alt="youtube" height="25" src="http://devsocket.com/emails/youtube-icon.png" width="25px"/></a></td>
                                                            <td height="25" width="16%"><a href="http://instagram.com/dealersocket" target="_blank"><img alt="instagram" height="25" src="http://devsocket.com/emails/instagram-icon.png" width="25px"/></a></td>
                                                            <td height="25" width="16%"><a href="http://www.linkedin.com/company/dealersocket" target="_blank"><img alt="linkedin" height="25" src="http://devsocket.com/emails/linkedin-icon.png" width="25px"/></a></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td align="center" valign="top" class="templateColumnContainer" style="padding-top: 20px; -webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 260px;">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr>
                                                        <td>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>
';

return $whemail;
}

////end thank you for demoing email////


?>
