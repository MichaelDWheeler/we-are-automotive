<?
session_start();
$userid=0;
$userid=$_SESSION['userid'];
$result=0;
if ($userid==0)
{
header("Location: ./index.php?heymichael=useridzero");
}
if ($userid='')
{
header("Location: ./index.php?heymichael=blank");
}
require "./php/waafunctions.php";

if (array_key_exists('userid-profile',$_POST))
{
    $result=update_user_profile($_POST['userid-profile'],$_POST['firstname'],$_POST['lastname'],$_POST['emailaddress'],$_POST['dealership'],$_POST['position'],$_POST['password']);
}
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title id="page-title">DealerSocket | We Are Automotive</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="./stylesheets/all-min.css" rel="stylesheet">
	<script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  <script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"
  integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E="
  crossorigin="anonymous"></script>
</head>
<body>

    <div class="container waa-heading">
        <div class="activateDar"></div>
        <div class="row waa-header">
            <div class="waa-logo">
                <a href="./index.php"><img class="shrink-mobile" src="./img/WeAreAutomotiveLogo-v2.png" alt="We Are Automotive"></a>
            </div>
            <div class="brand-container">
                <div class="brand-text">
                    Exclusive up-to-date market statistics<span class="larger-screen"> you can't get anywhere else.</span>
                </div>
            </div>
            <i class="fa fa-bars fa-2x mobile-nav" aria-hidden="true"></i>


                <div class="autoscroll">
                   <a href="./logout.php"><h4>Log Out</h4></a>
                </div>


            </div>
        </div>
        <!-- begin content -->
        <div class="p-10 main-container">
            <div class="left-navigation">

                <div class="general-manager icon-container">
                    <a class="click-icons" href="./general-manager.php">
                        <i class="fa fa-group fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="gm-text">
                            <div class="v-center">
                                General Manager Template
                            </div>
                        </div>
                    </a>
                </div>
                <div class="sales-manager icon-container">
                    <a class="click-icons" href="./sales-manager.php">
                        <i class="fa fa-cubes fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="sales-text">
                            <div class="v-center">
                                Sales Manager Template
                            </div>
                        </div>
                    </a>
                </div>
                <div class="service-manager icon-container">
                    <a class="click-icons" href="./service-manager.php">
                        <i class="fa fa-cogs fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="service-text">
                            <div class="v-center">
                                Service Manager Template
                            </div>
                        </div>
                    </a>
                </div>
                <div class="bdc-manager icon-container">
                    <a class="click-icons" href="./bdc-manager.php">
                        <i class="fa fa-taxi fa-2x nav-icon" aria-hidden="true"></i></br>
                        <div class="bdc-text">
                            <div class="v-center">
                                Business Development Center Template
                            </div>
                        </div>
                    </a>
                </div>
                <div class="logout-manager icon-container not-hidden" id="logout-waa">
                    <i class="fa fa-sign-out fa-2x nav-icon" aria-hidden="true"></i></br>
                    <div class="logout-text-user-profile">
                        <div class="v-center">
                            Log Out
                        </div>
                    </div>
                </div>

            </div>
        <div class="charts-displayed">
            <div class="backdrop"></div>
            <div class="grid-stack grid-stack-12 m-l-140 m-r-20">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <? if ($result!=0) { echo  "<h2>".$result."</h2>"; }

                         ?>
                        <? $userid=$_SESSION['userid']; $userform=build_user_profile_form($userid); echo $userform; ?>

                    </div>
                    <style type="text/css">
                    .list-group-item {
                        color:#000;
                        text-align:left;
                        width:100%;
                    }

                    </style>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <? $userid=$_SESSION['userid']; $boardlist=build_board_list($userid); echo $boardlist; ?>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="box-prop">

                            <h4 class="m-t-0 header-title">Latest at DealerSocket</h4>
                            <script type='text/javascript' id='vidyard_embed_code_HtoaTEaAkc49DHPcnGf29y' src='//play.vidyard.com/HtoaTEaAkc49DHPcnGf29y.js?v=3.1.1&type=lightbox'></script><div class='outer_vidyard_wrapper'><div class='vidyard_wrapper' onclick='fn_vidyard_HtoaTEaAkc49DHPcnGf29y();'><img class="latest-video" src="//play.vidyard.com/HtoaTEaAkc49DHPcnGf29y.jpg?" alt="DealerSocket We Are Automotive 4: Easy Wins"><div class="vidyard_play_button"><a href="javascript:void(0);"></a></div></div></div>
                        </div>
                    </div>

                </div>
        </div>
    </div>
    <!-- hidden forms -->
    <div class="right-content" style="display: none;">
        <!-- begin login-header -->
        <div class="login-header">
            <div id="login-content">
            </div>
        </div>
        <!-- begin login-content -->
        <div class="login-content">
            <hr />
            <p class="text-center">
                &copy; DealerSocket All Right Reserved 2016
            </p>
        </div>
        <!-- end login-content -->
    </div>
    <div class="blocker"></div>

</div>
</div>
</div>
<!-- ================== BEGIN PAGE LEVEL JS ================== -->
   	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="./js/all-min.js"></script>


<!-- end content -->
<!-- begin scroll to top btn -->
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- end scroll to top btn -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS FROM FOOTER ================== -->


<!--[if lt IE 9]>
<script src="assets/crossbrowserjs/html5shiv.js"></script>
<script src="assets/crossbrowserjs/respond.min.js"></script>
<script src="assets/crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->



<!-- ================== END PAGE LEVEL JS ================== -->

</body>
</html>
