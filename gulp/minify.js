//removed jquery-migrate-1.4.1.js from myScripts. Add it back if there are issues.
//removed switchery.min.js, from myScripts right after handlegridstack.js

(function () {
    'use strict';

    var gulp = require('gulp');
    var path = require('./config');
    var uglify = require('gulp-uglify');
    var minifyHtml = require('gulp-minify-html');
    var minifyCss = require('gulp-minify-css');
    var imageMin = require('imagemin');
    var imageminMozjpeg = require('imagemin-mozjpeg');
    var imageminPngquant = require('imagemin-pngquant');
    var Fontmin = require('fontmin');
    var concat = require('gulp-concat');
    var plumber = require('gulp-plumber');
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer');

    var fontmin = new Fontmin()
        .src(path.gulp.paths.src + '/fonts/*.{ttf,woff, eot, woff2}')
        .dest(path.gulp.paths.dist + '/fonts');
    // var unCss = require('gulp-uncss');

    var del = require('del');


    gulp.task('clean', function (done) {
        return del([path.gulp.paths.dist], done);
    });

    gulp.task('fonts', function () {
        fontmin.run(function (err, files) {
            if (err) {
                throw err;
            }
            console.log(files[0]);
        });
    });

    gulp.task('extraFont', function () {
        return gulp.src(path.gulp.paths.src + '/fonts/fontawesome-webfont.woff2')
            .pipe(gulp.dest(path.gulp.paths.dist + '/fonts'));
    });

    gulp.task('imageMin', function () {
        return imageMin([path.gulp.paths.src + '/img/*.{jpg,png}'], path.gulp.paths.dist + '/img', {
            plugin: [
                imageminMozjpeg({ targa: true }),
                imageminPngquant({ quality: '65-85' })
            ]
        }).then(function (files) {
            var flength = files.length;
            for (var i = 0; i < flength; i++) {
                console.log('Images done ' + files[i]);
            }
        });
    });

    gulp.task('scripts', function () {
        return gulp.src(path.gulp.paths.src + '/**/*.js')
            .pipe(plumber(function (error) {
                console.error(error.message);
                gulp.emit('finish');
            }))
            //.pipe(uglify())
            .pipe(gulp.dest(path.gulp.paths.dist));
    });


    gulp.task('myScripts', ['scripts'], function () {
        return gulp.src([path.gulp.paths.src + '/js/{waa-chartli.js,gridstack.js,handle-grid-stack.js,raphael-min.js,morris.min.js,dealership-overview.js,cars-sold.js,cars-sold-pie.js,cars-sold-minute.js,jquery.sparkline.js,interactive-map.js,phone-business-hours.js,revenue-sparkline.js,sold-new-past-30-days-average.js,sold-used-past-30-days-average.js,new-car-profit.js,line-chart-no-fill.js,line-chart-fill.js,bar-chart-no-guides-2.js,used-car-profit.js,line-chart-no-fill-2.js,target-bar-line-2.js,average-days-sold.js,morris-donut.js,chartist.min.js,horizontal-chart.js,prospects-sold.js,labor-hours.js,label-placement-chart.js,flot-live-update.js,jquery.flot.min.js,jquery.knob.js,outbound-calls-per-day.js,closed-ro-appt.js,closed-ro-future.js,sold-with-trade.js,sold-return-customer.js,sold-fresh-up.js,sold-phone-up.js,sold-internet.js,waa-chart.js}'])
            .pipe(concat('all-min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(path.gulp.paths.dist + '/js/'));
    });




    gulp.task('html', function () {
        return gulp.src(path.gulp.paths.src + '/**/*.html')
            .pipe(minifyHtml())
            .pipe(gulp.dest(path.gulp.paths.dist));
    });

    gulp.task('php', function () {
        return gulp.src(path.gulp.paths.src + '/**/*.php')
            .pipe(gulp.dest(path.gulp.paths.dist));
    });

    gulp.task('css', function () {
        return gulp.src(path.gulp.paths.src + '/stylesheets/*.css')
            .pipe(concat('all-min.css'))
            .pipe(sourcemaps.init())
            .pipe(postcss([autoprefixer()]))
            .pipe(sourcemaps.write('.'))
            .pipe(minifyCss({ processImport: false }))
            .pipe(gulp.dest(path.gulp.paths.dist + '/stylesheets'));
    });

    gulp.task('watch', function () {
        gulp.watch(path.gulp.paths.src + '/**/*.js', ['scripts', 'myScripts']);
        gulp.watch(path.gulp.paths.src + '/**/*.html', ['html']);
        gulp.watch(path.gulp.paths.src + '/**/*.php', ['php']);
        gulp.watch(path.gulp.paths.src + '/**/*.css', ['css']);
        gulp.watch(path.gulp.paths.src + '/**/*.{jpg,png}', ['imageMin']);
        gulp.watch(path.gulp.paths.src + '/**/*.{ttf,woff}', ['fonts']);
    });


    gulp.task('buildApp', ['scripts', 'myScripts', 'html', 'php', 'imageMin', 'fonts', 'extraFont', 'css']);

    gulp.task('build', ['clean'], function () {
        gulp.start(['buildApp', 'watch']);
        console.log('finished gulp tasks');
    });

})();
